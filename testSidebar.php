<!DOCTYPE html>
<!-- saved from url=(0044)http://plugins.adchsm.me/slidebars/usage.php -->
<html lang="en" class="sb-init"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta -->
        <meta charset="utf-8">
        <title>Slidebars | Usage</title>
        <meta name="description" content="Instructions on setting up and using Slidebars in your website.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Stylesheets -->
        <link href="./Slidebars   Usage_files/css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/slidebars.min.css">
        <link rel="stylesheet" href="./css/slidebars-theme.css">
        <!--<link rel="stylesheet" href="./css/slidebars-style.css">-->

        <!-- Shims -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- Analytics -->
        <!--<script async="" type="text/javascript" src="./Slidebars   Usage_files/ca-pub-7022862816626393.js"></script><script async="" src="./Slidebars   Usage_files/analytics.js"></script>-->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-42666109-2', 'adchsm.me');
            ga('send', 'pageview');
        </script>

    <body id="top" style="">
        <!-- Navbar -->
        <nav class="navbar navbar-default navbar-fixed-top sb-slide" role="navigation">
            <!-- Left Control -->
            <div class="sb-toggle-left navbar-left">
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
            </div><!-- /.sb-control-left -->

            <!-- Right Control -->
            <div class="sb-toggle-right navbar-right">
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
            </div><!-- /.sb-control-right -->

            <div class="container">
                <!-- Logo -->
                <div id="logo" class="navbar-left">
                    <a href="http://plugins.adchsm.me/slidebars/"><img src="./Slidebars   Usage_files/slidebars-logo@2x.png" alt="Slidebars Logo" width="118" height="40"></a>
                </div><!-- /#logo -->

                <!-- Menu -->
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://plugins.adchsm.me/slidebars/">Home</a></li>
                    <li><a href="http://plugins.adchsm.me/slidebars/index.php#download">Download</a></li>
                    <li><a href="./Slidebars   Usage_files/Slidebars   Usage.htm">Usage</a></li>
                    <li><a href="http://plugins.adchsm.me/slidebars/contact.php">Contact</a></li>
                    <li><a id="top-arrow" href="http://plugins.adchsm.me/slidebars/usage.php#top">^</a></li>
                </ul>
            </div>
        </nav>

        <!-- Site -->
        <div id="sb-site" style="min-height: 642px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 main vcenter">

                        <?php
                        // page displaybased on the $projectId & $storyId 

                        if (!isset($currentProjectId)) {
                            include('createProject.php');
                        } else if (isset($currentProjectId) && !isset($currentStoryId)) {
                            include ('./dialogueShow.php');
                            include('createStory.php');
                        } else {
                            ?>


                            <div class="container">
    <?php
    $story = DBConnector::getStory($currentStoryId);
    ?>
                                <form class="form-horizontal">
                                    <fieldset>

                                        <!-- Form Name -->
                                        <!--                                    <legend>New Story</legend>-->

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label" for="title">Title</label>  
                                            <div class="col-xs-8">
                                                <input id="title" name="title" type="text" placeholder="TItle" class="form-control input-md" required="" value="<?php echo $story['story'] ?>">

                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label" for="description">Description</label>
                                            <div class="col-xs-8">                     
                                                <textarea class="form-control" id="description" name="description" rows="5" ><?php echo $story['description'] ?></textarea>
                                            </div>
                                        </div>                                

                                        <!-- Button (Double) -->
                                        <!--                                    <div class="form-group">
                                                                                <label class="col-xs-2 control-label" for="button1id"></label>
                                                                                <div class="col-xs-8">
                                                                                    <button id="button1id" name="button1id" class="btn btn-primary">Save</button>
                                                                                    <button id="button1id" name="button1id" class="btn btn-success">Save</button>
                                                                                    <button id="button2id" name="button2id" class="btn btn-danger">Cancel</button>
                                                                                </div>
                                                                            </div>-->

                                    </fieldset>
                                </form>




                            </div> <!-- /container -->
    <?php
}
?>

                        <div class="modal fade bs-example-modal-lg" id="newitem" tabindex="-1" role="dialog" aria-labelledby="newitem" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Create a New Project</h4>
                                    </div>
                                    <!-- Code for input -->
                                    <fieldset> 
                                        <legend></legend>
                                        <form class="form-horizontal" name="creteProject" method="POST"  action="intermediatePasser.php">

                                            <div class="form-group">
                                                <label for="firstname" class="col-sm-2 control-label">Project Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="projectName" name="projectName" style="width:50%;"  placeholder="Enter Project Name">
                                                </div>


                                            </div>                    
                                            <br>
                                            <div class="modal-footer">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-success">Create</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>  


                                        </form>

                                    </fieldset>
                                    <!-- End of input-->
                                </div>
                            </div>
                        </div>
                        <div class="modal fade bs-example-modal-lg" id="showDialogue1" tabindex="-1" role="dialog" aria-labelledby="newitem" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Create a New Project</h4>
                                    </div>
                                    <!-- Code for input -->
                                    <fieldset> 
                                        <legend></legend>
                                        <form class="form-horizontal" name="creteProject" method="POST"  action="intermediatePasser.php">

                                            <div class="form-group">
                                                <label for="firstname" class="col-sm-2 control-label">Project Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="projectName" name="projectName" style="width:50%;"  placeholder="Enter Project Name">
                                                </div>
                                            </div>                    
                                            <br>
                                            <div class="modal-footer">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-success">Create</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>  


                                        </form>

                                    </fieldset>
                                    <!-- End of input-->
                                </div>
                            </div>
                        </div>
<?php if (isset($_GET['projectid'])) { ?>
                            <div class="modal fade bs-example-modal-lg" id="newStory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Create a New Story</h4>
                                        </div>
                                        <!-- Code for input -->
                                        <fieldset> 
                                            <legend></legend>
                                            <form class="form-horizontal" name="creteProject" method="post" onsubmit="" action="DBStoryValPasser.php">

                                                <div class="form-group">
                                                    <label for="firstname" class="col-sm-2 control-label">Story Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="storyName" name="storyName" style="width:50%;"  placeholder="Enter Story Name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="firstname" class="col-sm-2 control-label">Description</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="storyDescription" name="storyDescription" style="width:50%;"  placeholder="Enter Story Description">
                                                    </div>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input type="hidden" class="form-control" id="projectId" name="projectId" style="width:50%;"  value="<?php echo $_GET['projectid'] ?>">
                                                </div>



                                                <div class="modal-footer">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-success">Create</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                                                    </div>
                                                </div>  
                                            </form>
                                        </fieldset>
                                        <!-- End of input-->
                                    </div>
                                </div>
                            </div>
<?php } else if (!isset($_GET['projectid'])) {
    ?>
                            <div  class="modal fade bs-example-modal-lg" id="newStory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-header" style="height: 70px">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h3 style="color: red">Warning</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p><h4>Select the project first!!!</h4></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?php } ?>

                    </div><!-- /#content-->

                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /#sb-site -->

        <!-- Slidebars -->
        <div class="sb-slidebar sb-left sb-style-overlay">
            <nav>
                <ul class="sb-menu">
                    <li><img src="./Slidebars   Usage_files/slidebars-logo-white@2x.png" alt="Slidebars" width="118" height="40"></li>
                    <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/">Home</a></li>
                    <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/index.php#download">Download</a></li>
                    <li class="sb-close"><a href="./Slidebars   Usage_files/Slidebars   Usage.htm">Usage</a></li>
                    <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/usage.php#api">API</a></li>
                    <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/compatibility.php">Compatibility</a></li>
                    <li><a href="#" class="sb-toggle-submenu"><span class="sb-caret"></span> Help &amp; Issues</a>
                        <ul class="sb-submenu">
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues.php">Overview</a></li>
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues/fixed-position.php">Fixed Positions</a></li>
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues/modal.php">Modal.js</a></li>
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues/squashed-content.php">Squashed Navbar Content</a></li>
                        </ul>
                    </li>
                    <li><a href="#" class="sb-toggle-submenu"><span class="sb-caret"></span> Another</a>
                        <ul class="sb-submenu">
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues.php">Overview</a></li>
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues/fixed-position.php">Fixed Positions</a></li>
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues/modal.php">Modal.js</a></li>
                            <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/issues/squashed-content.php">Squashed Navbar Content</a></li>
                        </ul>
                    </li>
                    <li class="sb-close"><a href="http://plugins.adchsm.me/slidebars/contact.php">Contact</a></li>
                    <li class="sb-close"><a class="github" href="https://github.com/adchsm/Slidebars">Github</a></li>
                    <li class="sb-close"><span class="sb-open-right">About the Author</span></li>
                    <li class="sb-close"><small>Slidebars © 2014 Adam Smith</small></li>
                </ul>
            </nav>
        </div>
        <!-- Scripts -->
        <!-- jQuery -->
        <script src="./js/jquery.min.js"></script>

        <!-- Bootstrap -->
        <script src="./js/bootstrap.min.js"></script>

        <!-- Slidebars -->
        <script src="./js/slidebars.min.js"></script>

        <script>
            (function($) {
                $(document).ready(function() {
                    // Initiate Slidebars
                    $.slidebars();
                    // Slidebars Submenus
                    $('.sb-toggle-submenu').off('click').on('click', function() {
                        $submenu = $(this).parent().children('.sb-submenu');
                        $(this).add($submenu).toggleClass('sb-submenu-active'); // Toggle active class.


                        if ($submenu.hasClass('sb-submenu-active')) {
                            $submenu.slideDown(200);
                        } else {
                            $submenu.slideUp(200);
                        }
//                        $('.sb-submenu').not( this ).slideUp(200).removeClass('sb-submenu-active');
                    });
                });
            })(jQuery);
        </script>

        <!-- Further Tracking -->
        <script>
            (function($) {
                $(document).ready(function() {
                    // Downloads
                    // 0.10.2
                    $('a.download-0102').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.10.2', {'nonInteraction': 1});
                    });
                    // 0.10.1
                    $('a.download-0101').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.10.1', {'nonInteraction': 1});
                    });
                    // 0.10
                    $('a.download-010').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.10', {'nonInteraction': 1});
                    });
                    // 0.9.4
                    $('a.download-094').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.9.4', {'nonInteraction': 1});
                    });
                    // 0.9
                    $('a.download-09').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.9', {'nonInteraction': 1});
                    });
                    // 0.8.2
                    $('a.download-082').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.8.2', {'nonInteraction': 1});
                    });
                    // 0.8.1
                    $('a.download-081').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.8.1', {'nonInteraction': 1});
                    });
                    // 0.8
                    $('a.download-08').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.8', {'nonInteraction': 1});
                    });
                    // 0.7.1
                    $('a.download-071').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.7.1', {'nonInteraction': 1});
                    });
                    // 0.7
                    $('a.download-07').on('click', function() {
                        ga('send', 'event', 'Download', 'Slidebars', '0.7', {'nonInteraction': 1});
                    });
                    // Github
                    $('a.github').on('click', function() {
                        ga('send', 'event', 'External Link', 'GitHub', {'nonInteraction': 1});
                    });
                    // Dreamhost
                    $('a#dreamhost').on('click', function() {
                        ga('send', 'event', 'External Link', 'DreamHost', {'nonInteraction': 1});
                    });
                    // Open Left Slidebar
                    $('.sb-open-left, .sb-toggle-left').on('touchend click', function() {
                        ga('send', 'event', 'Function', 'Open Slidebar', 'Open Left Slidebar', {'nonInteraction': 1});
                    });
                    // Open Right Slidebar
                    $('.sb-open-right, .sb-toggle-right').on('touchend click', function() {
                        ga('send', 'event', 'Function', 'Open Slidebar', 'Open Right Slidebar', {'nonInteraction': 1});
                    });
                });
            })(jQuery);</script>

        <!-- Prettify -->


        <!-- Smooth Page Scrolling -->
        <script>
            (function($) {
                $(document).ready(function() {
                    $('a[href^="#"]').on('touchend click', function(e) {
                        e.preventDefault();
                        var id = $(this).attr("href");
                        var offset = 70;
                        var target = $(id).offset().top - offset;
                        $('html, body').animate({scrollTop: target}, 500);
                    });
                });
            })(jQuery);
        </script>

        
        
                        <div class="modal fade bs-example-modal-lg" id="showDialogue" tabindex="-1" role="dialog" aria-labelledby="newitem" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Create a New Project</h4>
                                    </div>
                                    <!-- Code for input -->
                                    <fieldset> 
                                        <legend></legend>
                                        <form class="form-horizontal" name="creteProject" method="POST"  action="intermediatePasser.php">

                                            <div class="form-group">
                                                <label for="firstname" class="col-sm-2 control-label">Project Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="projectName" name="projectName" style="width:50%;"  placeholder="asfasdf as Enter Project Name">
                                                </div>
                                            </div>                    
                                            <br>
                                            <div class="modal-footer">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-success">Create</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>  


                                        </form>

                                    </fieldset>
                                    <!-- End of input-->
                                </div>
                            </div>
                        </div>

    </body></html>