<!DOCTYPE html>

<?php
include './DBConnector.php';
if (isset($_GET['projectid'])) {
    $currentProjectId = $_GET['projectid'];
}
if (isset($_GET['storyid'])) {
    $currentStoryId = $_GET['storyid'];
}
?>

<!-- saved from url=(0044)http://getbootstrap.com/examples/dashboard/# -->
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="./img/favicon.png">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./dashboard/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="./css/jquery-ui.css">

        <script src="./js/jquery-1.10.2.js"></script>
        <script src="./js/jquery-ui.js"></script>
        <script src="./js/commons.js"></script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style id="holderjs-style" type="text/css"></style>

        <link href="./css/oscar-styles.css" rel="stylesheet">

        <script>
            $(document).ready(function() {
                hideWindowLoader();
            });
        </script>

    </head>

    <body style="">

        <?php
        include('documentHeader.php');
        ?>

        <script>
            document.getElementById("requirementsHeading").className = "active";
        </script>
        <div id="sb-site" style="min-height: 642px;">
            <div class="container-fluid fill">
                <div class="row fill">



                    <div class="col-xs-12 main vcenter">

                        <?php
                        // page displaybased on the $projectId & $storyId 

                        if (!isset($currentProjectId)) {
                            include('createProject.php');
                        } else if (isset($currentProjectId) && !isset($currentStoryId)) {
                            include('createStory.php');
                        } else {
                            ?>
                            <div class="container">
                                <?php
                                $story = DBConnector::getStory($currentStoryId);
//                                print_r($story);
                                ?>
                                <form class="form-horizontal">
                                    <fieldset>

                                        <!-- Form Name -->
                                        <!--                                    <legend>New Story</legend>-->
                                        <!--Ref ID-->
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label" for="title">Reference ID</label>  
                                            <div class="col-xs-8">
                                                <input id="refId" name="refId" type="text" placeholder="Reference ID" class="form-control input-md" required="" value="<?php echo $story['refId'] ?>" >

                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label" for="title">Title</label>  
                                            <div class="col-xs-8">
                                                <input id="title" name="title" type="text" placeholder="TItle" class="form-control input-md" required="" value="<?php echo $story['story'] ?>">

                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label" for="description">Description</label>
                                            <div class="col-xs-8">                     
                                                <textarea class="form-control" id="description" name="description" rows="5" ><?php echo $story['description'] ?></textarea>
                                            </div>
                                        </div>                                

                                        <!-- Button (Double) -->
                                        <!--                                    <div class="form-group">
                                                                                <label class="col-xs-2 control-label" for="button1id"></label>
                                                                                <div class="col-xs-8">
                                                                                    <button id="button1id" name="button1id" class="btn btn-primary">Save</button>
                                                                                    <button id="button1id" name="button1id" class="btn btn-success">Save</button>
                                                                                    <button id="button2id" name="button2id" class="btn btn-danger">Cancel</button>
                                                                                </div>
                                                                            </div>-->

                                    </fieldset>
                                </form>



                            </div> <!-- /container -->
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <?php
        include('Sidebar.php');
        ?>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/docs.min.js"></script>

        <?php
        include ('./CommonDialogs.php');
        ?>
    </body>
</html>