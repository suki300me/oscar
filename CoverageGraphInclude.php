
<!-- c3 core CSS -->
<link href="./css/c3/c3.css" rel="stylesheet">



<?php
//                       echo '<pre>';
include_once './ProcessCoverageGraph.php';
include_once './Commons.php';
$processCoverageGraph = new ProcessCoverageGraph($_GET['projectid'], $_GET['storyid'], "TC");
$tupleCoverage = $processCoverageGraph->go(true);
$tupleCoverageJsArray = json_encode($tupleCoverage);


$allCombProcessCoverageGraph = new ProcessCoverageGraph($_GET['projectid'], $_GET['storyid'], "TCAll");
$allCombTupleCoverage = $allCombProcessCoverageGraph->go(false);
$allCombTupleCoverageJsArray = json_encode($allCombTupleCoverage);

$commons = new Commons();

//                            print_r($commons->array_merge_recursive_sequential($tupleCoverage,$allCombTupleCoverage));
$mergedArray = json_encode($commons->array_merge_recursive_sequential($allCombTupleCoverage, $tupleCoverage));
//                            print_r($mergedArray);
//                        print_r($tupleCoverage);
//                        echo '</pre>';
//echo $tupleCoverageJsArray;
?>
<div id="chart"></div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/c3js/d3.min.js" charset="utf-8"></script>
<script src="./js/c3js/c3.min.js"></script>
<?php
//        include_once './ProcessCoverageGraph.php';
//        $processCoverageGraph = new ProcessCoverageGraph($_GET['projectid'], $_GET['storyid']);
//        $tupleCoverage = $processCoverageGraph->go();
//        $tupleCoverageJsArray = json_encode($tupleCoverage);
//echo $tupleCoverageJsArray;
?>
<script>
    var tupleCoverage = ['2+'];
<?php echo "var tupleCoverageValues = " . $mergedArray . ";\n"; ?>
<?php echo "var allCombTupleCoverageValues = " . $allCombTupleCoverageJsArray . ";\n"; ?>

    //tupleCoverage = $.merge( $.merge( [], tupleCoverage ), tupleCoverageValues );

    var allTags = [];
    allTags.push.apply(allTags, tupleCoverage);
    allTags.push.apply(allTags, tupleCoverageValues);

    var allPointsForGraph = [];

    var index;
    for (index = 0; index < Math.max(tupleCoverageValues, allCombTupleCoverageValues); ++index) {
        console.log(a[index]);
        allPointsForGraph[index] = tupleCoverageValues[index].concat(allCombTupleCoverageValues[index]);
    }


    var chart = c3.generate({
        bindto: '#chart',
        data: {
            rows: tupleCoverageValues,
            type: 'area-spline'
        },
        point: {
            show: false
        },
    color: {
        pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
    }
    });

</script>
