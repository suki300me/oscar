<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <link href="../../css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
            <body>
                <!--<h1 align="center">Test Cases</h1>-->

                <xsl:for-each select="TestCases/Function">
		
		
		
                    <!--<h3>TestCase ID : <xsl:value-of select="@id"/></h3>
                                            <p><b>Failure : <xsl:value-of select="@failure"/></b></p>
                    <h4>Inputs</h4>                    -->
		
			
		
			
                    <table border="0" class="table table-condensed table-hove" >
                        <!-- <xsl:for-each select="TestCase">-->
                        <!--<xsl:key name="preg" match="TestCase" use="@id"/>-->
                        <!--  <xsl:for-each select="key('preg','0')">-->
                        
                        <xsl:for-each select="TestCase">
                            <xsl:if test="@id &lt; 1">
                    
                                <tr bgcolor="LIGHTBLUE">
                                    <th>Test case#</th>
                                    <xsl:for-each select="Input/Var">
                                
                                        <th>
                                            <xsl:value-of select="@name"/>
                                        </th>
                                    </xsl:for-each>
                                </tr>
                            </xsl:if>
                        </xsl:for-each>
                        <!--</xsl:for-each>-->
                        <xsl:for-each select="TestCase">
                            <tr>
                                <td>
                                    <xsl:value-of select="@id"/> 
                                </td>
                                <xsl:for-each select="Input/Var">
                                                
                                                    
                                                
                                    <xsl:choose>
                                        <xsl:when test="@value ='NA'">
                                            <td bgcolor="LIGHTGREY" >
                                                <xsl:value-of select="@value"/>
                                            </td>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <td>
                                                <xsl:value-of select="@value"/>
                                            </td>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:for-each>
                                                
                            </tr>
                        </xsl:for-each> 
                    </table>
		
                </xsl:for-each>	
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>