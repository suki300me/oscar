<?php

require_once 'XmlToJson.php';

class ajaxValidate {

    function formValidate() {

        //include('XmlToJson.php');
        $parser = new XmlToJson();
        $result = $parser->Parse('tcases/bin/TC-Test.xml');

        // echo $result;
        $result1 = json_decode($result);
        $oscarTestCaseCount = count($result1->Function->TestCase);
        //echo ''+$value;
        //Put form elements into post variables (this is where you would sanitize your data)
        $testerCount = @$_POST['testerCount'];
        $timeForTestDesign = @$_POST['timeForTestDesign'];
        $testCaseCount = @$_POST['testCaseCount'];
        $timeForTestExection = @$_POST['timeForTestExection'];

        //Establish values that will be returned via ajax
        $return = array();
        $return['msg'] = '';
        $return['error'] = false;

        //Begin form validation functionality
        if (!isset($testerCount) || empty($testerCount) || !isset($timeForTestDesign) 
                || empty($timeForTestDesign) || !isset($testCaseCount) || empty($testCaseCount)||
                !isset($timeForTestExection) || empty($timeForTestExection)) {
            $return['error'] = true;
            $return['msg'] .= '<li>Error: Field1 is empty.</li>';
        }

        $CurrentDesignExecutionEffort = $timeForTestDesign + $timeForTestExection;
        
        $timeForOscarExecution = $timeForTestExection*$testerCount*$oscarTestCaseCount/$testCaseCount;
        
        $OscarDesignExecutionEffort = ($timeForTestDesign/100)*40 + $timeForOscarExecution;
        
        $Savings=$CurrentDesignExecutionEffort-$OscarDesignExecutionEffort;
        $ROI=$Savings/$OscarDesignExecutionEffort;

        //Begin form success functionality
        if ($return['error'] === false) {
            $return['msg'] = '<li>Success Message</li>';
        }

        //Return json encoded results
        $arrayRes = array();
        //array_push($arrayRes, $value);
        $arrayRes['msg'] = $ROI;
        return json_encode($arrayRes);
    }

}

$ajaxValidate = new ajaxValidate;
echo $ajaxValidate->formValidate();
?>