<!DOCTYPE html>
<!-- saved from url=(0044)http://getbootstrap.com/examples/dashboard/# -->
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="./img/favicon.png">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./css/dashboard.css" rel="stylesheet">
        <link href="dashboard/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="./css/jquery-ui.css">
        <link href="./css/oscar-styles.css" rel="stylesheet">

        <script src="./js/jquery-1.10.2.js"></script>
        <script src="./js/jquery-ui.js"></script>
        <script src="./js/Commons.js"></script>

        <script>
            $(document).ready(function(){
                hideWindowLoader();
            });
            $('.ui-button').button({
                text: false,
                icons: {
                    primary: "you-own-cusom-class"   // Custom icon
                }
            });
        </script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style id="holderjs-style" type="text/css"></style>
        <!--below two javascript files are responsible for dropdown menu for create project -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/docs.min.js"></script>

        <link rel="stylesheet" href="css/common.css">
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/Aristo/jquery-ui-1.8.7.custom.css" />
        <link rel="stylesheet" href="css/minicolors/jquery.miniColors.css">


    </head>

    <body style="">
        <?php
        include('documentHeader.php');
        ?>

        <script>
            document.getElementById("mindMapHeading").className = "active";
        </script>

        <?php
        include('Sidebar.php');
        ?>
        <div id="sb-site" style="min-height: 642px;">
            <div class="container-fluid fill">
                <div class="row fill">


                    <div class="col-xs-12 main" >
                        <?php
                        // page displaybased on the $projectId & $storyId 


                        if (!isset($_GET['projectid'])) {
                            //$projectId = $_GET['$projectid'];                         
                            include('createProject.php');
                        } else if (isset($_GET['projectid']) && !isset($_GET['storyid'])) {

                            include('createStory.php');
                        } else {
                            ?>

                            <div id="print-area">
                                <p class="print-placeholder">Please use the print option from the
                                    mind map menu</p>
                            </div>
                            <!-- DEBUG -->
                            <div id="debug-warning"></div>
                            <!-- /DEBUG -->
                            <div id="container">
                                <div id="topbar">
                                    <div id="toolbar">
                                        <div class="buttons">
                                            <span class="buttons-left"> </span> <span class="buttons-right">
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div id="canvas-container">
                                    <div id="drawing-area" class="no-select"></div>
                                </div>
                                <div id="bottombar">
                                    <div id="about">
                                    </div>
                                    <div id="statusbar">
                                        <div
                                            class="buttons buttons-right buttons-small buttons-less-padding"></div>
                                    </div>
                                </div>
                            </div>

                            <script src="js/jquery-1.7.2.min.js"></script>

                            <!-- DEBUG -->
                            <!-- set debug flag for all scripts. Will be removed in production -->
                            <script type="text/javascript">
                var mindmaps = mindmaps || {};
                mindmaps.DEBUG = true;
    <?php
//include 'DBConnector.php'; //This was already included in the projectSidebar.php -> Hence no need to include this again - Sukhitha
    if (isset($_GET['projectid']) && isset($_GET['storyid'])) {
        $map = DBConnector::getMindMap($_GET['projectid'], $_GET['storyid']);
        $idproject = $_GET['projectid'];
        $idstory = $_GET['storyid'];
    } else {
        $map = "";
        $idproject = "";
        $idstory = "";
    }
    ?>;
                var mmap = <?php echo json_encode($map); ?>;
                if (mmap.length > 0) {
                    mindmaps.LOADMAP = mmap;
                    mindmaps.PROJECTID =<?php echo json_encode($idproject); ?>;
                    mindmaps.STORYID = <?php echo json_encode($idstory); ?>;
                }
                //   mindmaps.LOADMAP = '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":134,"y":-26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1402325287664},"dimensions":{"x":4000,"y":2000},"autosave":false} ';

                //console.log(mindmaps.Document.prototype.serialize);
                            </script>
                            <div id="mindMapDrawArea">
                                <?php
                                include('./mindMapCanvas.php');
                                ?>
                            </div>
                            <!-- JS:LIB:END -->

                            <!-- PRODUCTION
                          <script type="text/javascript">
                            var _gaq = _gaq || [];
                            _gaq.push([ '_setAccount', 'UA-23624804-1' ]);
                            _gaq.push([ '_trackPageview' ]);
                            (function() {
                              var ga = document.createElement('script');
                              ga.type = 'text/javascript';
                              ga.async = true;
                              ga.src = ('https:' == document.location.protocol ? 'https://ssl'
                                  : 'http://www')
                                  + '.google-analytics.com/ga.js';
                              var s = document.getElementsByTagName('script')[0];
                              s.parentNode.insertBefore(ga, s);
                            })();
                          </script>
                          /PRODUCTION -->
                        <?php } ?>

                    </div>
                </div>


            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
<!--        <script src="./jquery.min.js"></script>
        <script src="./bootstrap.min.js"></script>
        <script src="./docs.min.js"></script>-->

        <script src="js/MindMapToTCaseInput.js"></script>
        <?php
        include ('./CommonDialogs.php');
        ?>
    </body>
</html>
