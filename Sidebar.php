
<!--<script src="jquery.min.js"></script>-->
<!--<link href="bootstrap-combined.min.css" rel="stylesheet">--> 
<!--<script src="bootstrap.min.js"></script>-->
<link href="./css/responsiveSheet.css" rel="stylesheet">
<link href="./css/treeview.css" rel="stylesheet">

<!-- Stylesheets -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" href="./css/slidebars.min.css">
<link rel="stylesheet" href="./css/slidebars-theme.css">

<?php
require_once ('DBConnector.php');

$projs = DBConnector::getAllProject();
?>
<!-- Slidebars -->
<div class="sb-slidebar sb-left sb-style-overlay">
    <nav>
        <ul class="sb-menu">
            <h1><a class ="navbar-brand" href="index.php">Oscar</a></h1>

            <?php
            foreach ($projs as $value) {
                $projId = $value['idproject'];
                $projName = $value['projectName'];
                $storiesForProject = DBConnector::getStoriesForProject($projId);
                ?>

                <li class="hover-buttons-parent menu-item-project" nodeid="<?php echo $projId; ?>">
                    <a href="#" class="sb-toggle-submenu">
                        <span class="sb-caret"></span> <?php echo $projName; ?>
                    </a>
                    <input class="btn btn-danger btn-hover-delete-project" type="button" value="X" nodeid = "<?php echo $projId; ?>"/>
                    <ul class="sb-submenu">
                        <?php
                        foreach ($storiesForProject as $st) {
                            $stId = $st['idstory'];
                            $stName = $st['story'];
                            ?>
                            <li class="sb-close hover-buttons-parent menu-item-story" nodeid = "<?php echo $stId; ?>">
                                <a href="?projectid=<?php echo $projId; ?>&storyid=<?php echo $stId; ?>"><?php echo $stName; ?></a>
                                <input class="btn btn-danger btn-sm btn-hover-delete-story" type="button" value="X" nodeid = "<?php echo $stId; ?>"/></li>
                            <?php
                        }
                        ?>
                        <!--<li class="sb-close"><a href="#" data-toggle="modal" data-target="#newStory">&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp; Add Story</a></li>-->
                        <li class="sb-close"><a href="?projectid=<?php echo $projId; ?>">&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp; Add Story</a></li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <li class="sb-close"><a href="#" data-toggle="modal" data-target="#newitem">&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp; Add Project</a></li>
        </ul>
    </nav>
</div>

<script src="./js/slidebars.min.js"></script>
<script src="./js/Commons.js"></script>
<script src="./js/Sidebar.js"></script>

<script>
    (function($) {
        $(document).ready(function() {
            initSidebar();
        });
    })(jQuery);
</script>