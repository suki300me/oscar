/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Author: Sukhitha Amarakoon
 */

function initSidebar() {

    // Initiate Slidebars
    $.slidebars();
    // Slidebars Submenus
    $('.sb-toggle-submenu').off('click').on('click', function() {
        $submenu = $(this).parent().children('.sb-submenu');
        $(this).add($submenu).toggleClass('sb-submenu-active'); // Toggle active class.


        if ($submenu.hasClass('sb-submenu-active')) {
            $submenu.slideDown(200);
        } else {
            $submenu.slideUp(200);
        }
//              $('.sb-submenu').not( this ).slideUp(200).removeClass('sb-submenu-active');
    });

    $(".btn-hover-delete-project").click(function(e) {
        e.stopPropagation();
        flagForDelete('project', $(this));
    });

    $(".btn-hover-delete-story").click(function(e) {
        e.stopPropagation();
        flagForDelete('story', $(this));
    });

    $('body').on('click', ".btn-undo-delete-project", function() {
        undoFlagForDelete('project', $(this));
    });

    $('body').on('click', ".btn-undo-delete-story", function() {
        undoFlagForDelete('story', $(this));
    });

    function flagForDelete(context, element) {
        var nodeId = element.attr('nodeid');
        var nodeCaption = $.trim(element.siblings('a').text());
        console.log(nodeCaption);
//                console.log(projId);
        fadeOut(element.closest('li'), 500);

        $.ajax({
            type: "POST",
            url: "ServeDBCalls.php",
            data: {'req_type': 'flag_delete_' + context, 'nodeId': nodeId},
            cache: false,
            success: function() {
                bootstrap_alert("", 'You just deleted a ' + context + ': <b><i>' + nodeCaption + '</i></b>. <a href="#" class="text-success btn-undo-delete-' + context + '" nodeid="' + nodeId + '">Undo</a>', 'danger', null);
            },
            error: function(jqXHR, status) {
                bootstrap_alert("", 'Error Occured!' + status, 'danger', 4000);
            }
        });
    }

    function undoFlagForDelete(context, element) {
        var nodeId = element.attr('nodeid');
        $.ajax({
            type: "POST",
            url: "ServeDBCalls.php",
            data: {'req_type': 'undo_flag_delete_' + context, 'nodeId': nodeId},
            cache: false,
            success: function()
            {
                fadeIn($("li[class$='hover-buttons-parent menu-item-" + context + "'][nodeid = " + nodeId + "]"), 500); // Show deleted Entree from the tree view
                fadeOut($("a[class = 'btn-undo-delete-" + context + "'][nodeid = " + nodeId + "]").closest('div'), 1000); // Show deleted Entree from the tree view
            },
            error: function(jqXHR, status) {
                bootstrap_alert("", 'Aw snap! We are look into it right now. ' + status, 'danger', 4000);
            }
        });
    }

    function fadeOut(element, speed) {
        element.slideUp(speed, function() {
            element.hide();
        });
    }

    function fadeIn(element, speed) {
        element.slideDown(speed, function() {
            element.show();
        });
    }
}
