/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// ---- Page Loading Spinnes ----------------------

var addLoadingSpinner = function() {
    var $div = $('<div/>').appendTo('body');
    $div.attr('class', 'loadingModal');
};

var showWindowLoader = function() {
    $("#WindowLoadingModal").show();
};

var hideWindowLoader = function() {
    $("#WindowLoadingModal").hide();
};

// ------------------------------------------------

// Alerts

/*
 * Shows floating alert on top of the page
 * @param {String} title Message to be displayed. Pass "" for empty title
 * @param {String} message Message to be displayed (HTML Allowed)
 * @param {String} alertClass Alert Type. Possible values are: success, info, warning, danger
 * @param {String} timeout Timeout delay for the Alert. Pass null to display for infinite time
 */

bootstrap_alert = function(title, message, alertClass, timeout) {
    if (alertClass === null) {
        alertClass = info;
    }
    if(title!==""){
        title = '<h4>' + title + '</h4>';
    }
    alertClass = 'alert-' + alertClass;
    $('#alert_placeholder').append('<div class="alert alert-block fade in ' + alertClass + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + title + message + '</div>');
    if (timeout !== null) {
        alertTimeout(timeout);
    }
};

function alertTimeout(wait) {
    setTimeout(function() {
        $('#alert_placeholder').children('.alert:first-child').remove();
    }, wait);
}