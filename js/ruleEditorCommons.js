/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// @author: Sukhitha Amarakoon


// No one starts out as an expert.

var initPage = function(mmap, storyid, projid) {
// TODO: Refactor this code. Find a point for this
    if (mmap.length > 0)
        var stringNodes = mmap;
    try {
        var nodes = JSON.parse(stringNodes);
    } catch (e) {
        console.error(e);
    }
//    var listEl = document.getElementById('dataTree');
//    var array = [];
////    array = parseNodes(nodes, listEl, array, "");
//
    var serializer = new XMLSerializer();
    xmlDoc = createTCaseInput(nodes);
    xmlstring = serializer.serializeToString(xmlDoc);
//End of TODO: refactor -------------------------------------------------------------------
    var removeIntent = false;

    //load rules for the first time
    getRules();
//begining of my code

//end of my code
    //button click events
    $('#ruleInsertBtn').click(function() {
        if ($(this).text() === 'Add Rule') {
            //display rule and send to the database
            insertRule();
        } else {
            var ruleid = $(this).attr('data-updateruleid');
            // alert("update button clicked");
            updateRule(ruleid);
        }
    });

    $('#ruleDiscardBtn').click(function(e) {
        removeRule(e);
    });

    $('#btnUpdateTuples').click(function() {
        // alert("$('#btnUpdateTuples').click(function()");
        updateInsertTuples();
    });

    $('#btnInsertTupleBlock').click(function() {
        insertNewTupleRule();
    });

    initDragDroppables();
    initSlider();
    clearRuleEditor();//this is beacause when the rule panel created for first time it isn't empty (mystery) 

    $("li.rule-element:not(.outer-rule-element), li.rule-constraints-element").draggable({
        //var index=$(".well.fill").attr("z-index");
        connectToSortable: "ul.rule-editor",
        helper: "clone",
        zIndex: 10000,
        start: function(event, ui) {

            var clone = $(ui.helper);
            clone.removeClass("rule-element");

            clone.addClass("notdraggable-rule-element");
        },
        revert: '10',
        appendTo: 'body'

    });

    //click event handling for dynamically created buttons rule buttons
    $(document).on('click', "button[id^='ruleid-']", function() {
        //console.log(rules);
        if ($(this).hasClass('rule-delbtn')) {
            //rule delete
            deleteRule($(this).data('rid'), storyid);
        } else if ($(this).hasClass('rule-editbtn')) {
            //rule edit
            console.log($(this));
            console.log($(this).data('rid'));
            $('#ruleInsertBtn')
                    .text('Update Rule')
                    .attr('data-updateruleid', $(this).data('rid'));
            addRuleParts($(this).data('rid'));
        }
    });

    function initDragDroppables() {
        $("ul.rule-editor").sortable({
            revert: '10',
            items: "li:not(.emptyMessage)",
//        placeholder: "ui-state-highlight",
//        connectWith: ".rule-editor, li.rule-element:not(.outer-rule-element), li.rule-constraints-element",
            over: function() {
//            console.log($(this).children());
                $(this).find('.emptyMessage').hide();
                removeIntent = false;
            },
            out: function() {
                droppablePlaceholderProcessor($(this), 3);
                removeIntent = true;
            },
            stop: function(event, ui) {
                droppablePlaceholderProcessor($(this), 1);
                ui.item.removeClass("rule-element");
                ui.item.addClass("notdraggable-rule-element");
            },
            beforeStop: function(event, ui) {
                newItem = ui.item;
                if (removeIntent === true) {
                    ui.item.remove();
                }
            },
            receive: function(event, ui) {
                var ruleTextArr = Array();
                $(ui.item).parents('ul.rule-block').parent('li').prev().each(function() {
                    ruleTextArr.push($(this).text());
                });
                ruleTextArr.push($(ui.item).text());
                $(newItem).attr({
                    id: $(ui.item).attr('id').replace('rule-element-', 'rule-element-dragged-'),
                    "data-text": ruleTextArr.join('.')
                });
            },
            create: function(event, ui) {
                createDroppablePlaceholder($(this), 1);
            }
        });
    }
    // -1 is a special case for add failure="true condition.otherwise it should be 0

    function initSlider() {
        $(".slider-range-min").slider({
            range: "min",
            value: 2,
            min: -1,
            max: 10,
            slide: function(event, ui) {
//                        log($(this).siblings());
                $(this).siblings(".amount").val(ui.value);
            }
        });
    }

    function createDroppablePlaceholder(element, thresoldElemCount) {
        element.append(
                $('<li>').attr('class', 'emptyMessage btn disabled pull-left').append("Drag and Drop Here to Build a Rule")
                );
        droppablePlaceholderProcessor(element, 1);
    }

    function droppablePlaceholderProcessor(element, thresoldElemCount) {
        placeholderElement = element.find('.emptyMessage');
        if (element.children().length <= thresoldElemCount) {
            placeholderElement.show();
        }
        else {
            placeholderElement.hide();
        }
    }

    function addRuleParts(ruleId) {
        // alert('addRuleParts called'+ruleId);
        console.log(rules);
        var btnClass;
        //clear build rule area
        $('#ruleEditorPanel').html('');
        for (var i = 0; i < rules.length; i++) {
            if (rules[i].ruleId == ruleId) {
                for (var j = 0; j < rules[i].ruleParts.length; j++) {
                    //set color class
                    // if (rules[i].ruleparts[j].elementId.indexOf("rule-constraint-have") == 0) {
                    //     btnClass = "btn-primary";
                    // } 
                    if (rules[i].ruleParts[j] == 'rule-constraint-only_when') {
                        btnClass = "btn-success";
                    } else if (rules[i].ruleParts[j]=='rule-constraint-only_not_when') {
                        btnClass = "btn-warning";
                    } 
                    else{
                        btnClass = "btn-info"
                    }
                    //get last text after period
                    var n = rules[i].ruleTextParts[j].lastIndexOf('.');
                    //create li elements for each part of a rule
                    var li = $('<li>', {id: rules[i].ruleParts[j], class: 'btn rule-constraints-element'})
                            .text(rules[i].ruleTextParts[j].substring(n + 1))
                            .attr("data-text", rules[i].ruleTextParts[j])
                            .addClass(btnClass);
                    $('#ruleEditorPanel').append(li);
                }
                break;
            }
        }
    }

    function getRules() {
        //get rules by ajax call
        // alert('getRules js called');
        $.ajax({
            type: "POST",
            url: "processRule.php",
            data: {command: 'getrules', storyid: storyid ,projid: projid},
            beforeSend: function() {
                $("#ruleListLoadingModal").show();
            },
            success: function(data) {
                try {
                    rules = JSON.parse(data);
                    $('#ruleList').html('');
               console.log(rules);
                    for (var i = 0; i < rules.length; i++) {
                        var ruleTxtParts = Array();
                        for (var j = 0; j < rules[i].ruleTextParts.length; j++) {
                            ruleTxtParts.push(rules[i].ruleTextParts[j]);
                            // console.log(rules[i].ruleTextParts[j]);
                        }
                        insertRuleToList(rules[i].ruleId, ruleTxtParts);
                    }
                } catch (e) {
                    console.error(e);
                }
            },
            complete: function() {
                $("#ruleListLoadingModal").hide();
                if (typeof callback === 'function') {
                    callback.call();
                }
            }
        });
    }


    function clearRuleEditor() {
        // alert("clear rule editor called");
        var list = document.getElementById('ruleEditorPanel');
        // var listItemsd = $(".rule-editor").children();
        var listItemsd = $("#ruleEditorPanel").children();
        for (var k = 0; k < listItemsd.length; k++) {
            // alert(listItemsd[k].innerHTML);
            if (listItemsd[k].innerHTML == "Drag and Drop Here to Build a Rule")
                continue;
            listItemsd[k].remove();
        }
        $('.emptyMessage').show();
    }

    //insert rule to the database
    function insertRule() {
        var ruleJson = createRule();
        var ruleText = Array();
        // alert(JSON.stringify(ruleJson, undefined, 2));
        sendRuleToDb("insertrule", ruleJson, getRules);
        //display rule in the rules list
        for (rulePart in ruleJson.rulePart) {
            ruleText.push(ruleJson.rulePart[rulePart]['text']);
        }
//        console.log(ruleText.join(' '));
//        insertRuleToList(rulesCount, ruleText);
        // alert("calling clear rule editor");
        clearRuleEditor();
    }

    //updates rule in the database
    function updateRule(id) {
        // alert("update rule called");
        var ruleJson = createRule();
        ruleJson.ruleId = id;
        sendRuleToDb("updaterule", ruleJson, getRules);
    }

    //delete rule
    function deleteRule(id, storyid) {
        $.post('processRule.php', {command: 'delrule', storyid: storyid, ruleid: id})
                .done(function() {
                    getRules();
                })
                .fail(function() {
                    alert("rule not deleted");
                });
    }

    function sendRuleToDb(cmd, ruleJSON, callback) {
        try {
            ruleStr = JSON.stringify(ruleJSON);
            $.ajax({
                type: "POST",
                url: "processRule.php",
                data: {command: cmd, rulestring: ruleStr, xmlAsString: xmlstring, storyid: storyid},
                beforeSend: function() {
                    $("#ruleListLoadingModal").show();
                },
                complete: function() {
                    $("#ruleListLoadingModal").hide();
                    if (typeof callback === 'function') {
                        callback.call();
                    }
                }
            })
        } catch (e) {
            console.error(e);
        }
    }

    function createRule() {
        //jsonstring send to server
        var ruleStr = "";
        //rule json
        var ruleJSON = {};
//        var listItems = [];
//        var listItems = $(".rule-editor").children();
        var list = $(".rule-editor").children("li:not(.emptyMessage)");//document.getElementById('ruleEditorPanel').childNodes;
        // alert(list.length);
        //var theArray = [];
        //set rule id
//        ruleJSON.ruleId = rulesCount;
        //increase rules counter by one
//        rulesCount++;
        //store rule parts as an array
        ruleJSON.rulePart = [];
        for (var i = 0; i < list.length; i++) {
            var arrValue = list[i].innerHTML;
            var nodeId = list[i].getAttribute('nodeid');
            // when the elment is goes with button or not goes with button nodeId is null
            if (nodeId==null) {
                nodeId = list[i].id;
            };
            ruleJSON.rulePart.push(
                    {
                        partId: i,
                        elemId: list[i].id,
                        text: list[i].getAttribute('data-text'),
                        nodeId: nodeId
                    });
            //theArray.push(arrValue);
        }
//        console.log(ruleJSON);
        return ruleJSON;
    }

    function insertRuleToList(ruleid, ruleComponents) {
        html = '<li class = "list-group-item" style = "margin-top: 10px;"><h4 style="line-height: 25px;">';
        html += '<button class="btn btn-default rule-editbtn pull-right" type="button" data-rid="' + ruleid + '" id="ruleid-' + ruleid + '">Edit Rule</button>';
        html += '<button class="btn btn-danger rule-delbtn pull-right top" type="button" data-rid="' + ruleid + '" id="ruleid-' + ruleid + '">Delete Rule</button>';
        $.each(ruleComponents, function(key, value) {
            if (value !== null){
//                remove the root element from rule
                value = value.substring(value.indexOf(".") + 1);
                html += ' <span class="label label-primary">' + value + '</span>';
//                alert(value);
//                console.log(value);
            }
        });
        html += '</h4></li>';

        $(html).appendTo('#ruleList');
    }

    function removeRule(e) {
        $('#ruleEditorPanel').html('');
        createDroppablePlaceholder($('#ruleEditorPanel'), 1);
        $('#ruleInsertBtn').text('Add Rule').attr('data-update', 'insert').removeAttr('data-updateruleid');
    }

    /*
     * Tuple processor client
     * Creates something like below to send to the server
     * {
     *  [
     *      tupleConstraints: 2,
     *      elementIds: 
     *              [
     *                  fh87-43tr34-tr43q-r5q23-4r523f23q
     *              ]
     *  ],
     *  [
     *      tupleConstraints: 3,
     *      elementIds: 
     *              [
     *                  fh87-43tr34-tr43q-r5q23-4r523f432,
     *                  43-f-34gfw45ge-h5e6j-5-7j6r-7kj,
     *                  kmrgti6y-54y4-w5y-45yw54yw5y-w45
     *              ]
     *  ]
     * }
     */

    function getTupleConfiguration() {
        var tupleRules = {};
        tupleRules.constraintBlock = [];
        var ids;
        var duplicateItems = [];
        alert("getTupleConfiguration");
        $('#tupleRulesHolder').find('.tupleBlock').each(function() {
            var tupleConstraint = $(this).find('.amount').val();

            var tupleRule = {};
            tupleRule.ruleElements = [];

            $(this).find('.tupleEditorPanel').children().each(function() {
//                if($(this)hasAttribute('nodeid'))
                ids = $('[id="' + $(this).attr('id')  + '"]');
                if (ids.length > 1 && ids[0] == this) {
                    duplicateItems.push($(this).attr('nodeid'));
                    console.warn('Multiple IDs #' + $(this).attr('nodeid'));
                }

                var elementId = $(this).attr('nodeid');
                if (typeof elementId !== 'undefined') {
                    tupleRule.ruleElements.push(elementId);
                }
            console.log(elementId);
            });
            tupleRules.constraintBlock.push(
                    {
                        tupleConstraint: tupleConstraint,
                        elementIds: tupleRule
                    });
//            console.log('muhahahaha');
        });
        if (duplicateItems.length > 0) {
            highlightTupleErrors(duplicateItems);
            return null;
        }
        else {
            return tupleRules;
        }
    }

    function highlightTupleErrors(duplicateItems) {
        var errorHighlightTime = 2000; //in milliseconds

        duplicateItems.forEach(function(item) {
            $('#tupleRulesHolder').find('li[nodeid=' + '"' + item + '"]').each(function() {
                $(this).addClass("btn-danger");
            });
            setTimeout(function() {
                $("#tupleRulesHolder").find(".btn-danger").each(function() {
                    $(this).removeClass("btn-danger");
                });
            }, errorHighlightTime);
        });
    }

    function sendTupleToServer(tupleJSON) {
        // console.log(JSON.stringify(tupleJSON, undefined, 2));
        // console.log(typeof tupleJSON);
        // var tuppleObj=jQuery.parseJSON(tupleJSON);
        // alert(tupleJSON.constraintBlock.tupleConstraint);
        // console.log(tupleJSON.constraintBlock[0].tupleConstraint);
        try {
            tupleStr = JSON.stringify(tupleJSON);
            // tupleConstraint passed because to check if it is -1
            tupleconstraint=tupleJSON.constraintBlock[0].tupleConstraint;
            $.ajax({
                type: "POST",
                url: "ServeTuples.php",
                data: {tupleSring: tupleStr, storyid: storyid, tupleConstraint: tupleconstraint},
                beforeSend: function() {
                    $("#WindowLoadingModal").show();
                },
                complete: function() {
                    $("#WindowLoadingModal").hide();
//                    if (typeof callback === 'function') {
//                        callback.call();
//                    }
                }
            });
        } catch (e) {
            console.error(e);
        }
    }

    function updateInsertTuples() {
        // alert("updateInsertTuples");
        var tupleJson = getTupleConfiguration();
        if (tupleJson !== null) {

            // alert(JSON.stringify(tupleJson, undefined, 2));
            sendTupleToServer(tupleJson);
        }
        else{
            console.log("tupleJson !== null")
        }
    }

    function insertNewTupleRule() {
        html = '<tr valign="middle" class="tupleBlock panel panel-default">';
        html += '    <td class="active" style="width:1%; vertical-align: middle;">';
        html += '        <input type="text" class="amount" value="2" readonly style="border:0; color:#f6931f; font-weight:bold;">';
        html += '        <div class="slider-range-min"></div>';
        html += '    </td>';
        html += '    <td class="success">';
        html += '        <ul class="rule-editor tupleEditorPanel">';
//        html += '            <li class="emptyMessage btn disabled pull-left">Drag and Drop Here to Build a Rule</li>';
        html += '        </ul>';
        html += '    </td>';
        html += '</tr>';

        $(html).appendTo('#tupleRulesHolder');
        initSlider();
        initDragDroppables();
    }
};
