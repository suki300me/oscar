jQuery(document).ready(function(){
    jQuery("#fromList li").draggable('destroy').draggable({
        connectToSortable: "#toList",
        revert: "invalid",
        containment: '#equipCont',
        helper: function(e, ui) {
            return jQuery(this).clone().css('width', jQuery(this).width());
        }
    });
    jQuery("#toList").droppable('destroy').droppable({
        drop: function(e, ui) {
            var dragClone = jQuery(ui.draggable).clone();
            jQuery("#toList").append(dragClone);
        }
    });
    jQuery("ul, li").disableSelection();
});