/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function createXmlDocument() {
    var xmldoc = document.implementation.createDocument(null, "System", null);
    return xmldoc;
}

function createTCaseInput(nodes) {
    var xmlDoc = document.implementation.createDocument(null, "System", null);
    xmlDoc.getElementsByTagName("System")[0].setAttribute("name", "Tcases");

    func = xmlDoc.createElement("Function");
    func.setAttribute("name", "TC");

    xmlUselessComment = ' Version: $Revision: 199 $ $Date: 2012-02-26 14:50:41 -0600 (Sun, 26 Feb 2012) $ ';
    newComment = xmlDoc.createComment(xmlUselessComment);

    loadDoc = xmlDoc.getElementsByTagName("System")[0];
    loadDoc.appendChild(newComment);
    loadDoc.appendChild(func);

    input = xmlDoc.createElement("Input");
    //input.setAttribute("test","value");
    loadDoc = xmlDoc.getElementsByTagName("Function")[0];
    loadDoc.appendChild(input);

    //console.log(new XMLSerializer().serializeToString(xmlDoc));
    createXmlFromArray(nodes.mindmap.root, xmlDoc.getElementsByTagName("Input")[0], xmlDoc);
    return xmlDoc;
}

function createXmlFromArray(mindmapnode, xmlnode, xmldocument1) {
    if (mindmapnode.children.length > 0) {
        for (var i = 0; i < mindmapnode.children.length; i++) {
//        xmlDoc=xmlnode.ownerDocument;
            xmlDoc = xmldocument1;
            if (xmlnode.lastChild) {
                //console.log(xmlnode.lastChild.tagname + "  " + xmlnode.lastChild.getAttribute("name"));
            }

            if (mindmapnode.children[i].children.length > 0) {
                if (xmlnode.lastChild && xmlnode.lastChild.nodeName == "VarSet" && xmlnode.lastChild.getAttribute("name") == mindmapnode.text.caption.replace(/ /g, '-')) {
                    myElement = xmlnode.lastChild;

                } else {
                        myElement = xmlDoc.createElement("VarSet");
                        myElement.setAttribute("name", mindmapnode.text.caption.replace(/ /g, '-'));
                        myElement.setAttribute("id", mindmapnode.id);
                        xmlnode.appendChild(myElement);
                }
            } else {

                if (xmlnode.lastChild && xmlnode.lastChild.nodeName == "Var" && xmlnode.lastChild.getAttribute("name") == mindmapnode.text.caption.replace(/ /g, '-')) {
                    myElement = xmlnode.lastChild;
                } else {
                    myElement = xmlDoc.createElement("Var");
                    myElement.setAttribute("name", mindmapnode.text.caption.replace(/ /g, '-'));
                    myElement.setAttribute("id", mindmapnode.id);
                    xmlnode.appendChild(myElement);
                }
            }


            createXmlFromArray(mindmapnode.children[i], myElement, xmldocument1);

        }

    } else {
        xmlDoc = xmldocument1;
//                xmlDoc=xmlnode.ownerDocument;
        myElement = xmlDoc.createElement("Value");
        myElement.setAttribute("name", mindmapnode.text.caption.replace(/ /g, '-'));
        myElement.setAttribute("id", mindmapnode.id);
        xmlnode.appendChild(myElement);
    }
}
