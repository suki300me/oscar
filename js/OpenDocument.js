/**
 * Unused for now.
 * 
 * @constructor
 */
mindmaps.OpenDocumentView = function() {

};

/**
 * Creates a new OpenDocumentPresenter. It loads a Mind Map
 * 
 * @constructor
 */
mindmaps.OpenDocumentPresenter = function(eventBus, mindmapModel, view, loadableMindMap) {

  this.go = function() {
    //var doc = new mindmaps.Document();
    //console.log(loadableMindMap);
    var doc = mindmaps.Document.fromJSON(loadableMindMap);
    mindmapModel.setDocument(doc);
  };
};
