<!DOCTYPE html>

<!-- saved from url=(0044)http://getbootstrap.com/examples/dashboard/# -->
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./dashboard/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="./css/jquery-ui.css">

        <!--<script src="./js/jquery-1.10.2.js"></script>-->
        <script src="./js/jquery-ui.js"></script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style id="holderjs-style" type="text/css"></style>

        <link href="./css/oscar-styles.css" rel="stylesheet">

        <!-- c3 core CSS -->
        <link href="./css/c3/c3.css" rel="stylesheet">

    </head>

    <body style="overflow: scroll;">

        <?php
        include('documentHeader.php');
        ?>

        <script>
            //document.getElementById("requirementsHeading").className = "active";
        </script>
        <div class="container-fluid fill">
            <div class="row fill">
                <div class="col-xs-2 sidebar fill" style="overflow-y: hidden; height:90%;">
                    <?php
                    include('projectSidebar.php');
                    ?>
                </div>

                <div class="col-xs-10 main vcenter">
                    <center><h1>Coverage Graph</h1></center>
                    <div class="container">
                        <div id="mainData" class="col-xs-11 main vcenter">
                            <?php
                       echo '<pre>';
                            include_once './ProcessCoverageGraph.php';
                            include_once './Commons.php';
                            $processCoverageGraph = new ProcessCoverageGraph($_GET['projectid'], $_GET['storyid'], "TC");
                            $tupleCoverage = $processCoverageGraph->go(true);
                            $tupleCoverageJsArray = json_encode($tupleCoverage);


                            $allCombProcessCoverageGraph = new ProcessCoverageGraph($_GET['projectid'], $_GET['storyid'], "TCAll");
                            $allCombTupleCoverage = $allCombProcessCoverageGraph->go(false);
                            $allCombTupleCoverageJsArray = json_encode($allCombTupleCoverage);
                            
                            $commons = new Commons();
                            
//                            print_r($commons->array_merge_recursive_sequential($tupleCoverage,$allCombTupleCoverage));
                            $mergedArray = json_encode($commons->array_merge_recursive_sequential($allCombTupleCoverage, $tupleCoverage));
//                            print_r($mergedArray);
//                        print_r($tupleCoverage);
                        echo '</pre>';
                            //echo $tupleCoverageJsArray;
                            ?>
                            <div id="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/transition.js"></script>
        <script src="./js/c3js/d3.min.js" charset="utf-8"></script>
        <script src="./js/c3js/c3.min.js"></script>
        <?php
//        include_once './ProcessCoverageGraph.php';
//        $processCoverageGraph = new ProcessCoverageGraph($_GET['projectid'], $_GET['storyid']);
//        $tupleCoverage = $processCoverageGraph->go();
//        $tupleCoverageJsArray = json_encode($tupleCoverage);
        //echo $tupleCoverageJsArray;
        ?>
        <script>
            var tupleCoverage = ['2+'];
<?php echo "var tupleCoverageValues = " . $mergedArray . ";\n"; ?>
<?php echo "var allCombTupleCoverageValues = " . $allCombTupleCoverageJsArray . ";\n"; ?>

            //tupleCoverage = $.merge( $.merge( [], tupleCoverage ), tupleCoverageValues );

            var allTags = [];
            allTags.push.apply(allTags, tupleCoverage);
            allTags.push.apply(allTags, tupleCoverageValues);

            var allPointsForGraph = [];

            var index;
            for (index = 0; index < Math.max(tupleCoverageValues, allCombTupleCoverageValues); ++index) {
                console.log(a[index]);
                allPointsForGraph[index] = tupleCoverageValues[index].concat(allCombTupleCoverageValues[index]);
            }


            var chart = c3.generate({
                bindto: '#chart',
                data: {
                    rows: tupleCoverageValues,
                    type: 'area-spline'
                }
            });

        </script>
    </body>
</html>