<?php
require_once('DBConnector.php');
require_once('Commons.php');
require_once('ProcessMindMap.php');

if(isset($_POST['command'])){
    $cmd = $_POST['command'];
}

if(isset($cmd) && $cmd == 'updaterule'){
//    echo 'update';
    try {
        $rule = json_decode($_POST['rulestring']);
        //var_dump($rule);
        DBConnector::updateRule($rule->ruleId, $_POST['storyid'], $rule->rulePart);
        // file_put_contents("debug.txt", print_r("DBConnector::updateRule called from processRule",true));
        } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

if(isset($cmd) && $cmd == 'insertrule'){
    $ruleId = uniqid('brid_');
    try {
        $rule = json_decode($_POST['rulestring']);
//        var_dump($rule);
        //insert to database
        DBConnector::insertRule($ruleId, $_POST['storyid'], $rule->rulePart);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

if(isset($cmd) && $cmd == 'getrules'){
    try {
        $rules = DBConnector::getRules($_POST['storyid']);
        // try {
        //     file_put_contents("debug.txt", print_r('trying',true));
        $rules=processRulesArrayForAjaxCall($rules,$_POST['projid'],$_POST['storyid']);
        //     file_put_contents("debug.txt", print_r('success',true));
        // } catch (Exception $e) {
        //     file_put_contents("debug.txt", print_r('fail',true));
        // }
        
        // file_put_contents("debug.txt", print_r($rules,true));
        echo json_encode($rules);
    } catch (Exception $ex) {
        // file_put_contents("debug.txt", print_r($ex,true));
    }
}


if(isset($cmd) && $cmd == 'delrule'){
    file_put_contents("debug.txt", print_r($_POST['ruleid'],true));
    try {
//        echo 'delete';
        echo $_POST['ruleid']."fdsdfs ". $_POST['storyid'];
        DBConnector::deleteRule($_POST['ruleid'], $_POST['storyid']);
    } catch (Exception $ex) {

    }
} 



//    //inster to db
//    
//    
//    
//    //decode json
//    try {
////       $rule = json_decode($_POST['rulestring']);
////       var_dump($rule);
////       //search for only_when or only_not_when
////       foreach($rule['rulePart'] as $key => $value){
////           if(is_array($key) && $key['elemId'] === "rule-constraint-only_when"){
////               
////           }else if(is_array($key) && $key['elemId'] === "rule-constraint-only_not_when"){
////               
////           }
////       }
//       
//       
//       //read xml file
//       $xml = simplexml_load_string($_POST['xmlAsString']);
//       var_dump($xml);
//       
//    } catch (Exception $exc) {
//       
//    }



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//steps
//1. query database get storyid

if(isset($_POST['getrules'])){
    // var_dump(DBConnector::getRulesByStoryId(1, 7));
}

if(isset($_POST['rulecount'])){
    // var_dump(DBConnector::getRulesCountOfStory(1));
}

if(isset($_POST['rulesarray'])){
    // var_dump(DBConnector::getRulesIdArray(1));
}

// if(isset($_POST['processrules'])){
//         //read xml string and create simple xml object
//         //$xml = simplexml_load_string($_POST['xmlAsString']);
//         //var_dump($xml);
//     file_put_contents("debug.txt", print_r("cmd processrules is used",true));
//         try{
//             processRules(1, $_POST['xmlAsString'], 'test.xml');
//         }catch(Exception $e){
//             echo $e->getMessage();
//         }
// }

if(isset($_POST['getrules'])){
    // var_dump(DBConnector::getRulesAsArray($_POST['getrules']));
}

function processRulesArrayForAjaxCall($rules,$projid,$storyid){
    // file_put_contents("debug.txt", print_r($rules,true));
    $mmProcessor = new ProcessMindMap($projid, $storyid);
    for ($i = 0; $i < count($rules); $i++) {
        // $ruleText
        for ($j = 0; $j < count($rules[$i]['ruleParts']); $j++) {
            if ($rules[$i]['ruleParts'][$j]=='rule-constraint-only_not_when') {
                $rules[$i]['ruleTextParts'][$j]='Does Not Go With';
            } elseif ($rules[$i]['ruleParts'][$j]=='rule-constraint-only_when') {
                $rules[$i]['ruleTextParts'][$j]='Only Goes With';
            }else{
                $rules[$i]['ruleTextParts'][$j]=$mmProcessor->getElementPathById($rules[$i]['ruleParts'][$j], '');
            }
            
            
        }
    }
    // file_put_contents("debug.txt", print_r($rules,true));
    return $rules;
    // file_put_contents("debug.txt", print_r($rules,true));
}
/**
 * 
 * @param int $storyId story id which should be the rule's story Id
 * @param string $xmlStr well formatted xml string to process according to rules of given story 
 * @param string $filepath path of the xml file to save 
 * @return mixed Returns the number of bytes written or FALSE if an error occurred.
 * @throws Exception
 */
//not used function
// function processRules($storyId, $xmlStr, $filepath){
//     //create xml dom from xml string
//     $xmldoc = new DOMDocument();
//     $xmldoc->loadXML($xmlStr);
//     //get rules ids as an array
//     $rules = DBConnector::getRulesIdArray($storyId);
//     //iterate over rules
//     foreach($rules as $ruleId){
//         //iterate over rules
//         //rule begins
//         //TODO get rule one by one from a collection not from the database
//         $rule = DBConnector::getRulesByStoryId($storyId, $ruleId);
//         //iterate over rule parts
//         for($i = 0; $i < count($rule); $i++){
//             //rule part begins
//             //check part element for only when rule constraint
//             if($rule[$i]['elementId'] === "rule-constraint-only_when" || $rule[$i]['elementId'] === "rule-constraint-only_not_when"){
//                 //store current rule constraint
//                 $ruleCons = null;
//                 if($rule[$i]['elementId'] === "rule-constraint-only_when"){
//                     $ruleCons = "when";
//                 }else if($rule[$i]['elementId'] === "rule-constraint-only_not_when"){
//                     $ruleCons = "whenNot";
//                 }
//                 //property attribute value should add to when attribute
//                 $propertyVal = null;
//                 //get previous part, add property
//                 $prePartId = $rule[$i]['partId'] - 1;
//                 if($prePartId >= 0 && $prePartId < $rule[$i]['partId']){
//                     if(substr($rule[$prePartId]['elementId'], 0, 21) != "rule-element-dragged-"){    
//                         throw new Exception("Not acceptable rule part before the rule constraint");
//                     }
//                     $partid = str_replace("rule-element-dragged-", "", $rule[$prePartId]['elementId']);
//                     $xpath = new DOMXPath($xmldoc);
//                     $node = $xpath->query("//*[@id='$partid']")->item(0);
//                     if($node->getAttribute('property')){
//                         //property attribute is present
//                         $propertyVal = preg_replace('/\s+/', '', $node->getAttribute('property'));
//                     }else{
//                         //property attribute is not present
//                         //add property attribute
//                         $propertyVal = 'property'.preg_replace('/\s+/', '', $node->getAttribute('name'));
//                         $node->setAttribute('property', $propertyVal);
//                     }
//                 }else{
//                     //incorrect format of the rule
//                     //there is no parts before rule constraint
//                     throw new Exception("There is no rule part before rule constraint");
//                 }
//                 //get next part add rule constraint
//                 $nextPartId = $rule[$i]['partId'] + 1;
//                 if($nextPartId > $rule[$i]['partId'] && $nextPartId < count($rule)){
//                     if(substr($rule[$prePartId]['elementId'], 0, 21) != "rule-element-dragged-"){
//                         throw new Exception("Not acceptable rule part after the rule constraint");
//                     }
//                     $partid = str_replace("rule-element-dragged-", "", $rule[$nextPartId]['elementId']);
//                     $xpath = new DOMXPath($xmldoc);
//                     $node = $xpath->query("//*[@id='$partid']")->item(0);
//                     if($node->hasAttribute($ruleCons)){
//                         //when attribute is present
//                         $whenVal = $node->getAttribute($ruleCons);
//                         $valueArr = explode(",", $whenVal);
//                         array_push($valueArr, $propertyVal);
//                         $valueStr = implode(", ", $valueArr);
//                         $node->setAttribute($ruleCons, $valueStr);
//                     }else{
//                         $node->setAttribute($ruleCons, $propertyVal);
//                     }
//                 }else{
//                     //incorrect format of the rule
//                     //there is no parts after rule constraint
//                     throw new Exception("There is no rule part after rule constraint");
//                 }
//             }
//             //rule part ends
//         }
//         //rule ends
//     }
//     //remove ids of elements
//     $xpath = new DOMXPath($xmldoc);
//     //get all nodes
//     $nodes = $xpath->query("//*");
//     foreach($nodes as $node){
//         $node->removeAttribute('id');
//     }
//     return $xmldoc->save($filepath);
// }