<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logger
 *
 * @author samarakoon
 */

/*
 * Logger sample call:-
 * Logger::log(LogType::error, "Boo");
 */

class Logger {

    static function log($prefix, $message) {
        $filePath = "./logs/oscar.log";

        $timeStamp = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
        
        
        echo '<script type="text/javascript">';
        echo "bootstrap_alert('', 'Aw Snap! Something broke. We are looking into this right now' + status, 'danger', 4000);";
        echo '</script>';

        error_log($timeStamp . ' | ' . $prefix . ' | ' . $message . "\r\n", 3, $filePath);
    }

}

abstract class LogType {
    const error = "ERROR";
    const info = "INFO";
    const debug = "DEBUG";
}
