<?php
 require_once './DBConnector.php';
 require_once './ProcessTuples.php';
 
$projId="069f4f43f414a01472b3b3a55c18e4dd";
$storyId="734f5b219cae31fb77d239ffb573f6ef";
$ruleId="brid_5406daa7c03cf";
$inputMap = json_decode(DBConnector::getMindMap($projId, $storyId));

function createXML($inputMap){
    $inputXML = new SimpleXMLElement("<Input></Input>");
    $xmlDocument=createXMLProcessor($inputMap->mindmap->root, $inputXML);
}

function createXMLProcessor($node, $xmlDocument){
    if (is_array($node)) {

            foreach ($node as $value) {
                //print_r($node);
                $xmlDocument = $this->createXMLInput($value, $xmlDocument);
            }
        } else {
            print_r($node->children);
            echo "</br>";
            print_r($node->children->children);
            echo "</br>";
            if(count($node->children)>0){
                if(count($node->children->children)>0){
                    // node VarSet
                    $xmlDocument = $xmlDocument->addChild("VarSet");
                    $xmlDocument->addAttribute('name',$inputMapNode->text->caption);
                }
                else{
                    //node var
                    $xmlDocument = $xmlDocument->addChild("Var");
                    $xmlDocument->addAttribute('name',$inputMapNode->text->caption);
                }
            }
            else{
                //node val
                $xmlDocument = $xmlDocument->addChild("Value");
                $xmlDocument->addAttribute('name',$inputMapNode->text->caption);
            }
            $xmlDocument = $this->createXMLProcessor($node->children, $xmlDocument);
        }
        return $xmlDocument;
    }

function createXMLInput($inputMapNode, $xmlDocument){
    // file_put_contents("debugXML.txt", print_r( $xmlDocument->asXML(),true),FILE_APPEND);
    if (isset($inputMapNode->children)&&count($inputMapNode->children)>0) {
        for ($i=0; $i < count($inputMapNode->children); $i++) {
            $myElement=$xmlDocument;
            if (isset($inputMapNode->children[$i]->children)&&count($inputMapNode->children[$i]->children)>0) {
                if (isset($xmlDocument->VarSet)&&$xmlDocument->VarSet['name']==$inputMapNode->text->caption) {
                    $myElement=$myElement->VarSet;
                }
                else{  
                    $myElement = $xmlDocument->addChild("VarSet");
                    $myElement->addAttribute('name',$inputMapNode->text->caption);
                }
            } elseif (isset($inputMapNode->children[$i]->children)&&count($inputMapNode->children[$i]->children)==0) {
                 if (isset($xmlDocument->Var)&&$xmlDocument->Var['name']==$inputMapNode->text->caption) {
                    $myElement=$myElement->Var;
                }
                else{
                    $myElement = $xmlDocument->addChild("Var");
                    $myElement->addAttribute('name',$inputMapNode->text->caption); 
                }
            }
            createXMLInput($inputMapNode->children[$i], $myElement);
        }
    }elseif (isset($inputMapNode->children)&&count($inputMapNode->children)==0) {
        echo "child added ".$inputMapNode->text->caption."</br>";
        $myElement = $xmlDocument->addChild("Value");
        $myElement->addAttribute('name',$inputMapNode->text->caption); 
    }
}

//     if (isset($inputMapNode->children)&&count($inputMapNode->children)>0) {
//         for ($i=0; $i < count($inputMapNode->children); $i++) {
//             $myElement=$xmlDocument;
//             if (isset($inputMapNode->children[$i]->children)&&count($inputMapNode->children[$i]->children)>0) {
//                 if (isset($xmlDocument->VarSet)&&$xmlDocument->VarSet['name']==$inputMapNode->text->caption) {
//                     $myElement=$myElement->VarSet;
//                 }
//                 else{  
//                     $myElement = $xmlDocument->addChild("VarSet");
//                     $myElement->addAttribute('name',$inputMapNode->text->caption);
//                 }
//             } elseif (isset($inputMapNode->children[$i]->children)&&count($inputMapNode->children[$i]->children)==0) {
//                  if (isset($xmlDocument->Var)&&$xmlDocument->Var['name']==$inputMapNode->text->caption) {
//                     $myElement=$myElement->Var;
//                 }
//                 else{
//                     $myElement = $xmlDocument->addChild("Var");
//                     $myElement->addAttribute('name',$inputMapNode->text->caption); 
//                 }
//             }
//             createXMLInput($inputMapNode->children[$i], $myElement);
//         }
//     }elseif (isset($inputMapNode->children)&&count($inputMapNode->children)==0) {
//         echo "child added ".$inputMapNode->text->caption."</br>";
//         $myElement = $xmlDocument->addChild("Value");
//         $myElement->addAttribute('name',$inputMapNode->text->caption); 
//     }
// }

// print_r($inputMap);
// file_put_contents("debugXML.txt", print_r( $inputMap,true));
// createXMLInput($inputMap->mindmap->root,$inputXML);
// file_put_contents("debugXML.txt", print_r( $inputXML->asXML(),true));

    createXML($inputMap);
    

?>