<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class XmlParser {

	public function parseXmlToJson ($xmlstring) {

		$xmlstring = str_replace(array("\n", "\r", "\t"), '', $xmlstring);

		$xmlstring = trim(str_replace('"', "'", $xmlstring));

		$simpleXml = simplexml_load_string($xmlstring);

		$json = json_encode($simpleXml, true);

		return $json;

	}

	public function parseXmlToJsonFromFile ($url) {

		$fileContents= file_get_contents($url);

		$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);

		$fileContents = trim(str_replace('"', "'", $fileContents));

		$simpleXml = simplexml_load_string($fileContents);

		$json = json_encode($simpleXml, true);

		return $json;

	}

	public function parseXmlToObjectFromFile ($url) {

		$fileContents= file_get_contents($url);

		$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);

		$fileContents = trim(str_replace('"', "'", $fileContents));

		$simpleXml = simplexml_load_string($fileContents);

		return $simpleXml;

	}

}

?>