<html>
 <head>
  <title>PHP Test</title>
 </head>
 <body>
 <?php
$file="xmlfile.xml";
$xml = simplexml_load_file($file) or die ("no load");
echo "<table>
  <tr>
    <th>Title</th>
    <th>Year</th>
    <th>Producers/Director</th>
    <th>Stars</th>
    <th>Budget</th>
    <th>MST3000</th>
  </tr>";
foreach($xml->movie as $movie){
  echo "<tr>";
  echo "<td>{$movie->title}</td>";
  echo "<td>{$movie->year}</td>";
  echo "<td>{$movie->director->producer}</td>";
  echo "<td>";
  foreach($movie->stars->star as $stars){
    echo $stars[0] . '<br/>';
  }
  echo "</td>";
  echo "<td>{$movie->budget}</td>";
  echo "<td>{$movie->mst}</td>";
  echo "<br />";
  echo "</tr>";
}
echo '</table>';
?>
 </body>
</html>