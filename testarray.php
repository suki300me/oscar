<? php 
//Db connector
public static function getTuplesByStoryId($storyId, $ruleId) {
        $pdo = Database::connect();
        $sql = "SELECT * FROM genrators WHERE genStoryId = :storyid AND genRuleId = :ruleid ORDER BY genPartId ASC";
        $stmt = $pdo->prepare($sql);
        try {
            //execute with given parameters
            $stmt->execute(array(
                ':storyid' => $storyId,
                ':ruleid' => $ruleId));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            return null;
        } finally {
            Database::disconnect();
        }
    }

     public static function getTuplesAsArray($storyId) {
        $rulesArr = array();
        $pdo = Database::connect();
        $sql = "SELECT genElementId, genText FROM generators WHERE genStoryID = :storyid AND genRuleId = :ruleid ORDER BY genPartId ASC";
        $stmt = $pdo->prepare($sql);
        $rules = DBConnector::getTuplesIdArray($storyId);
        //bind parameters
        $stmt->bindParam(':ruleid', $idRule, PDO::PARAM_INT);
        $stmt->bindParam(':storyid', $stryId, PDO::PARAM_STR);
        foreach ($rules as $rule) {
            try {
                $idRule = $rule;
                $stryId = $storyId;
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                //var_dump(array_column($result, 'text', 'elementId'));
                //var_dump(array_column($rulesArr, "text"));
                //var_dump(array_column($rulesArr, 'text', 'elementId'));
                //array_push($rulesArr, implode(" ", $result));
                $keyValArr = array_column($result, 'text', 'elementId');
                array_push($rulesArr, $keyValArr);
            } catch (PDOException $e) {
                Database::disconnect();
            }
        }
        Database::disconnect();
        return $rulesArr;
    }

     public static function getTuplesIdArray($storyId) {
        $pdo = Database::connect();
        $sql = "SELECT genRuleId FROM generators WHERE genStoryId = :storyid";
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute(array(':storyid' => $storyId));
            $result = $stmt->fetchAll(PDO::FETCH_COLUMN | PDO::FETCH_UNIQUE, 0);
            return $result;
        } catch (PDOException $e) {
            return null;
        } finally {
            Database::disconnect();
        }
    }
    ?>