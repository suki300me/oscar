<?php

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of ProcessTcasesInputs
*
* @author samarakoon
*/
class ProcessTcasesInputs {

private $storyId;
private $projId;
private $mmProcessor;

public function __construct($projId, $storyId) {
$this->projId = $projId;
$this->storyId = $storyId;
$this->mmProcessor = new ProcessMindMap($this->projId, $this->storyId);
}

/**
* 
* @param int $storyId story id which should be the rule's story Id
* @param string $xmlStr well formatted xml string to process according to rules of given story 
* @param string $filepath path of the xml file to save 
* @return mixed Returns the number of bytes written or FALSE if an error occurred.
* @throws Exception
*/
function processRulesWhenTupleConstraintMinus($xmldoc){
    $tuples = DBConnector::getTuples($this->storyId);
    $rootConstraint = 2;
    $simpleXmlDoc=simplexml_import_dom($xmldoc);
    $failureTrueArray=array();
    foreach ($tuples as $tuple) {
        $tupleElements = explode(',', $tuple['nodeId']);
        $tupleConstraint = $tuple['tupleConstraint'];
        if ($tupleConstraint==-1) {
            foreach ($tupleElements as $tupleElement) {
                $elementPath=$this->mmProcessor->getElementPathById($tupleElement, '-');
                $failureTrueArray[]=$elementPath;
            }
        }        
    }    
    $fXPath='/System[@name="Tcases"]/Function[@name="TC"]/Input';
    foreach ($failureTrueArray as $failureTrueVal) {
        $eleInFailureTrueVal=explode('.', $failureTrueVal);        
        for ($varCount=count($eleInFailureTrueVal); $varCount >2 ; $varCount--) { 
            $fXPath=$fXPath.'/VarSet[@name="%s"]';            
        }
        $fXPath=$fXPath.'/Var[@name="%s"]/Value[@name="%s"]';
        $fXPathUpdated=vsprintf($fXPath, $eleInFailureTrueVal);       
        $itemList=$simpleXmlDoc->xpath($fXPathUpdated);
        $itemList[0]->addAttribute('failure', 'true');        
    }    
    return $simpleXmlDoc;
}

function createGenerators($function) {
    $tuples = DBConnector::getTuples($this->storyId);
    $rootConstraint = 2;
    $generatorsXML = new SimpleXMLElement("<Generators></Generators>");
    $tupleGenerator = $generatorsXML->addChild('TupleGenerator');
    $tupleGenerator->addAttribute('function', $function);
    foreach ($tuples as $tuple) {
        $tupleElements = explode(',', $tuple['nodeId']);
        $tupleConstraint = $tuple['tupleConstraint'];
        if ($tupleConstraint>-1) {
            $combine = $tupleGenerator->addChild("Combine");
            $combine->addAttribute("tuples", $tupleConstraint);
            foreach ($tupleElements as $tupleElement) {
                if ($tupleElement == $this->mmProcessor->getInputMap()->mindmap->root->id) {//this line is causes a error
                    $rootConstraint = $tupleConstraint;
                } else {
                    $include = $combine->addChild("Include");
                    $include->addAttribute("var", $this->mmProcessor->getElementPathById($tupleElement, '-'));
                }
            }
        }
    }
    $tupleGenerator->addAttribute('tuples', $rootConstraint);
    $generatorsXML->saveXML("tcases\bin\TC-Generators.xml");
}

//can't use this function yet because $xmlStr has a id attribute now. Also $xmlStr should be provided dynamicaly not like from db as of now
function processRules($storyId, $xmlStr, $filepath){
        // $mmProcessor = new ProcessMindMap($projId, $storyId);
        $rules = DBConnector::getRules($storyId);
        // file_put_contents("debug.txt", print_r($xmlStr,true));
        $xmlStr=simplexml_load_string($xmlStr);
        // $inputXML = new SimpleXMLElement($xmlStr);        
        $rulesArray= array();      
        $ruleCons=array('rule-constraint-only_when','rule-constraint-only_not_when');        
        for ($i=0; $i < count($rules); $i++) {
            //iterate over rules
            $ruleArray=array();
            $ruleCon;
            $rulesFirstPartArray=array();
            $rulesSecondPartArray=array();
            $secondPart= false;
            for ($j=0; $j < count($rules[$i]['ruleParts']); $j++) {
                // iterate over rule parts
                if (!$secondPart) {
                    //still before/in the constraint part
                    if (in_array($rules[$i]['ruleParts'][$j], $ruleCons)) {
                        //in the contraint part
                        $ruleCon=$rules[$i]['ruleParts'][$j];//init the constraint
                        $secondPart=true;                        
                    } else {
                        array_push($rulesFirstPartArray,$rules[$i]['ruleParts'][$j]);
                    }                    
                } else {
                    //after the constraint part
                    //put the element in rulesSecondPartArray array
                    array_push($rulesSecondPartArray,$rules[$i]['ruleParts'][$j]);
                }  
            }
            //build the rule
            foreach ($rulesFirstPartArray as $fkey => $fvalue) {
                $ruleArray[$fvalue]=array($ruleCon=>array());
                foreach ($rulesSecondPartArray as $skey => $svalue) {
                    array_push($ruleArray[$fvalue][$ruleCon],$svalue);
                }
            }
            // merge all the rules together     
            $rulesArray=array_merge_recursive($ruleArray,$rulesArray);
        }
        //start processing the xml
        foreach ($rulesArray as $key => $value) {
            $fpart=$this->mmProcessor->getElementPathById($key, '-');
            $spart=$value;
            $fpartAsArray=explode('.',$fpart);
            $fXPath='/System[@name="Tcases"]/Function[@name="TC"]/Input';
            for ($varCount=count($fpartAsArray); $varCount >2 ; $varCount--) { 
                $fXPath=$fXPath.'/VarSet[@name="%s"]';            
                }
            $fXPath=$fXPath.'/Var[@name="%s"]/Value[@name="%s"]';
            $fXPathUpdated=vsprintf($fXPath, $fpartAsArray);
            foreach ($spart as $skey => $svalue) {
                $conName=$skey;
                $conElements=array();
                foreach ($svalue as $srkey => $srvalue) {
                    $conElement=$fpart=$this->mmProcessor->getElementPathById($srvalue, '-');
                    array_push($conElements, $conElement);
                }
                $itemList=$xmlStr->xpath($fXPathUpdated);
                $conElementsStr=str_replace('.', '', implode (", ", $conElements));
                if ($conName=='rule-constraint-only_when') {
                    $itemList[0]->addAttribute('when', $conElementsStr);
                } else if($conName=='rule-constraint-only_not_when'){
                    $itemList[0]->addAttribute('whenNot', $conElementsStr);
                }  
            }
        }
        // change the xml according to pririty rule constraint =-1
        $xmlStr=Self::processRulesWhenTupleConstraintMinus($xmlStr);
        
        return $xmlStr->saveXML($filepath);

    }

function processRulesOld($storyId, $xmlStr, $filepath) {

// file_put_contents("debug.txt", print_r($xmlStr,true));

//create xml dom from xml string
$xmldoc = new DOMDocument();
$xmldoc->loadXML($xmlStr);
//get rules ids as an array
$rules = DBConnector::getRulesIdArray($storyId);
//iterate over rules
foreach ($rules as $ruleId) {
//iterate over rules
//rule begins
//TODO get rule one by one from a collection not from the database
$rule = DBConnector::getRulesByStoryId($storyId, $ruleId);
//iterate over rule parts
for ($i = 0; $i < count($rule); $i++) {
//rule part begins
//check part element for only when rule constraint
if ($rule[$i]['elementId'] === "rule-constraint-only_when" || $rule[$i]['elementId'] === "rule-constraint-only_not_when") {
//store current rule constraint
$ruleCons = null;
if ($rule[$i]['elementId'] === "rule-constraint-only_when") {
    $ruleCons = "when";
} else if ($rule[$i]['elementId'] === "rule-constraint-only_not_when") {
    $ruleCons = "whenNot";
}
//property attribute value should add to when attribute
$propertyVal = null;
//get previous part, add property
$nextPartId = $rule[$i]['partId'] + 1;
if ($nextPartId > $rule[$i]['partId'] && $nextPartId < count($rule)) {
    if (substr($rule[$nextPartId]['elementId'], 0, 21) != "rule-element-dragged-") {
        throw new Exception("Not acceptable rule part before the rule constraint");
    }
    $partid = str_replace("rule-element-dragged-", "", $rule[$nextPartId]['elementId']);
    $xpath = new DOMXPath($xmldoc);
    $node = $xpath->query("//*[@id='$partid']")->item(0);
    if ($node->getAttribute('property')) {
        //property attribute is present
        $propertyVal = preg_replace('/\s+/', '', $node->getAttribute('property'));
    } else {
        //property attribute is not present
        //add property attribute
        $propertyVal = 'property' . preg_replace('/\s+/', '', $node->getAttribute('name'));
        $node->setAttribute('property', $propertyVal);
    }
} else {
    //incorrect format of the rule
    //there is no parts before rule constraint
    throw new Exception("There is no rule part before rule constraint");
}
//get next part add rule constraint
$prePartId = $rule[$i]['partId'] - 1;
if ($prePartId >= 0 && $prePartId < $rule[$i]['partId']) {
    if (substr($rule[$prePartId]['elementId'], 0, 21) != "rule-element-dragged-") {
        throw new Exception("Not acceptable rule part after the rule constraint");
    }
    $partid = str_replace("rule-element-dragged-", "", $rule[$prePartId]['elementId']);
    $xpath = new DOMXPath($xmldoc);
    $node = $xpath->query("//*[@id='$partid']")->item(0);
    if ($node->hasAttribute($ruleCons)) {
        //when attribute is present
        $whenVal = $node->getAttribute($ruleCons);
        $valueArr = explode(",", $whenVal);
        array_push($valueArr, $propertyVal);
        $valueStr = implode(", ", $valueArr);
        $node->setAttribute($ruleCons, $valueStr);
    } else {
        $node->setAttribute($ruleCons, $propertyVal);
    }
} else {
    //incorrect format of the rule
    //there is no parts after rule constraint
    throw new Exception("There is no rule part after rule constraint");
}
}
//rule part ends
}
//rule ends
}
//remove ids of elements
$xpath = new DOMXPath($xmldoc);
//get all nodes
$nodes = $xpath->query("//*");
foreach ($nodes as $node) {
$node->removeAttribute('id');
}
// modify the $xmldoc with tcases failure=true attribute
// NOTE: modifiedXMLdoc is a $simpleXML doc while $xmldoc is a DOM element
// file_put_contents("debug.txt", print_r($xmldoc,true));
$modifiedXMLdoc=Self::processRulesWhenTupleConstraintMinus($xmldoc);
// write the modified xml as input file
// return $xmldoc->save($filepath);
// $modifiedXMLdoc->saveXML("debug1.xml"); 
return $modifiedXMLdoc->saveXML($filepath);
}

}
?>