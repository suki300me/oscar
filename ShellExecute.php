<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShellExecute
 *
 * @author samarakoon
 */
class ShellExecute {

    public static function execute($cmd, $input = '') {
        $proc = proc_open($cmd, array(0 => array('pipe', 'r'), 1 => array('pipe', 'w'), 2 => array('pipe', 'w')), $pipes);
        fwrite($pipes[0], $input);
        fclose($pipes[0]);
        $stdout = stream_get_contents($pipes[1]);
        fclose($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        $rtn = proc_close($proc);
        return array('stdout' => $stdout,
            'stderr' => $stderr,
            'return' => $rtn
        );
    }

    //var_export(my_exec('echo -e $(</dev/stdin) | wc -l', 'h\\nel\\nlo'));
}
