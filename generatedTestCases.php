<!DOCTYPE html>
<?php
require_once './ProcessMindMap.php';
require_once ("./TcasesGenerator.php");
require_once ("./TestCaseProcessor.php");
require_once ("./ProcessTcasesInputs.php");
require_once('./DBConnector.php');
require_once('./Commons.php');
?>

<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link href="./css/bootstrap-responsive.css" rel="stylesheet">-->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="./img/favicon.png">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="dashboard/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="css/jquery-ui.css">
        <link href="./css/oscar-styles.css" rel="stylesheet">

        <script src="./js/jquery-1.10.2.js"></script>
        <script src="./js/jquery-ui.js"></script>
        <script src="./js/Commons.js"></script>

        <style id="holderjs-style" type="text/css"></style>

        <script>
            $(document).ready(function() {
                hideWindowLoader();
            });
        </script>

    </head>

    <body class="fill">

        <?php
        include('documentHeader.php');
        ?>

        <script>
            document.getElementById("generatedTestCasesHeading").className = "active";
        </script>
        <div id="sb-site" style="height: 100%;">
            <div class="container-fluid fill">
                <div class="row fill">

                    <div class="col-xs-12 main" style="height: calc(100% - 60px);">
                        <!--Begins edit-->

                        <?php
                        file_put_contents("debug.txt", print_r( "processing rules\n",true));
                        //process rules
                        if (isset($_GET['storyid'])) {
                            // file_put_contents("debug.txt", print_r( "getting xml from db\n",true),FILE_APPEND);
                            // $xml = DBConnector::getXML($_GET['storyid']);
                            // file_put_contents("debug.txt", print_r( "printing xml\n",true),FILE_APPEND);
                            // file_put_contents("debug.txt", print_r( $xml,true));
                            $mindMapProcessor = new ProcessMindMap($_GET['projectid'], $_GET['storyid']);
                            // try{
                            $xml = $mindMapProcessor->getTcasesInputXML();
                            // }catch(Exception $e) {
                            //         // echo $e->getMessage();
                                // file_put_contents("debug.txt", print_r( $e,true));
                            //     die;
                                    
                            // }
                            file_put_contents("debug.txt", print_r( "xml\n".$xml,true));
                            $tcasesInputsProcessor = new ProcessTcasesInputs($_GET['projectid'],$_GET['storyid']);
                            
                            try {
                                $tcasesInputsProcessor->processRules($_GET['storyid'], $xml, './tcases/bin/TC-Input.xml');
                                $tcasesInputsProcessor->createGenerators("TC", '2');
                            } catch (Exception $e) {
                                echo $e->getMessage();
                            }
                        }
                        ?>

                        <!-- end of  edit-->

                        <div  class="fill">
                            <div class="page-header">
                                <center>
                                    <h1>
                                        Test cases generated for your story
                                        <small>
                                            <button class="btn btn-default pull-right" data-toggle="modal" data-target="#displayAllCombinations" id="all-combinations">Before Optimization</button>
                                            <a class="btn btn-default pull-right" href="./TestCaseToExcel.php" target="_blank" id="excel-download">Download</a>
                                            <button class="btn btn-default pull-right" data-toggle="modal" data-target="#displayCoverageGraph" id="coverage-graph">Coverage Graph</button>
                                            <!--<a class="btn btn-default pull-right" href="./CoverageGraph.php?<?php $_GET['storyid'] ?>" target="_blank" id="excel-download">Download</a>-->
                                        </small>
                                    </h1>
                                </center>
                            </div>
                            <div style="overflow: auto; height: calc(100% - 40px);">
                                <?php
                                $tcasesGenerator = new TcasesGenerator();
                                $execStatus = $tcasesGenerator->writeTcasesToFile();
                                if ($execStatus) {
                                    echo '<table class="table tableSorter">';
                                    $testCaseProc = new TestCaseProcessor("TC");
                                    print_r($testCaseProc->createTestCaseTable());
                                    echo '</table>';
                                } else {
                                    echo "Error Occured";
                                }
                                ?>
                                <?PHP
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('Sidebar.php');
        ?>

        <!--Edit for run file -->




        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
        <!--<script src="js/docs.min.js"></script>-->
        <!-- Removable code. This is to show all possible combinations -->
        <div class="modal fade" id="displayAllCombinations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: calc(100% - 80px); height: calc(100% - 60px); ">
                <div class="modal-content fill">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">All Possible Combinations</h4>
                    </div>
                    <div class="modal-body" style="height: calc(100% - 120px); ">
                        <!--<iframe  src="./tcases/bin/TCAll-Test.xml" style="display:block; width:100%;  min-height: 60%;" name="frame2" id="frame2" frameborder="0" marginwidth="0" marginheight="0" scrolling="auto" onload="" allowtransparency="false"></iframe>-->
                        <div class="fill" style="overflow: auto;">
                            <?php
                            if ($execStatus) {
                                echo '<table class="table tableSorter">';
                                $testCaseProc = new TestCaseProcessor("TCAll");
                                print_r($testCaseProc->createTestCaseTable());
                                echo '</table>';
                            } else {
                                echo "Error Occured";
                            }
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="displayCoverageGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Coverage Graph</h4>
                    </div>
                    <div class="modal-body">
                        <?php // require_once './CoverageGraphInclude.php'; ?>
                        <script>
//                        $(document).ready(function(){
//                          $("#ttteesstt").click(function(){
//                            $("#mainData1234567890").load("http://www.w3schools.com/jquery/demo_ajax_load.asp");
//                          });
//                        });
                        </script>
                        <center><h1>Coverage Graph</h1></center>
                        <div class="container">
                            <div id="mainData1234567890" class="col-xs-11 main vcenter">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

    </body>
    <script src="./js/tsort.min.js"></script>
    <script type="text/javascript">

//            $(document).ready(function() {
//                $('#chart').load('CoverageGraph.php');
//            });

//            $(document).ready(function() {
//
//                $('table').tableSort({
//                    animation: 'slide',
//                    speed: 500
//                });
//            });
    </script>
</html>