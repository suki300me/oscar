<!DOCTYPE html>

<!-- saved from url=(0044)http://getbootstrap.com/examples/dashboard/# -->
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="./css/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="./css/jquery-ui.css">
        <link href="./css/oscar-styles.css" rel="stylesheet">

        <script src="./js/jquery-1.10.2.js"></script>
        <script src="./js/jquery-ui.js"></script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style id="holderjs-style" type="text/css"></style>



    </head>

    <body style="">

        <?php
        include('documentHeader.php');
        ?>

        <script>
            //document.getElementById("requirementsHeading").className = "active";
        </script>
        <ul id="info1">
            <li>Put anything in the field below.</li>
        </ul>
        <div class="row">
            <center><h1>ROI Calculator</h1></center>
            <div class="container">

                <div id="mainData" class="col-xs-12 main vcenter">
                    <!-- multistep form -->
                    <form id="roiForm">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active">Available Testers</li>
                            <li>Social Profiles</li>
                            <li>Personal Details</li>
                        </ul>
                        <!-- fieldsets -->
                        <fieldset>
                            <h2 class="fs-title">CURRENT TEST DESIGN TIME</h2>
                            <h3 class="fs-subtitle">Step 1</h3>
                            <input type="text" name="timeForTestDesign" placeholder="Time in hrs" />
<!--                            <input type="password" name="pass" placeholder="Password" />
                            <input type="password" name="cpass" placeholder="Confirm Password" />-->
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </fieldset>
                        <fieldset>
                            <h2 class="fs-title">HOW MANY AVAILABLE TESTERS?</h2>
                            <h3 class="fs-subtitle">Step 2</h3>
                            <input type="text" name="testersCount" placeholder="Number of testers" />
                           <!--<input type="text" name="gplus" placeholder="Google Plus" />-->
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </fieldset>
                        <fieldset>
                            <h3 class="fs-subtitle">Step 3</h3>
                            <h2 class="fs-title">CURRENT NUMBER OF TEST CASES</h2>
                            <input type="text" name="testCaseCount" placeholder="Number of testcases" />
                            <h2 class="fs-title">TEST EXECUTION TIME</h2>
                            <input type="text" name="timeForTestExection" placeholder="Time in hrs" />
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <!--<input type="submit" name="submit" class="submit action-button" value="Submit" />-->
                            <input type="submit" name="submit" value="Submit" class="action-button">
                        </fieldset>
                    </form>

                    <!-- jQuery -->
                    <script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js" type="text/javascript"></script>
                    <!-- jQuery easing plugin -->
                    <script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
                    <script src="./js/roi.js" type="text/javascript"></script>
                    <link href="./css/roi.css" rel="stylesheet">
                </div>
                <center> 
                    <div id="roiResult" class="well" hidden="true" style="width: 300px; position: relative; margin-top:20px" ></div>
                </center>

            </div>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/transition.js"></script>
        <script>
            $('#roiForm').submit(function(event) {
                event.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: 'processROI.php',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $('#mainData').hide();
                        $('#roiResult').show();
                        $('#roiResult').html("<br><h3>ROI value</h3>  <h5>"+data.msg+"</h5>");
                    }
                });
            });
        </script>
    </body>
</html>