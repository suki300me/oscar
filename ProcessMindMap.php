<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProcessMindMap
 *
 * @author samarakoon
 */
require_once('DBConnector.php');

class ProcessMindMap {

    private $inputMap;

    function __construct($projectid, $storyid) {
        try {
            $this->inputMap = json_decode(DBConnector::getMindMap($projectid, $storyid));            
        } catch (Exception $e) {
            echo("<script>console.log('PHP: " . $e->getMessage() . "');</script>");
        }
    }

    public function getInputMap() {
        return $this->inputMap;
    }


    //returns the tcases input xml
    // Magic. Do not touch.
    function getTcasesInputXML(){
        $inputXML = new SimpleXMLElement('<System name="Tcases"></System>');
        // add the common elements to $inputXML
        $function=$inputXML->addChild('Function');
        $function->addAttribute('name','TC');
        $input=$function->addChild('Input');
        $varSet=$input->addChild("VarSet");
        $varSet->addAttribute('name',str_replace(" ", "-", $this->inputMap->mindmap->root->text->caption));
        // $varSet->addAttribute('id',str_replace(" ", "-", $this->inputMap->mindmap->root->id));

        // pass the first children array and input element of input->rootVarSet xml node to recursive function
        $this->inputXMLProccesor($this->inputMap->mindmap->root->children, $inputXML->Function->Input->VarSet);
        return $inputXML->asXML();
    }

    // recursive function to make the tcases-input xml    
    function inputXMLProccesor($node, $xmlDocument){
        // check for the children array 
        if (is_array($node)) {
            // iterate through children array
            foreach ($node as $keyChild => $valueChild) {            
                if (count($valueChild->children)>0) {
                    if (count($valueChild->children[0]->children)>0) {
                        // VarSet
                        $newNode=$xmlDocument->addChild("VarSet");                    
                    } else {
                        // Var
                        $newNode=$xmlDocument->addChild("Var");
                    }                
                }
                else{
                    // value
                    $newNode=$xmlDocument->addChild("Value");
                    // get the path of the element for property attribute
                    $elementPath=$this->getElementPathById($valueChild->id,'-');
                    $newNode->addAttribute('property',str_replace('.', '', $elementPath));
                }
                $newNode->addAttribute('name',str_replace(" ", "-", $valueChild->text->caption));
                // $newNode->addAttribute('id',str_replace(" ", "-", $valueChild->id));
                $this->inputXMLProccesor($valueChild->children, $newNode);
            }
        } 
    }
    /*
     * This will list paths of all the child variables belongs to the given parent
     * ** Pass null if you want to list paths of all the child variables belongs to the root
     * Eg:-
     * Array
      (
      [0] => Find A Dentist.My Network is
      [1] => Find A Dentist.Looking For.Speciliaties
      [2] => Find A Dentist.Located Within.Distance.Miles
      [3] => Find A Dentist.Dental Plan
      )
     */

    function getChildrenPaths($path, $replaceSpaces) {
        $innerObject = $this->getMindMapNodeForPath($path);
        if ($path != null) {
            $path+='.';
        }
        return $this->getChildVars($innerObject, $path, array(), $replaceSpaces);
    }

    /*
     * This is the processor for @getChildrenPaths. Do not call this method directly. Call this through @getChildrenPaths
     */

    function getChildVars($node, $path, $childrenArray, $replaceSpaces) {
        $tempPath = '';

        if (is_array($node)) {
            foreach ($node as $value) {
                $childrenArray = $this->getChildVars($value, $path, $childrenArray, $replaceSpaces);
            }
        } else {
            foreach ($node as $key => $value) {
                if ($key == 'text') {
                    $tempPath = $value->caption . ".";
                }

                if ($key == 'children') {

                    if (empty($value)) {
                        $currntPath = substr($path, 0, -1);
//                        $currntPath = str_replace(' ', '-', $currntPath);
                        //echo $currntPath.'<br/>';
                        if ($replaceSpaces) {
                            $currntPath = str_replace(" ", "-", $currntPath);
                        }
                        array_push($childrenArray, $currntPath);
                    } else {
                        $path = $path . $tempPath;
                        $childrenArray = $this->getChildVars($value, $path, $childrenArray, $replaceSpaces);
                    }
                }
            }
        }
        return array_unique($childrenArray);
//        return $childrenArray;
    }

    public function tempCaller() {
        return $this->pharseInputValueMatrix($this->inputMap->mindmap->root, "", new InputValuesHolder());
    }

    /*
     * This method returns an array paths to each variable and array of applicable values per each variable
     * Eg:-
     * Output :-
     *      Array(
     *          TravelSystem.Customer => [Standard, Privilaged]
     *          TravelSystem.Travel-Type => [Domestic, International]
     *      )
     */

    public function pharseInputValueMatrix($node, $path, $inputValueHolder) {
        $tempPath = '';

        if (is_array($node)) {

            foreach ($node as $value) {
                //print_r($node);
                $inputValueHolder = $this->pharseInputValueMatrix($value, $path, $inputValueHolder);
            }
        } else {

            foreach ($node as $key => $value) {


                if ($key == 'text') {
                    $tempPath = $value->caption . ".";
                }

                if ($key == 'children') {

                    if (empty($value)) {

                        $currntPath = substr($tempPath, 0, -1);
                        $currntPath = str_replace(' ', '-', $currntPath);
                        array_push($inputValueHolder->innerArray, $currntPath);
                    } else {

                        $this->pharseInputValueMatrix($value, $path . $tempPath, $inputValueHolder);

                        if (sizeof($inputValueHolder->innerArray) > 0) {
                            $currntPath = substr($path . $tempPath, 0, -1);
                            $currntPath = str_replace(' ', '-', $currntPath);
                            $inputValueHolder->outerArray[$currntPath] = $inputValueHolder->innerArray;
                        }

                        $inputValueHolder->innerArray = array();
                    }
                }
            }
        }
        return $inputValueHolder;
    }

    /*
     * Takes variable path in A.B.C.D format and returns the applicable object node
     * Eg:-
     * Input: getMindMapNodeForPath("TravelSystem.Customer")
     * Returns: PHP Object inside Cutomer->Children
     */

    public function getMindMapNodeForPath($path) {
        return $this->getMindMapNodeForPathProcessor(trim($path), $this->inputMap->mindmap->root);
    }

    /*
     * This is the processor for @getMindMapNodeForPath. Do not call this method directly. Call this through @getMindMapNodeForPath
     */

    private function getMindMapNodeForPathProcessor($path, $node) {
        $rockBottom = null; // Went upto the end of the path ($path) and found a match?

        $firstDelimeterPos = strpos($path, '.');
        //print_r("dot pos = " . $firstDelimeterPos . '<br/>');
        if ($firstDelimeterPos != 0) {
            $currentlySearchingVariable = substr($path, 0, $firstDelimeterPos);
            $remainder = substr($path, $firstDelimeterPos + 1);
        } else if ($path == null) {
            return $node;
        } else {
            $currentlySearchingVariable = $path;
            $remainder = "";
        }
//        echo 'Var = ' . $currentlySearchingVariable . '<br/>';
//        echo 'Rem = ' . $remainder . '<br/>';

        if (is_array($node)) {
            foreach ($node as $value) {
                $temp = $this->getMindMapNodeForPathProcessor($path, $value);
                if ($temp != null) {
                    $rockBottom = $temp;
                }
            }
        } else {
            foreach ($node as $key => $value) {
                if ($key == 'text') {
                    //echo 'obj = ' . $value->caption . '<br/>';
                    if ($value->caption == $currentlySearchingVariable) {
                        if ($remainder == "") {
                            //print_r($node->children);
                            $rockBottom = $node->children;
                        } else {
                            $temp = $this->getMindMapNodeForPathProcessor($remainder, $node->children);
                            if ($temp != null)
                                $rockBottom = $temp;
                        }
                    }
                }
            }
        }
        //print_r($rockBottom);
        return $rockBottom;
    }

    public function createTree() {
        $html = new DOMDocument('1.0', 'iso-8859-1');
        $html->formatOutput = true;
//            return html_entity_decode($html->saveHTML());
        return html_entity_decode($this->createTreeProcessor($this->inputMap->mindmap->root, $html, $html, true, 1)->saveHTML());
//        return $this->createTreeProcessor($this->inputMap->mindmap->root, $html);
    }

    private function createTreeProcessor($node, $htmlDom, $parantDom, $lastElement, $backgroundAlpha) {
        if (is_array($node)) {
            $itemsCount = count($node);
            $i = 0;
            foreach ($node as $value) {
                if (++$i === $itemsCount) {
                    $lastElement = true;
                } else {
                    $lastElement = false;
                }
                $this->createTreeProcessor($value, $htmlDom, $parantDom, $lastElement, $backgroundAlpha * 9 / 10);
            }
        } else {
            $nodeName = $node->text->caption;
            $listElement = $htmlDom->createElement('li');
            $listElementClasses = 'btn rule-element tree-element responsiveFont ui-draggable';

            if ($lastElement) {
                $listElementClasses .= ' last';
            }
            
            if (count($node->children) > 0){
                $isValue=false;
                $listElementClasses .= ' variable';
            }
            else{
                $isValue=true;
                $listElementClasses .= ' value';
            }

            $listElement->setAttribute('style', 'opacity: ' . $backgroundAlpha . ';');

            $listElement->setAttribute('class', $listElementClasses);

            $listElement->setAttribute('id', 'rule-element-' . $node->id);
            $listElement->setAttribute('nodeid', $node->id);
            $title_text = $htmlDom->createTextNode($nodeName);
            $listElement->appendChild($title_text);
            $parantDom->appendChild($listElement);

            if (!$isValue) {
                $innerParentWrapper = $htmlDom->createElement('li');
                $innerParentWrapper->setAttribute('class', 'outer-rule-element rule-element');
                $parantDom->appendChild($innerParentWrapper);

                $innerParent = $htmlDom->createElement('ul');
                $innerParent->setAttribute('class', 'rule-block');
                $innerParentWrapper->appendChild($innerParent);

                $this->createTreeProcessor($node->children, $htmlDom, $innerParent, $lastElement, $backgroundAlpha * 9 / 10);
            }
        }
        return $htmlDom;
    }

    public function getElementPathById($elementId, $spaceFiller) {
        return $this->getElementPathByIdProcessor($elementId, $this->inputMap->mindmap->root, '', $spaceFiller);
    }

    public function getElementPathByIdProcessor($elementId, $node, $currPath, $spaceFiller) {

        if (is_array($node)) {
            foreach ($node as $value) {
                $retPath = $this->getElementPathByIdProcessor($elementId, $value, $currPath, $spaceFiller);
                if ($retPath != null) {
                    return $retPath;
                }
            }
        } else {
            if ($currPath != '') {
                $currPath.='.';
            }
            if ($spaceFiller != '') {
                $caption = str_replace(' ', $spaceFiller, $node->text->caption);
            }
            else{
                $caption = $node->text->caption;
            }
            if ($node->id == $elementId) {
                return $currPath . $caption;
            } else {
                return $this->getElementPathByIdProcessor($elementId, $node->children, $currPath . $caption, $spaceFiller);
            }
        }
    }

}
