<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CombinationHolder
 *
 * @author samarakoon
 */
class CombinationHolder {
    public $combination = array();
    public $combinationFlag  = false;
  
    function __construct($combination, $combinationFlag) {
        $this->combination = $combination;
        $this->combinationFlag = $combinationFlag;
    }

    public function setCombination($combination) {
        $this->combination = $combination;
    }

    public function setCombinationFlag($combinationFlag) {
        $this->combinationFlag = $combinationFlag;
    }

    public function getCombination() {
        return $this->combination;
    }

    public function getCombinationFlag() {
        return $this->combinationFlag;
    }
    }
