<!DOCTYPE html>

<?php
include './ProcessMindMap.php';
require_once './Commons.php';
require_once './ProcessTuples.php';
?>

<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="./img/favicon.png">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/responsiveSheet.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="./dashboard/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="css/jquery-ui.css">
        <link href="./css/oscar-styles.css" rel="stylesheet">
        <link href="./css/rule-editor-treeview.css" rel="stylesheet">

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.js"></script>

        <script src="./js/Commons.js"></script>
        <!--script for tool tips-->
        <script type="text/javascript" src="js/jquery.balloon.js"></script>
        <!--TODO :optimize and move the script below to ruleEditorCommons.js-->
        <script>
            var $ulGoesWith;
            var $ulDoesntGoWith;
            function fillTheExampleValues() {
                var $treeValues = $("#dataTree li.value");
                var $treeValue1 = $($($treeValues).clone()[0]).removeClass("rule-element").addClass("notdraggable-rule-element");
                var $treeValue2 = $($($treeValues).clone()[$treeValues.length - 1]).removeClass("rule-element").addClass("notdraggable-rule-element");               
                if ($($treeValues[0]).parent()[0] === $($treeValues[$treeValues.length - 1]).parent()[0]) {
                    $treeValue1.text("Value X");
                    $treeValue2.text("Value y");
                }
                setTheToolTip($treeValue1, $treeValue2);
            }
            function setTheToolTip($treeValue1, $treeValue2) {
                var $goesWith = $("#rule-constraint-only_when").clone();
                var $doesNotGoWith = $("#rule-constraint-only_not_when").clone();
                $ulDoesntGoWith = $('<ul/>', {id: "EE_ID", style: 'padding-left:0px;'})
                        .append($treeValue1.clone())
                        .append($doesNotGoWith)
                        .append($treeValue2.clone());
                $ulGoesWith = $('<ul/>', {id: "EE_ID", style: 'padding-left:0px;'})
                        .append($treeValue1.clone())
                        .append($goesWith)
                        .append($treeValue2.clone());
            }
        </script>
        <!--<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">-->


        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
        <style id="holderjs-style" type="text/css"></style>
        <?php
//        include 'DBConnector.php';
        if (isset($_GET['projectid']) && isset($_GET['storyid'])) {
            $map = DBConnector::getMindMap($_GET['projectid'], $_GET['storyid']);
            $storyid = $_GET['storyid'];
            $projid = $_GET['projectid'];
        } else {
            $map = "";
            $storyid = "";
            $projid = "";
        }
        ?>
        <script src="js/ruleEditorCommons.js"></script>
        <script>
            $(document).ready(function() {
//                $(".amount").val($("#slider-range-min").slider("value"));
                mmap = <?php echo json_encode($map); ?>;
                storyid = "<?php echo $storyid; ?>";
                // projid = "";
                projid = "<?php echo $projid; ?>";
                initPage(mmap, storyid, projid);
                hideWindowLoader();
                fillTheExampleValues();
                $("#rule-constraint-only_when").balloon({position: "bottom", contents: $ulGoesWith});//                
                $("#rule-constraint-only_not_when").balloon({position: "bottom", contents: $ulDoesntGoWith});//
//                fillTheExampleValues();
            });
        </script>
    </head>
    <body class="fill">
        <?php
        include('documentHeader.php');
        ?> 

        <script>
            document.getElementById("ruleEditorHeading").className = "active";
        </script>

        <div id="sb-site" style="height:100%;">
            <div class="container-fluid" style="height:100%;">
                <div class="col-xs-12 main " style="height:100%;"> 
                    <div class="col-xs-8 " style="height:100%;">
                        <ul class="nav nav-tabs responsiveFont">
                            <li class="active"><a href="#tab_a" data-toggle="tab" >Business Rules</a></li>
                            <li><a href="#tab_b" data-toggle="tab" >Priority Rules</a></li>
                        </ul>
                        <div class="tab-content" style="height:90%;">
                            <div class="tab-pane active" id="tab_a" style="height: 100%">
                                <p> </p>
                                <div class="panel panel-success">
                                    <div class="row panel-body" >
                                        <ul class="rule-constraints-block" style="padding-left: 10px;">
                                            <!--<li id="rule-constraint-have" class="btn btn-primary rule-constraints-element responsiveFont">Have</li>-->
                                            <li id="rule-constraint-only_when" class="btn btn-success rule-constraints-element responsiveFont">Only Goes With</li>
                                            <li id="rule-constraint-only_not_when" class="btn btn-warning rule-constraints-element responsiveFont">Does Not Go With</li>
                                            <!-- <li id="rule-constraint-is_an_invalid_value" class="btn btn-danger rule-constraints-element responsiveFont">Is Invalid</li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-success">
                                    <div class="panel-heading rule-editor-heading">
                                        <h3 class="panel-title rule-editor-heading-title">Rule Editor</h3>
                                        <div class="btn-group pull-right rule-editor-heading-buttons">
                                            <a href="#" class="btn btn-success responsiveFont" id="ruleInsertBtn">Add Rule</a>
                                            <a href="#" class="btn btn-default responsiveFont" id="ruleDiscardBtn">Discard</a>
                                        </div>
                                    </div>
                                    <div class="row panel-body rule-editor-panel-body" >
                                        <div class="caption">
                                            <ul class="rule-editor" id="ruleEditorPanel" >
                                                <!--<li class="emptyMessage btn disabled pull-left">Drag and Drop Here to Build a Rule</li>-->
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div class="panel panel-success" style="height:calc(100% - 200px);">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Rules</h3>
                                    </div>
                                    <div class="panel-body" style="height:calc(100% - 37px); overflow-y: scroll; position:relative; ">
                                        <div id="ruleListLoadingModal" class="loadingModal"></div>
                                        <ul id="ruleList" class="rule-block1 list-group" style="width:100%">
                                        </ul>
                                    </div>
                                </div>  
                            </div>
                            <div class="tab-pane" id="tab_b" style="height: 100%">

                                <p> </p>
                                <div class="tab-pane active" id="tab_a">
                                    <div class="row well" style="overflow-y: auto; height: 1%">
                                        <table class="table" id="tupleRulesHolder">
<!--                                            <tr valign="middle" class="tupleBlock">
                                                <td class="danger" style="width:1%; vertical-align: middle;">
                                                    <input type="text" class="amount" value="2" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                                    <div class="slider-range-min"></div>
                                                </td>
                                                <td class="info">
                                                    <ul class="rule-editor" id="tupleEditorPanel" >
                                                        <li class="emptyMessage  btn disabled">Drag and Drop Here to Build a Rule</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr valign="middle" class="tupleBlock">
                                                <td class="danger" style="width:1%; vertical-align: middle;">
                                                    <input type="text" class="amount" value="2" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                                    <div class="slider-range-min"></div>
                                                </td>
                                                <td class="info">
                                                    <ul class="rule-editor" id="tupleEditorPanel" >
                                                        <li class="btn btn-info">Money-Transfer</li>
                                                    </ul>
                                                </td>
                                            </tr>-->
                                            <?php
                                            $tupleProcessor = new ProcessTuples($projid, $storyid);
                                            print_r($tupleProcessor->createTupleStruct($storyid));
                                            file_put_contents("debug.txt", print_r( $tupleProcessor->createTupleStruct($storyid),true));
                                            ?>
                                        </table>
                                        </li>
                                        </ul>

                                        <div class="well">
                                            <center><button class="btn btn-sm btn-danger" id="btnInsertTupleBlock" style="width: 100%;">+</button></center>
                                        </div>

                                        <div class="btn-group pull-right">
                                            <a href="#" class="btn btn-primary responsiveFont" id="btnUpdateTuples">Update</a>
                                            <a href="#" class="btn btn-default responsiveFont" id="btnDiscardTuple">Disregard Changes</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div><!-- tab content -->

                    </div>
                    <div class="col-xs-4" style="height:100%;" >
                        <div class="panel panel-success fill">
                            <div class="row panel-body fill" style="overflow-y: scroll" >
                                <ul id='dataTree' class="rule-block1" >
                                    <!-- Tree from the MindMap will come here -->
                                    <?php
                                    $mmProcessor = new ProcessMindMap($projid, $storyid);
                                    print_r($mmProcessor->createTree());
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/MindMapToTCaseInput.js"></script>


        <?php
        include('./Sidebar.php');
        include ('./CommonDialogs.php');
        ?>
    </body>
</html>
