
<!DOCTYPE html>

<div class="modal fade bs-example-modal-lg" id="newitem" tabindex="-1" role="dialog" aria-labelledby="newitem" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create a New Project</h4>
            </div>
            <!-- Code for input -->
            <fieldset> 
                <legend></legend>
                <form class="form-horizontal" name="creteProject" method="POST"  action="intermediatePasser.php">

                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">Project Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="projectName" name="projectName" style="width:50%;"  placeholder="Enter Project Name">
                        </div>


                    </div>                    
                    <br>
                    <div class="modal-footer">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Create</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div>  


                </form>

            </fieldset>
            <!-- End of input-->
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" id="showDialogue" tabindex="-1" role="dialog" aria-labelledby="newitem" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create a New Project</h4>
            </div>
            <!-- Code for input -->
            <fieldset> 
                <legend></legend>
                <form class="form-horizontal" name="creteProject" method="POST"  action="intermediatePasser.php">

                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">Project Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="projectName" name="projectName" style="width:50%;"  placeholder="Enter Project Name">
                        </div>
                    </div>                    
                    <br>
                    <div class="modal-footer">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Create</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div>  


                </form>

            </fieldset>
            <!-- End of input-->
        </div>
    </div>
</div>
<?php if (isset($_GET['projectid'])) { ?>
    <div class="modal fade bs-example-modal-lg" id="newStory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Create a New Story</h4>
                </div>
                <!-- Code for input -->
                <fieldset> 
                    <legend></legend>
                    <form class="form-horizontal" name="creteProject" method="post" onsubmit="" action="DBStoryValPasser.php">
                        <!--Ref id-->
                        <div class="form-group">
                            <label for="firstname" class="col-sm-2 control-label">Reference ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="refId" name="refId" style="width:50%;"  placeholder="Enter Reference ID">
                            </div>
                        </div>
                        <!--Story Name-->
                        <div class="form-group">
                            <label for="firstname" class="col-sm-2 control-label">Story Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="storyName" name="storyName" style="width:50%;"  placeholder="Enter Story Name">
                            </div>
                        </div>
                        <!--Story Description-->
                        <div class="form-group">
                            <label for="firstname" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="storyDescription" name="storyDescription" style="width:50%;"  placeholder="Enter Story Description">
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" id="projectId" name="projectId" style="width:50%;"  value="<?php echo $_GET['projectid'] ?>">
                        </div>



                        <div class="modal-footer">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                            </div>
                        </div>  
                    </form>
                </fieldset>
                <!-- End of input-->
            </div>
        </div>
    </div>
<?php } else if (!isset($_GET['projectid'])) {
    ?>
    <div  class="modal fade bs-example-modal-lg" id="newStory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header" style="height: 70px">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 style="color: red">Warning</h3>
                </div>
                <div class="modal-body">
                    <p><h4>Select the project first!!!</h4></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
  