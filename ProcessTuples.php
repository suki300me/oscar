<?php

require_once './DBConnector.php';
require_once './ProcessMindMap.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProcessTuples
 *
 * @author samarakoon
 */
class ProcessTuples {

    public $storyId;
    public $projId;
    public $mmProcessor;

    public function __construct($projId, $storyId) {
        $this->projId = $projId;
        $this->storyId = $storyId;
        $this->mmProcessor = new ProcessMindMap($this->projId, $this->storyId);
    }

    public function createTupleStruct() {
        $html = new DOMDocument('1.0', 'iso-8859-1');
        $html->formatOutput = true;
        $tuples = DBConnector::getTuples($this->storyId);
//        print_r($tuples);
        $tupleTbody = $html->createElement('tbody');
        foreach ($tuples as $tuple) {
            $tupleElements = explode(',', $tuple['nodeId']);
            $tupleConstraint = $tuple['tupleConstraint'];
            $tupleTbody->appendChild($this->createTupleBlock($tupleConstraint, $tupleElements, $html));
        }
        $html->appendChild($tupleTbody);
        file_put_contents("debug.txt", print_r( $html,true));
        return (html_entity_decode($html->saveHTML()));
    }

    public function createTupleBlock($tupleCons, $tupleElements, $htmlDom) {
        //Tuple block
        $tupleBlock = $htmlDom->createElement('tr');
        $tupleBlock->setAttribute('class', 'tupleBlock');
        $tupleBlock->setAttribute('valign', 'middle');

        //Constraint Cell -> under *Tuple block*
        $tupleBlockConstraintCell = $htmlDom->createElement('td');
        $tupleBlockConstraintCell->setAttribute('class', 'tuple-cons-cell');
        $tupleBlockConstraintCell->setAttribute('valign', 'middle');
        $tupleBlock->appendChild($tupleBlockConstraintCell);

        //Constraint Value -> under *Tuple block > Constraint Cell*
        $tupleBlockConsCellValue = $htmlDom->createElement('input');
        $tupleBlockConsCellValue->setAttribute('type', 'text');
        $tupleBlockConsCellValue->setAttribute('class', 'tuple-cons-cell-value amount');
        $tupleBlockConsCellValue->setAttribute('value', $tupleCons);
        $tupleBlockConsCellValue->setAttribute('readonly', 'readonly');
        $tupleBlockConstraintCell->appendChild($tupleBlockConsCellValue);

        //Constraint Value -> under *Tuple block > Constraint Cell*
        $tupleBlockConsCellSlider = $htmlDom->createElement('div');
        $tupleBlockConsCellSlider->setAttribute('class', 'slider-range-min');
        $tupleBlockConstraintCell->appendChild($tupleBlockConsCellSlider);

        //Tuple elements Cell -> under *Tuple block*
        $tupleBlockElementsCell = $htmlDom->createElement('td');
        $tupleBlockElementsCell->setAttribute('class', 'success');
        $tupleBlock->appendChild($tupleBlockElementsCell);

        //Droppable Tuples Elements Holder -> under *Tuple block > Tuple elements Cell*
        $droppableHolder = $htmlDom->createElement('ul');
        $droppableHolder->setAttribute('class', 'rule-editor tupleEditorPanel');
        $tupleBlockElementsCell->appendChild($droppableHolder);

        //Adding children to *Droppable Tuples Elements Holder*
        $droppableHolder = $this->addElementsToTuple($tupleElements, $droppableHolder, $htmlDom);

        return $tupleBlock;
    }

    public function addElementsToTuple($tupleElementIds, $parent, $htmlDom) {
        foreach ($tupleElementIds as $tupleElementId) {
            $tupleElementTextPath = $this->mmProcessor->getElementPathById($tupleElementId, '');
            $pathElements = explode('.', $tupleElementTextPath);
            $tupleElementTextValue = array_pop($pathElements);
//            $tupleElementTextValue = strrchr ($tupleElementTextPath, '.');
            $tupleElement = $htmlDom->createElement('li');
            $tupleElement->setAttribute('class', 'btn tree-element responsiveFont ui-draggable notdraggable-rule-element');
            $tupleElement->setAttribute('nodeid', $tupleElementId);
            $tupleElement->setAttribute('id', 'rule-element-dragged-' . $tupleElementId);
            $tupleElementText = $htmlDom->createTextNode($tupleElementTextValue);
            $tupleElement->appendChild($tupleElementText);
            $parent->appendChild($tupleElement);
        }
        return $parent;
    }

}
