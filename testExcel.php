<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*
         * To change this license header, choose License Headers in Project Properties.
         * To change this template file, choose Tools | Templates
         * and open the template in the editor.
         */

        $url = '.\tcases\bin\TC-Test.xml'; // xml file location with file name
        if (file_exists($url)) {
            $xml = simplexml_load_file($url);
            echo '<pre>';
            $firstIteration = true;
            foreach ($xml->Function->TestCase as $testCase) {
                if ($firstIteration) {
                    echo('id' . '    ');
                    foreach ($testCase->Input->Var as $var) {
                        echo($var->attributes()->name) . '    ';
                    }
                    echo '<br/>';
                }
                $firstIteration = false;
                echo($testCase->attributes()->id + 1) . '    ';
                foreach ($testCase->Input->Var as $var) {
                    echo($var->attributes()->value) . '    ';
                }
                echo '<br/>';
//                print_r($testCase);
            }
            echo '</pre>';
        }
        ?>
    </body>
</html>
