<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MongoDbConnector
 *
 * @author samarakoon
 */
class MongoDbConnector {

    public function connectToDb() {
        // connect
        $m = new MongoClient();

// select a database
        $db = $m->oscardb;

// select a collection (analogous to a relational database's table)
        $collection = $db->project;

//// add a record
//        $document = array("title" => "Calvin and Hobbes", "author" => "Bill Watterson");
//        $collection->insert($document);
//
//// add another record, with a different "shape"
//        $document = array("title" => "XKCD", "online" => true);
//        $collection->insert($document);

// find everything in the collection
        $cursor = $collection->find();

// iterate through the results
        foreach ($cursor as $document) {
            echo $document["idproject"] . "\n";
        }
    }

}
