<?php

require_once './DBConnector.php';
require_once './Commons.php';

try {
    if (isset($_POST["req_type"]) && $_POST["req_type"] == "flag_delete_project") {
        DBConnector::flagForDeleteProject($_POST["nodeId"], true);
    }
    if (isset($_POST["req_type"]) && $_POST["req_type"] == "flag_delete_story") {
        DBConnector::flagForDeleteStory($_POST["nodeId"], true);
    }
    if (isset($_POST["req_type"]) && $_POST["req_type"] == "undo_flag_delete_project") {
        DBConnector::flagForDeleteProject($_POST["nodeId"], false);
    }
    if (isset($_POST["req_type"]) && $_POST["req_type"] == "undo_flag_delete_story") {
        DBConnector::flagForDeleteStory($_POST["nodeId"], false);
    } else {
        Logger::log(LogType::error, "Illegal operation received for ServeDBCalls");
    }
} catch (Exception $e) {
    Logger::log(LogType::error, $e->getMessage());
}
