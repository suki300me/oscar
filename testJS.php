<!DOCTYPE html>
<!-- saved from url=(0044)http://getbootstrap.com/examples/dashboard/# -->
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="http://getbootstrap.com/assets/ico/favicon.ico">

        <title>Oscar | Enterprise Testing</title>

        <!-- Bootstrap core CSS -->
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">

        <link rel="stylesheet" href="css/jquery-ui.css">

        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style id="holderjs-style" type="text/css"></style>
        <style>
            ul.rule-editor, ul.rule-block { list-style-type: none; margin: 0; padding: 0; float: left; margin-right: 10px; margin-left: 0px; background: #eee; padding: 5px; width: 143px; min-height: 40px}
            li.rule-element { margin: 5px; padding: 5px; font-size: 18px; width: 120px; }
        </style>
        <script>
            $(function() {
                var stringNodes = '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":134,"y":-26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1402325287664},"dimensions":{"x":4000,"y":2000},"autosave":false} ';
                var nodes = JSON.parse(stringNodes);
                var listEl = document.getElementById('dataTree');
                convertMindMap(nodes, listEl);
                //$(document.getElementById('treeViewElements')).append(parseNodes(nodes));
                //console.log(parseNodes(nodes));

                $("ul.rule-editor").sortable({
                    revert: true
                });
                $("li.rule-element").draggable({
                    connectToSortable: "ul.rule-editor",
                    helper: "clone",
                    revert: "invalid"
                });
                $("ul, li").disableSelection();
            });
        </script>
    </head>

    <body style="">

        <?php
        include('documentHeader.php');
        ?>

        <script>
            document.getElementById("ruleEditorHeading").className = "active";
        </script>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="#">Overview</a></li>
                        <li><a href="#">Reports</a></li>
                        <li><a href="#">Analytics</a></li>
                        <li><a href="#">Export</a></li>
                    </ul>
                    <ul class="nav nav-sidebar">
                        <li><a href="">Nav item</a></li>
                        <li><a href="">Nav item again</a></li>
                        <li><a href="">One more nav</a></li>
                        <li><a href="">Another nav item</a></li>
                        <li><a href="">More navigation</a></li>
                    </ul>
                    <ul class="nav nav-sidebar">
                        <li><a href="">Nav item again</a></li>
                        <li><a href="">One more nav</a></li>
                        <li><a href="">Another nav item</a></li>
                    </ul>
                </div>

                <div class="col-md-10 col-md-offset-2 main">
                    <div class="col-md-9">
                        <h2 class="sub-header">DOM Test</h2>
                        <div class="table-responsive" id="treeViewElements">
                            <div class="col-md-6">
                                <div class="well" style="height: 1000px">
                                    <ul id='dataTree' class="rule-block">
                                        <!-- Tree from the MindMap will come here -->
                                    </ul>
                                </div>
                            </div>
<!--                            <div class="col-md-6">
                                <div class="well" style="height: 280px">
                                    <ul class="rule-block">
                                        <li class="ui-widget-header rule-element">Only When</li>
                                        <li class="ui-widget-header rule-element">Only Not When</li>
                                        <li class="ui-widget-header rule-element">Then</li>
                                        <li class="ui-widget-header rule-element">Is an Invalid Value</li>
                                    </ul>
                                </div>
                            </div>-->
                            <!--
                            <ul class="rule-block">
                                <li class="ui-state-highlight rule-element">Vehicle</li>
                                <li class="ui-state-default rule-element">Car</li>
                                <li class="ui-state-default rule-element">Van</li>
                                <li class="ui-state-default rule-element">SUV</li>
                                <li class="ui-state-default rule-element">Bus</li>
                            </ul>
                            <ul class="rule-block">
                                <li class="ui-state-highlight rule-element">Car-Type</li>
                                <li class="ui-state-default rule-element">Hatchback</li>
                                <li class="ui-state-default rule-element">Sedan</li>
                                <li class="ui-state-default rule-element">Wagon</li>
                            </ul>
                            <ul class="rule-block">
                                <li class="ui-state-highlight rule-element">SUV-Type-Full Size</li>
                                <li class="ui-state-default rule-element">4WD</li>
                                <li class="ui-state-default rule-element">AWD</li>
                                <li class="ui-state-default rule-element">2WD</li>
                            </ul>
                            <ul class="rule-block">
                                <li class="ui-state-highlight rule-element">SUV-Type-Compact</li>
                                <li class="ui-state-default rule-element">4WD</li>
                                <ul class="rule-block">
                                    <li class="ui-state-default rule-element">test1234</li>
                                    <li class="ui-state-default rule-element">test1234</li>
                                    <li class="ui-state-default rule-element">test1234</li>
                                    <li class="ui-state-default rule-element">test1234</li>
                                </ul>
                                <li class="ui-state-default rule-element">AWD</li>
                                <li class="ui-state-default rule-element">2WD</li>
                            </ul>

                            <br style="clear:both">
                            -->
                        </div>
                    </div>

                    <div class="col-md-3 ">
                          <!--<input type="text" class="form-control" placeholder="Search...">-->
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="well">
                                    <div class="caption">
                                        <ul class="rule-editor">
                                            <li class="emptyMessage">Drag and Drop Here to Build a Rule</li>
                                        </ul>
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-primary">Add Rule</a>
                                            <a href="#" class="btn btn-default">Discard</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- -->
                </div>

            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/docs.min.js"></script>
        <script src="js/MindMapToTCaseInput.js"></script>

    </body></html>