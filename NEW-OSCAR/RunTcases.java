package info.kpgx.hello;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.cornutum.tcases.Tcases;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class RunTcases this class uses java temp dir as the
 * working dir for files which used for Tcases
 */
@WebServlet("/hello")
public class RunTcases extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Stack<String> stack = new Stack<String>();

	private final static String RULE_CONSTRAINT_WHEN = "when";
	private final static String RULE_CONSTRAINT_WHEN_NOT = "whenNot";
	// add above defined rules to this array
	private final static String[] DEFINED_RULE_CONSTRAINTS = {
			RULE_CONSTRAINT_WHEN, RULE_CONSTRAINT_WHEN_NOT };

	private final static String S_NOT = "not";
	private final static String E_NOT = "/not";
	private final static String S_ALL = "all";
	private final static String E_ALL = "/all";
	private final static String S_ANY = "any";
	private final static String E_ANY = "/any";

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public RunTcases() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * executes tcases and create the
	 */
	public void runTcases(String[] tcasesArgs) {
		System.out.println("Preventing System.exit");
		MySystemExitControl.forbidSystemExitCall();
		try {
			Tcases.main(tcasesArgs);
		} catch (MySystemExitControl.ExitTrappedException e) {
			System.out.println("Forbidding a call to System.exit");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Allowing System.exit");
		MySystemExitControl.enableSystemExitCall();
	}

	public File createTempDefFile(String pre, String suf, String content,
			String dir) {
		File temp = null;
		try {
			temp = File.createTempFile(pre, suf, new File(dir));
			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			bw.write(content);
			bw.close();
			System.out.println("Done writing to file :" + pre);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}

	/*
	 * reads a file and return the content as a string
	 */
	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	/*
	 * returns the complete input xml string
	 */
	public static String getInputXML(String mindMapJson, Map rulesMap) {
		StringWriter sw = new StringWriter();
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(mindMapJson);
		String storyName = o.getAsJsonObject("mindmap").getAsJsonObject("root")
				.getAsJsonObject("text").getAsJsonPrimitive("caption")
				.toString().replace("\"", "").replace(" ", "-");
		try {
			XMLStreamWriter writer = addPreXmlPart(sw, storyName);
			// add the elements to xml
			inputXMLProccesor(
					o.getAsJsonObject("mindmap").getAsJsonObject("root")
							.getAsJsonArray("children"), writer, rulesMap);
			// finalize the xml
			addPostXmlPart(writer);
		} catch (FileNotFoundException | XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sw.toString();
	}

	/*
	 * recursive function to iterate the mindpap and create the tcases input xml
	 * magic ,do not touch
	 */
	@SuppressWarnings("unchecked")
	static void inputXMLProccesor(JsonArray jsonArray, XMLStreamWriter writer,
			Map rulesMap) throws XMLStreamException {
		// iterate over children array
		for (int i = 0; i < jsonArray.size(); i++) {
			if (((JsonObject) jsonArray.get(i)).getAsJsonArray("children")
					.size() > 0) {
				if (((JsonObject) ((JsonObject) jsonArray.get(i))
						.getAsJsonArray("children").get(0)).getAsJsonArray(
						"children").size() > 0) {
					// varset
					if (!stack.empty() && stack.peek() == "val") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "var") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "varset") {
						stack.pop();
						writer.writeEndElement();
					}

					writer.writeStartElement("VarSet");
					stack.push("varset");
					writer.writeAttribute(
							"name",
							((JsonObject) jsonArray.get(i))
									.getAsJsonObject("text")
									.getAsJsonPrimitive("caption").toString()
									.replace("\"", "").replace(" ", "-"));
				} else {
					if (!stack.empty() && stack.peek() == "val") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "var") {
						stack.pop();
						writer.writeEndElement();
					}
					// var
					writer.writeStartElement("Var");
					stack.push("var");
					writer.writeAttribute(
							"name",
							((JsonObject) jsonArray.get(i))
									.getAsJsonObject("text")
									.getAsJsonPrimitive("caption").toString()
									.replace("\"", "").replace(" ", "-"));
				}
			} else {
				// val
				if (!stack.empty() && stack.peek() == "val") {
					stack.pop();
					writer.writeEndElement();
				}
				writer.writeStartElement("Value");
				stack.push("val");
				writer.writeAttribute("name", ((JsonObject) jsonArray.get(i))
						.getAsJsonObject("text").getAsJsonPrimitive("caption")
						.toString().replace("\"", "").replace(" ", "-"));
				String nId = ((JsonObject) jsonArray.get(i))
						.getAsJsonPrimitive("id").toString().replace("-", "")
						.replace("\"", "");
				writer.writeAttribute("property", nId);
				// add the rules to xml
				if (rulesMap != null) {
					if (rulesMap.get(nId) != null) {
						// System.out.println("rulesMap current :"
						// + rulesMap.get(nId).toString());
						ArrayList<String> currentRule = (ArrayList<String>) rulesMap
								.get(nId);
						// System.out.println(currentRule);
						writeRuleToXML(writer, currentRule);
					}
				}
			}
			inputXMLProccesor(
					((JsonObject) jsonArray.get(i)).getAsJsonArray("children"),
					writer, rulesMap);
		}
	}

	/*
	 * create a xml writer object,add the generic, pre elements to the xmlDoc
	 * and returns the writer object.
	 */
	private static XMLStreamWriter addPreXmlPart(StringWriter sw,
			String mMapName) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = outputFactory.createXMLStreamWriter(sw);
		writer.writeStartDocument();
		writer.writeStartElement("System");
		writer.writeAttribute("name", "Tcases");
		writer.writeStartElement("Function");
		writer.writeAttribute("name", "TC");
		writer.writeStartElement("Input");
		writer.writeStartElement("VarSet");
		writer.writeAttribute("name", mMapName);
		return writer;
	}

	/*
	 * Finalize the xmlDoc writing
	 */
	private static void addPostXmlPart(XMLStreamWriter writer)
			throws XMLStreamException {
		// finalize the elements opened in inputXMLProccesor
		while (!stack.empty()) {
			stack.pop();
			writer.writeEndElement();
		}
		// finalize the elements opened in addPreXmlPart
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		writer.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		pw.println("you came to this page from GET request while you are supposed to come from a POST request, smthing is wrong!!!");
	}

	/*
	 * process the entire rule json string and return the rules as a array list
	 * (has to update for new rules format)
	 */
	private List<String[]> processRuleString(String rulesMap) {
		List<String[]> rules = new ArrayList<String[]>();
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(rulesMap);
		JsonArray ruleArray = o.getAsJsonArray("rules");
		// System.out.println(ruleArray.size());
		for (int i = 0; i < ruleArray.size(); i++) {
			List<String> currentRuleArrayList = new ArrayList<String>();
			JsonObject currentRule = (JsonObject) ruleArray.get(i);
			JsonArray currentRuleJsonArray = currentRule
					.getAsJsonArray("ruleNode");
			// System.out.println(currentRuleArray.size());
			for (int j = 0; j < currentRuleJsonArray.size(); j++) {
				JsonObject currentRulePart = (JsonObject) currentRuleJsonArray
						.get(j);
				JsonObject currentRuleNode = currentRulePart
						.getAsJsonObject("node");
				String currentRulePartId = currentRuleNode
						.getAsJsonPrimitive("id").toString().replace('"', '-')
						.replace("-", "");
				String currentRulePartText = currentRuleNode
						.getAsJsonObject("text").getAsJsonPrimitive("caption")
						.toString();
				// System.out.println(currentRulePartId);
				currentRuleArrayList.add(currentRulePartId);
			}
			// System.out.println("array list size :"+currentRuleArrayList.size());
			String[] currentRuleArray = currentRuleArrayList
					.toArray(new String[currentRuleArrayList.size()]);
			rules.add(currentRuleArray);
			// System.out.println(currentRuleArray.length);
		}
		return rules;
	}

	/*
	 * process the rules array
	 */
	public static Map<String, ArrayList<String>> processRuleArray(
			String[][] rules) {
		Map<String, ArrayList<String>> proccessedRules = new HashMap<String, ArrayList<String>>();
		for (int i = 0; i < rules.length; i++) {
			proccessedRules = processRule(proccessedRules, rules[i]);
		}
		return proccessedRules;
	}

	/*
	 * process a individual rule
	 */
	public static Map<String, ArrayList<String>> processRule(
			Map<String, ArrayList<String>> ruleMap, String[] rule) {
		// rule must contain least 3 elements, else it's invalid
		if (rule.length < 3) {
			System.err.println("invalid rule :"
					+ Arrays.asList(rule).toString());
			return ruleMap;
		}
		String ruleHead = rule[0];
		String ruleConstraint = rule[1];
		// 2nd element of rule must be a rule_constraint. else it's invalid
		if (!Arrays.asList(DEFINED_RULE_CONSTRAINTS).contains(ruleConstraint)) {
			System.err.println("invalid rule :"
					+ Arrays.asList(rule).toString());
			return ruleMap;
		}
		// get the rule body
		String[] ruleBody = Arrays.copyOfRange(rule, 2, rule.length);

		StringBuilder builder = new StringBuilder();
		// make a string from rule body
		for (String string : ruleBody) {
			if (builder.length() > 0) {
				builder.append(" ");
			}
			builder.append(string);
		}
		String ruleBodyString = builder.toString();
		// Process the rule body
		ArrayList<String> processedRule = getProccessedRule(ruleBodyString);
		// handle special cases
		if (ruleConstraint.equals(RULE_CONSTRAINT_WHEN_NOT)) {
			processedRule.add(0, S_NOT);
			processedRule.add(E_NOT);
		}
		if (ruleBody.length == 1) {
			processedRule.add(0, S_ALL);
			processedRule.add(E_ALL);
		}
		// System.out.println("Processed rule :"+processedRule.toString());
		ruleMap.put(ruleHead, processedRule);
		return ruleMap;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String mindMapJson = request.getParameter("mmapString");
		String rulesString = request.getParameter("rulesString");
		String stid = request.getParameter("stid");
		String ptid = request.getParameter("ptid");
		String ctx = request.getParameter("ctx");
		// System.out.println("got this mindmap :"+mindMapJson);
		// System.out.println("got these rulemap :"+rulesString);

		// List<String[]> rules =new ArrayList<>();
		// rules =addTempRules(rules);

		// System.out.println(rules.toString());
		// for (Iterator iterator = rules.iterator(); iterator.hasNext();) {
		// String[] strings = (String[]) iterator.next();
		// for (int i = 0; i < strings.length; i++) {
		// System.out.println(strings[i]);
		// }
		// }
		// add temporary rules for debuging
		// rules=addTempRules(rules);
		// process the raw rules
		System.out.println("processing raw rules");
		String[] rawRule = { "fddf9eee32644e7eb1b02857754e5eb1", "when",
				"199139e643f24b5399db8be64a8d76ee" };
		String[][] rawRules = { rawRule };

		Map<String, ArrayList<String>> rulesMap = processRuleArray(rawRules);
		// Map rulesMap = getRuleMap(rules);
		System.out.println("rule map :" + rulesMap.toString());
		// System.out.println("reading mindmap.json");
		// String mindMapJson = readFile("C:/test/foo/mindmap.json",
		// Charset.defaultCharset());
		System.out.println("processing mindmap..");
		String inputXMLStr = getInputXML(mindMapJson, rulesMap);
		System.out.println("created xml :\n" + inputXMLStr);
		System.out
				.println("reading input files(only the generator file, TC-Input is created dynamically)");
		// String inputXML = "C:/test/foo/TC-Input.xml";
		String generatorXML = "C:/test/foo/TC-Generators.xml";
		// String inputXMLStr = readFile(inputXML, Charset.defaultCharset());
		String genXMLStr = readFile(generatorXML, Charset.defaultCharset());
		// working dir for tcases
		String dir = System.getProperty("java.io.tmpdir");
		System.out.println("using the temp dir :" + dir);
		// temp files needed for tcases
		System.out.println("creating input files for tcases");
		File inputF = createTempDefFile("inputDef", ".xml", inputXMLStr, dir);
		File generaterF = createTempDefFile("generaterDef", ".xml", genXMLStr,
				dir);
		File testDefF = createTempDefFile("testDefF", ".xml", "", dir);
		// arguments for tcases main
		String[] tcasesArgsWithGeneratorOption = { "-g",
				dir + generaterF.getName(), "-f", dir + testDefF.getName(),
				dir + inputF.getName() };
		// without generators
		String[] tcasesArgsWithOutGeneratorOption = { "-f",
				dir + testDefF.getName(), dir + inputF.getName() };

		// run the tcases
		System.out.println("running tcases");
		runTcases(tcasesArgsWithOutGeneratorOption);
		// read the tcases output file
		String testDefXML = readFile(dir + testDefF.getName(),
				Charset.defaultCharset());
		// delete temp files
		System.out.println("deleting created temp files");
		inputF.delete();
		generaterF.delete();
		testDefF.delete();
		// add the xml data in request
		request.setAttribute("testDef", testDefXML);
		// forward to TestCasesVIew
		System.out.println("forwarding to view jsp");
		request.getRequestDispatcher("/hello.jsp?p=" + ptid + "&s=" + stid)
				.forward(request, response);
	}

	/*
	 * write the complex rule to xml writer
	 */
	public static void writeRuleToXML(XMLStreamWriter writer,
			ArrayList<String> proccessedCond) throws XMLStreamException {
//		System.out.println("processing rule :" + proccessedCond.toString());
		writer.writeStartElement("When");
		for (Iterator iterator = proccessedCond.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
//			System.out.println(string);
			if (string.equals("all")) {
				writer.writeStartElement("AllOf");
			} else if (string.equals("any")) {
				writer.writeStartElement("AnyOf");
			} else if (string.equals("not")) {
				writer.writeStartElement("Not");
			} else if (string.equals("/all")) {
				writer.writeEndElement();
			} else if (string.equals("/any")) {
				writer.writeEndElement();
			} else if (string.equals("/not")) {
				writer.writeEndElement();
			} else {
				writer.writeEmptyElement("AllOf");
				writer.writeAttribute("property", string);
			}
		}
		writer.writeEndElement();
	}

	public static ArrayList<String> getProccessedRule(String rule) {
		return getComplexRuleMap(infixToPostfix(rule).split(" "));
	}

	/*
	 * evaluate the complex rule in RPN returns a xml friendly array
	 */
	public static ArrayList<String> getComplexRuleMap(String[] complexCond) {
		final String S_NOT = "not";
		final String E_NOT = "/not";
		final String S_ALL = "all";
		final String E_ALL = "/all";
		final String S_ANY = "any";
		final String E_ANY = "/any";

		Stack<ArrayList<String>> mArrayList = new Stack<ArrayList<String>>();
		String[] definedOperators = { "!", "&", "|" };
		for (int i = 0; i < complexCond.length; i++) {
			String currentElement = complexCond[i];
			if (Arrays.asList(definedOperators).contains(currentElement)) {
				// operator
				if (currentElement.equals("!")) {
					ArrayList<String> currentArrayList = mArrayList.pop();
					currentArrayList.add(0, S_NOT);
					currentArrayList.add(E_NOT);
					mArrayList.push(currentArrayList);
				} else if (currentElement.equals("&")) {
					ArrayList<String> currentArrayListTwo = mArrayList.pop();
					ArrayList<String> currentArrayList = new ArrayList<>(
							mArrayList.pop());
					currentArrayList.addAll(currentArrayListTwo);
					currentArrayList.add(0, S_ALL);
					currentArrayList.add(E_ALL);
					mArrayList.push(currentArrayList);
				} else if (currentElement.equals("|")) {
					ArrayList<String> currentArrayListTwo = mArrayList.pop();
					ArrayList<String> currentArrayList = new ArrayList<>(
							mArrayList.pop());
					currentArrayList.addAll(currentArrayListTwo);
					currentArrayList.add(0, S_ANY);
					currentArrayList.add(E_ANY);
					mArrayList.push(currentArrayList);
				} else {
					System.out.println("Error undefined operator :"
							+ currentElement);
				}
			} else {
				// operand
				ArrayList<String> newArrayList = new ArrayList<>();
				newArrayList.add(currentElement);
				mArrayList.add(newArrayList);
			}
		}
		if (mArrayList.size() > 1) {
			return null;
		}
		return mArrayList.get(0);
	}

	/*
	 * change the infix expression to postfix expression using Shunting-yard
	 * algorithm
	 */
	static String infixToPostfix(String infix) {
		// precedence increases left to right
		final String ops = "&|!";
		StringBuilder sb = new StringBuilder();
		Stack<Integer> s = new Stack<>();

		for (String token : infix.split("\\s")) {
			char c = token.charAt(0);
			int idx = ops.indexOf(c);
			if (idx != -1 && token.length() == 1) {
				if (s.isEmpty())
					s.push(idx);
				else {
					while (!s.isEmpty()) {
						int prec2 = s.peek() / 2;
						int prec1 = idx / 2;
						if (prec2 > prec1 || (prec2 == prec1 && c != '^'))
							sb.append(ops.charAt(s.pop())).append(' ');
						else
							break;
					}
					s.push(idx);
				}
			} else if (c == '(') {
				s.push(-2);
			} else if (c == ')') {
				while (s.peek() != -2)
					sb.append(ops.charAt(s.pop())).append(' ');
				s.pop();
			} else {
				sb.append(token).append(' ');
			}
		}
		while (!s.isEmpty())
			sb.append(ops.charAt(s.pop())).append(' ');
		return sb.toString();
	}
}

/*
 * implements way to control system.exit calls(Tcases issues system.exit calls)
 */
class MySystemExitControl {

	public static class ExitTrappedException extends SecurityException {
	}

	public static void forbidSystemExitCall() {
		final SecurityManager securityManager = new SecurityManager() {
			@Override
			public void checkPermission(Permission permission) {
				if (permission.getName().contains("exitVM")) {
					throw new ExitTrappedException();
				}
			}
		};
		System.setSecurityManager(securityManager);
	}

	public static void enableSystemExitCall() {
		System.setSecurityManager(null);
	}
}
