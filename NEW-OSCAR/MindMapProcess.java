package info.kpgx.mytcases;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.ArrayUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MindMapProcess {
	// this is for keeping track of which xml elements are open. In order to
	// close them
	private static Stack<String> stack = new Stack<String>();

	/*
	 * function for reading files returns string representation of file
	 */
	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	@SuppressWarnings("unchecked")
	public static Map getRuleMap(List<String[]> rules) {
		Map<String, Map> rulesMap = new HashMap<String, Map>();
		for (Iterator iterator = rules.iterator(); iterator.hasNext();) {
			String[] rule = (String[]) iterator.next();
			Map<String, List> ruleMap = new HashMap<String, List>();
			List<String> whenArrayList=new ArrayList<String>();
			List<String> whenNotArrayList=new ArrayList<String>();
			ruleMap.put("when", whenArrayList);
			ruleMap.put("whenNot", whenNotArrayList);
			ArrayUtils.reverse(rule);
			String constraint = null;
			Boolean afterCons = true;
			List<String> valsAfterCons = new ArrayList<String>();
			for (int i = 0; i < rule.length; i++) {
				if (rule[i] == "goesWith" || rule[i] == "notGoesWith") {
					afterCons = false;
					if (rule[i] == "goesWith") {
						constraint = "when";
					} else if (rule[i] == "notGoesWith") {
						constraint = "whenNot";
					}					
				} else {
					if (afterCons) {
						if (rulesMap.get(rule[i]) == null) {
							rulesMap.put(rule[i], ruleMap);							
						}
						valsAfterCons.add(rule[i]);
					} else {
						for (Iterator iterator2 = valsAfterCons.iterator(); iterator2
								.hasNext();) {
							String string = (String) iterator2.next();
//							System.out.println(rulesMap.get(string).get(constraint));
							if (!((List<String>) rulesMap.get(string).get(constraint)).contains(rule[i])) {
								((List<String>) rulesMap.get(string).get(constraint)).add(rule[i]);
							}							
						}
					}
				}
			}
//			System.out.println(rulesMap.toString());
		}
		System.out.println(rulesMap.toString());
//		sys
		return rulesMap;
	}

	public static void createInputXML(Map rulesMap) {
		String dir = "C:/test/foo/";
		String mindMapJson = null;
		try {
			mindMapJson = readFile(dir + "mindmap.json",
					Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
		}
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(mindMapJson);
		try {
			XMLStreamWriter writer = addPreXmlPart(dir + "tInput.xml", "test");
			inputXMLProccesor(
					o.getAsJsonObject("mindmap").getAsJsonObject("root")
							.getAsJsonArray("children"), writer,rulesMap);
			endXml(writer);
		} catch (FileNotFoundException | XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String[]> rules = new ArrayList<String[]>();
		rules.add(new String[] { "c1", "goesWith", "c2" });
		rules.add(new String[] { "c1", "notGoesWith", "c3" });
		rules.add(new String[] { "c4", "goesWith", "c2" });
		rules.add(new String[] { "c5", "c6", "goesWith", "c7", "c8" });
		Map rulesMap=getRuleMap(rules);
		 createInputXML(rulesMap);
	}

	/*
	 * recursive funvtion to iterate the mindpap and create the tcases input xml
	 * magic ,do not touch
	 */
	static void inputXMLProccesor(JsonArray jsonArray, XMLStreamWriter writer,Map rulesMap)
			throws XMLStreamException {
		System.out.println(stack.toString());
		for (int i = 0; i < jsonArray.size(); i++) {
			if (((JsonObject) jsonArray.get(i)).getAsJsonArray("children")
					.size() > 0) {
				if (((JsonObject) ((JsonObject) jsonArray.get(i))
						.getAsJsonArray("children").get(0)).getAsJsonArray(
						"children").size() > 0) {
					System.out.print("varset :");
					if (!stack.empty() && stack.peek() == "var") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "varset") {
						stack.pop();
						writer.writeEndElement();
					}
					writer.writeStartElement("VarSet");
					stack.push("varset");
				} else {
					if (!stack.empty() && stack.peek() == "var") {
						stack.pop();
						writer.writeEndElement();
					}
					System.out.print("var :");
					writer.writeStartElement("Var");
					stack.push("var");
				}
			} else {
				System.out.print("val :");
				writer.writeEmptyElement("Value");
			}
			writer.writeAttribute("name", ((JsonObject) jsonArray.get(i))
					.getAsJsonObject("text").getAsJsonPrimitive("caption")
					.toString().replace("\"", ""));
//			if (rulesMap.get(key)) {
//				
//			}
			System.out.println(((JsonObject) jsonArray.get(i))
					.getAsJsonObject("text").getAsJsonPrimitive("caption")
					.toString());
			inputXMLProccesor(
					((JsonObject) jsonArray.get(i)).getAsJsonArray("children"),
					writer,rulesMap);
		}
	}

	/*
	 * create a xml writer object,add the generic elements to the xmlDoc and
	 * returns the writer object.
	 */
	private static XMLStreamWriter addPreXmlPart(String xmlF, String mMapName)
			throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = outputFactory
				.createXMLStreamWriter(new FileOutputStream(xmlF));
		writer.writeStartDocument();
		writer.writeStartElement("System");
		writer.writeAttribute("name", "Tcases");
		writer.writeStartElement("Function");
		writer.writeAttribute("name", "TC");
		writer.writeStartElement("Input");
		writer.writeStartElement("VarSet");
		writer.writeAttribute("name", mMapName);
		return writer;
	}

	/*
	 * finishup the xmlDoc writing
	 */
	private static void endXml(XMLStreamWriter writer)
			throws XMLStreamException {
		while (!stack.empty()) {
			stack.pop();
			writer.writeEndElement();
		}
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		writer.flush();
	}
}
