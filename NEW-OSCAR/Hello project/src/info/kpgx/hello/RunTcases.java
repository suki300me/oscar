package info.kpgx.hello;

import info.kpgx.hello.TcasesUtilesGenDef;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cornutum.tcases.Tcases;

/**
 * Servlet implementation class RunTcases this class uses java temp dir as the
 * working dir for files which used for Tcases
 */
@WebServlet("/hello")
public class RunTcases extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public RunTcases() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * executes tcases and create the
	 */
	public static void runTcases(String[] tcasesArgs) {
		System.out.println("Preventing System.exit");
		MySystemExitControl.forbidSystemExitCall();
		try {
			Tcases.main(tcasesArgs);
		} catch (MySystemExitControl.ExitTrappedException e) {
			System.out.println("Forbidding a call to System.exit");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Allowing System.exit :/");
		MySystemExitControl.enableSystemExitCall();
	}

	public static String readFile(String path, Charset encoding)
			throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static File createTempDefFile(String pre, String suf,
			String content, String dir) {
		File temp = null;
		try {
			temp = File.createTempFile(pre, suf, new File(dir));
			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			bw.write(content);
			bw.close();
			System.out.println("Done writing to file :" + pre);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		pw.println("you came to this page from GET request while you are supposed to come from a POST request, smthing is wrong!!!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String mindMapJson = request.getParameter("mmapString");
		String rulesString = request.getParameter("rulesString");
		String stid = request.getParameter("stid");
		String ptid = request.getParameter("ptid");
		String ctx = request.getParameter("ctx");
		
		String inputXMLStr = TcasesUtilsInputDef.getFinalInputDefXML(mindMapJson,rulesString);
		System.out.println("InputDef XML :" + inputXMLStr);
		
		String genXMLStr1 =TcasesUtilesGenDef.getFinalGenDefXML(mindMapJson," ");
		System.out.println("got xml gen new :"+genXMLStr1);
		String generatorXML = "C:/test/foo/TC-Generators.xml";		
		String genXMLStr = readFile(generatorXML, Charset.defaultCharset());
		// working dir for tcases
		String dir = System.getProperty("java.io.tmpdir");
		System.out.println("using the temp dir :" + dir);
		// temp files needed for tcases
		System.out.println("creating input files for tcases");
		File inputF = createTempDefFile("inputDef", ".xml", inputXMLStr, dir);
		File generaterF = createTempDefFile("generaterDef", ".xml", genXMLStr,
				dir);
		File testDefF = createTempDefFile("testDefF", ".xml", "", dir);
		// arguments for tcases main
		String[] tcasesArgsWithGeneratorOption = { "-g",
				dir + generaterF.getName(), "-f", dir + testDefF.getName(),
				dir + inputF.getName() };
		// without generators
		String[] tcasesArgsWithOutGeneratorOption = { "-f",
				dir + testDefF.getName(), dir + inputF.getName() };

		// run the tcases
		System.out.println("running tcases");
		runTcases(tcasesArgsWithOutGeneratorOption);
		// read the tcases output file
		String testDefXML = readFile(dir + testDefF.getName(),
				Charset.defaultCharset());
		// delete temp files
		System.out.println("deleting created temp files");
		inputF.delete();
		generaterF.delete();
		testDefF.delete();
		// add the xml data in request
		request.setAttribute("testDef", testDefXML);
		// forward to TestCasesVIew
		System.out.println("forwarding to view jsp");
		request.getRequestDispatcher("/hello.jsp?p=" + ptid + "&s=" + stid)
				.forward(request, response);
	}
}
