package info.kpgx.hello;

import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TcasesUtilesGenDef {
	private static Stack<String> stack = new Stack<String>();
	private static Stack<String> textStack = new Stack<String>();

	public static String getFinalGenDefXML(String mindMap, String generators) {

		StringWriter sw = new StringWriter();
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(mindMap);
		String storyName = o.getAsJsonObject("mindmap").getAsJsonObject("root")
				.getAsJsonObject("text").getAsJsonPrimitive("caption")
				.toString().replace("\"", "").replace(" ", "-");
		ArrayList<String[]> generatorList = getGeneratorList(generators);
		try {
			XMLStreamWriter writer = addPreXmlPart(sw, storyName);
			addGenRules(writer, generatorList);
			addPostXmlPart(writer);
		} catch (FileNotFoundException | XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, String[]> pathIdMap=getPathIdMap(mindMap);
		System.out.println("Paths for values :");
		for (Map.Entry entry : pathIdMap.entrySet()) {
			String[] value=(String[]) entry.getValue();
		    System.out.println(entry.getKey() + ", " + Arrays.toString(value));
		}
		return sw.toString();
	}

	private static ArrayList<String[]> getGeneratorList(String generators) {
		ArrayList<String[]> generatorList = new ArrayList<>();
		String[] generatorRule = { "2", "X", "Y" };
		String[] generatorRule2 = { "3", "A", "B" };
		generatorList.add(generatorRule);
		generatorList.add(generatorRule2);
		return generatorList;
	}

	private static XMLStreamWriter addPreXmlPart(StringWriter sw,
			String storyName) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = outputFactory.createXMLStreamWriter(sw);
		writer.writeStartDocument();
		writer.writeStartElement("Generators");
		writer.writeStartElement("TupleGenerator");
		writer.writeAttribute("function", storyName);
		return writer;
	}

	private static Map<String, String[]> getPathIdMap(String mindMap) {
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(mindMap);
		String storyName = o.getAsJsonObject("mindmap").getAsJsonObject("root")
				.getAsJsonObject("text").getAsJsonPrimitive("caption")
				.toString().replace("\"", "").replace(" ", "-");
		textStack.push(storyName);
		JsonArray childrenArray = o.getAsJsonObject("mindmap")
				.getAsJsonObject("root").getAsJsonArray("children");
		Map<String, String[]> pathIdMap= new HashMap<>();
		pathIdMapProcessor(childrenArray, pathIdMap);
//		System.out.println("pathIdMap :"+pathIdMap.toString());
		/*for (Map.Entry entry : pathIdMap.entrySet()) {
			String[] value=(String[]) entry.getValue();
		    System.out.println(entry.getKey() + ", " + Arrays.toString(value));
		}*/
		return pathIdMap;
	}

	private static void pathIdMapProcessor(JsonArray childrenArray,Map<String, String[]> pathIdMap) {
		for (int i = 0; i < childrenArray.size(); i++) {
			String text = ((JsonObject) childrenArray.get(i))
					.getAsJsonObject("text").getAsJsonPrimitive("caption").toString();
//			System.out.println("TEXT :"+text);
			if (((JsonObject) childrenArray.get(i)).getAsJsonArray("children")
					.size() > 0) {
				if (((JsonObject) ((JsonObject) childrenArray.get(i))
						.getAsJsonArray("children").get(0)).getAsJsonArray(
						"children").size() > 0) {
					// varset
					if (!stack.empty() && stack.peek() == "val") {
						textStack.pop();
						stack.pop();
					}
					if (!stack.empty() && stack.peek() == "var") {
						textStack.pop();
						stack.pop();
					}
					if (!stack.empty() && stack.peek() == "varset") {
						textStack.pop();
						stack.pop();
					}
					textStack.push(text);
					stack.push("varset");

				} else {
					// var
					if (!stack.empty() && stack.peek() == "val") {
						textStack.pop();						
						stack.pop();
					}
					if (!stack.empty() && stack.peek() == "var") {
						textStack.pop();
						stack.pop();
					}
					textStack.push(text);
					stack.push("var");
				}
			} else {
				// val
				if (!stack.empty() && stack.peek() == "val") {
					textStack.pop();
					stack.pop();
				}
				textStack.push(text);
				stack.push("val");

				String nId = ((JsonObject) childrenArray.get(i))
						.getAsJsonPrimitive("id").toString().replace("-", "")
						.replace("\"", "");
				System.out.println(nId+" :"+textStack.toString());
				String[] pathAsArrayStrings= new String [textStack.size()];
				pathIdMap.put(nId, textStack.toArray(pathAsArrayStrings));
			}
			pathIdMapProcessor(((JsonObject) childrenArray.get(i))
					.getAsJsonArray("children"),pathIdMap);
		}
	}

	private static void addGenRules(XMLStreamWriter writer,
			ArrayList<String[]> generatorList) throws XMLStreamException {
		for (Iterator iterator = generatorList.iterator(); iterator.hasNext();) {
			String[] strings = (String[]) iterator.next();
			if (strings.length < 2) {
				continue;
			}
			String genConstraint = strings[0];
			writer.writeStartElement("Combine");
			writer.writeAttribute("tuples", genConstraint);
			for (int i = 1; i < strings.length; i++) {
				writer.writeEmptyElement("Include");
				writer.writeAttribute("var", strings[i]);
			}
			writer.writeEndElement();
		}
	}

	private static void addPostXmlPart(XMLStreamWriter writer)
			throws XMLStreamException {

		// finalize the elements opened in addPreXmlPart
		writer.writeEndElement();
		writer.writeEndElement();
		writer.flush();
	}

}
