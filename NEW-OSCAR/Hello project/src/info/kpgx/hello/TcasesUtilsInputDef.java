package info.kpgx.hello;

import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TcasesUtilsInputDef {

	private static Stack<String> stack = new Stack<String>();
	private final static String RULE_CONSTRAINT_WHEN = "when";
	private final static String RULE_CONSTRAINT_WHEN_NOT = "whenNot";
	private final static String RULE_CONSTRAINT_ONCE = "once";
	// add above defined rules to this array
	private final static String[] DEFINED_RULE_CONSTRAINTS = {
			RULE_CONSTRAINT_WHEN, RULE_CONSTRAINT_WHEN_NOT,
			RULE_CONSTRAINT_ONCE };

	// strings for complex rules
	private final static String S_NOT = "not";
	private final static String E_NOT = "/not";
	private final static String S_ALL = "all";
	private final static String E_ALL = "/all";
	private final static String S_ANY = "any";
	private final static String E_ANY = "/any";

	public static String getFinalInputDefXML(String mindMap, String rulesString) {

		StringWriter sw = new StringWriter();
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(mindMap);
		String storyName = o.getAsJsonObject("mindmap").getAsJsonObject("root")
				.getAsJsonObject("text").getAsJsonPrimitive("caption")
				.toString().replace("\"", "").replace(" ", "-");

		// List<String[]> ruleList = processRuleString(rules);
		String[] rawRule = { "fddf9eee32644e7eb1b02857754e5eb1", "when",
				"199139e643f24b5399db8be64a8d76ee" };
		String[] rawRule1 = { "ed6bb6def23a41c89266a82fdef5c2d4", "once" };

		String[][] rawRules = { rawRule, rawRule1 };

		Map<String, ArrayList<String>> complexRulesMap = getComplexRulesMap(rawRules);
		ArrayList<String> onceRulesList = getOnceRulesList(rawRules);
		System.out.println("onceRulesList :" + onceRulesList);
		try {
			XMLStreamWriter writer = addPreXmlPart(sw, storyName);
			// add the elements to xml
			inputXMLProccesor(
					o.getAsJsonObject("mindmap").getAsJsonObject("root")
							.getAsJsonArray("children"), writer,
					complexRulesMap, onceRulesList);
			// finalize the xml
			addPostXmlPart(writer);
		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}
		return sw.toString();
	}

	private static ArrayList<String> getOnceRulesList(String[][] rawRules) {
		ArrayList<String> onceRulesList = new ArrayList<String>();
		for (int i = 0; i < rawRules.length; i++) {
			if (rawRules[i].length == 2
					&& rawRules[i][1].equals(RULE_CONSTRAINT_ONCE)) {
				onceRulesList.add(rawRules[i][0]);
			}
		}
		return onceRulesList;
	}

	private static ArrayList<String> getRuleBodyAsList(String rule) {
		return getProcessedRuleWithXMLParts(infixToPostfix(rule).split(" "));
	}

	/*
	 * process a individual rule
	 */
	private static Map<String, ArrayList<String>> getOneProcessedRule(
			Map<String, ArrayList<String>> ruleMap, String[] rule) {
		// rule must contain least 2 elements, else it's invalid
		if (rule.length < 3) {
			System.err
					.println("invalid rule complex rule (maybe a once rule) :"
							+ Arrays.asList(rule).toString());
			return ruleMap;
		}
		String ruleHead = rule[0];
		String ruleConstraint = rule[1];
		// 2nd element of rule must be a rule_constraint. else it's invalid
		if (!Arrays.asList(DEFINED_RULE_CONSTRAINTS).contains(ruleConstraint)) {
			System.err.println("invalid rule :"
					+ Arrays.asList(rule).toString());
			return ruleMap;
		}
		// get the rule body
		String[] ruleBody = Arrays.copyOfRange(rule, 2, rule.length);

		StringBuilder builder = new StringBuilder();
		// make a string from rule body
		for (String string : ruleBody) {
			if (builder.length() > 0) {
				builder.append(" ");
			}
			builder.append(string);
		}
		String ruleBodyString = builder.toString();
		// Process the rule body
		ArrayList<String> processedRule = getRuleBodyAsList(ruleBodyString);
		// handle special cases
		if (ruleConstraint.equals(RULE_CONSTRAINT_WHEN_NOT)) {
			processedRule.add(0, S_NOT);
			processedRule.add(E_NOT);
		}
		if (ruleBody.length == 1) {
			processedRule.add(0, S_ALL);
			processedRule.add(E_ALL);
		}
		// System.out.println("Processed rule :"+processedRule.toString());
		ruleMap.put(ruleHead, processedRule);
		return ruleMap;
	}

	/*
	 * process the rules array
	 */
	private static Map<String, ArrayList<String>> getComplexRulesMap(
			String[][] rules) {
		Map<String, ArrayList<String>> proccessedRules = new HashMap<String, ArrayList<String>>();
		for (int i = 0; i < rules.length; i++) {
			proccessedRules = getOneProcessedRule(proccessedRules, rules[i]);
		}
		return proccessedRules;
	}

	/*
	 * evaluate the complex rule in RPN returns a xml friendly array
	 */
	private static ArrayList<String> getProcessedRuleWithXMLParts(
			String[] complexCond) {

		Stack<ArrayList<String>> mArrayList = new Stack<ArrayList<String>>();
		String[] definedOperators = { "!", "&", "|" };
		for (int i = 0; i < complexCond.length; i++) {
			String currentElement = complexCond[i];
			if (Arrays.asList(definedOperators).contains(currentElement)) {
				// operator
				if (currentElement.equals("!")) {
					ArrayList<String> currentArrayList = mArrayList.pop();
					currentArrayList.add(0, S_NOT);
					currentArrayList.add(E_NOT);
					mArrayList.push(currentArrayList);
				} else if (currentElement.equals("&")) {
					ArrayList<String> currentArrayListTwo = mArrayList.pop();
					ArrayList<String> currentArrayList = new ArrayList<>(
							mArrayList.pop());
					currentArrayList.addAll(currentArrayListTwo);
					currentArrayList.add(0, S_ALL);
					currentArrayList.add(E_ALL);
					mArrayList.push(currentArrayList);
				} else if (currentElement.equals("|")) {
					ArrayList<String> currentArrayListTwo = mArrayList.pop();
					ArrayList<String> currentArrayList = new ArrayList<>(
							mArrayList.pop());
					currentArrayList.addAll(currentArrayListTwo);
					currentArrayList.add(0, S_ANY);
					currentArrayList.add(E_ANY);
					mArrayList.push(currentArrayList);
				} else {
					System.out.println("Error undefined operator :"
							+ currentElement);
				}
			} else {
				// operand
				ArrayList<String> newArrayList = new ArrayList<>();
				newArrayList.add(currentElement);
				mArrayList.add(newArrayList);
			}
		}
		if (mArrayList.size() > 1) {
			return null;
		}
		return mArrayList.get(0);
	}

	/*
	 * change the infix expression to postfix expression using Shunting-yard
	 * algorithm
	 */
	private static String infixToPostfix(String infix) {
		// precedence increases left to right
		final String ops = "&|!";
		StringBuilder sb = new StringBuilder();
		Stack<Integer> s = new Stack<>();

		for (String token : infix.split("\\s")) {
			char c = token.charAt(0);
			int idx = ops.indexOf(c);
			if (idx != -1 && token.length() == 1) {
				if (s.isEmpty())
					s.push(idx);
				else {
					while (!s.isEmpty()) {
						int prec2 = s.peek() / 2;
						int prec1 = idx / 2;
						if (prec2 > prec1 || (prec2 == prec1 && c != '^'))
							sb.append(ops.charAt(s.pop())).append(' ');
						else
							break;
					}
					s.push(idx);
				}
			} else if (c == '(') {
				s.push(-2);
			} else if (c == ')') {
				while (s.peek() != -2)
					sb.append(ops.charAt(s.pop())).append(' ');
				s.pop();
			} else {
				sb.append(token).append(' ');
			}
		}
		while (!s.isEmpty())
			sb.append(ops.charAt(s.pop())).append(' ');
		return sb.toString();
	}

	/*
	 * process the entire rule json string and return the rules as a array list
	 * (has to update for new rules format)
	 */
	private static List<String[]> processRuleString(String rulesMap) {
		List<String[]> rules = new ArrayList<String[]>();
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(rulesMap);
		JsonArray ruleArray = o.getAsJsonArray("rules");
		// System.out.println(ruleArray.size());
		for (int i = 0; i < ruleArray.size(); i++) {
			List<String> currentRuleArrayList = new ArrayList<String>();
			JsonObject currentRule = (JsonObject) ruleArray.get(i);
			JsonArray currentRuleJsonArray = currentRule
					.getAsJsonArray("ruleNode");
			// System.out.println(currentRuleArray.size());
			for (int j = 0; j < currentRuleJsonArray.size(); j++) {
				JsonObject currentRulePart = (JsonObject) currentRuleJsonArray
						.get(j);
				JsonObject currentRuleNode = currentRulePart
						.getAsJsonObject("node");
				String currentRulePartId = currentRuleNode
						.getAsJsonPrimitive("id").toString().replace('"', '-')
						.replace("-", "");
				String currentRulePartText = currentRuleNode
						.getAsJsonObject("text").getAsJsonPrimitive("caption")
						.toString();
				// System.out.println(currentRulePartId);
				currentRuleArrayList.add(currentRulePartId);
			}
			// System.out.println("array list size :"+currentRuleArrayList.size());
			String[] currentRuleArray = currentRuleArrayList
					.toArray(new String[currentRuleArrayList.size()]);
			rules.add(currentRuleArray);
			// System.out.println(currentRuleArray.length);
		}
		return rules;
	}

	/*
	 * recursive function to iterate the mindpap and create the tcases input xml
	 * magic ,do not touch
	 */
	@SuppressWarnings("unchecked")
	private static void inputXMLProccesor(JsonArray jsonArray,
			XMLStreamWriter writer, Map rulesMap,
			ArrayList<String> onceRulesList) throws XMLStreamException {
		// iterate over children array
		for (int i = 0; i < jsonArray.size(); i++) {
			if (((JsonObject) jsonArray.get(i)).getAsJsonArray("children")
					.size() > 0) {
				if (((JsonObject) ((JsonObject) jsonArray.get(i))
						.getAsJsonArray("children").get(0)).getAsJsonArray(
						"children").size() > 0) {
					// varset
					if (!stack.empty() && stack.peek() == "val") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "var") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "varset") {
						stack.pop();
						writer.writeEndElement();
					}

					writer.writeStartElement("VarSet");
					stack.push("varset");
					writer.writeAttribute(
							"name",
							((JsonObject) jsonArray.get(i))
									.getAsJsonObject("text")
									.getAsJsonPrimitive("caption").toString()
									.replace("\"", "").replace(" ", "-"));
				} else {
					// var
					if (!stack.empty() && stack.peek() == "val") {
						stack.pop();
						writer.writeEndElement();
					}
					if (!stack.empty() && stack.peek() == "var") {
						stack.pop();
						writer.writeEndElement();
					}
					writer.writeStartElement("Var");
					stack.push("var");
					writer.writeAttribute(
							"name",
							((JsonObject) jsonArray.get(i))
									.getAsJsonObject("text")
									.getAsJsonPrimitive("caption").toString()
									.replace("\"", "").replace(" ", "-"));
				}
			} else {
				// val
				if (!stack.empty() && stack.peek() == "val") {
					stack.pop();
					writer.writeEndElement();
				}
				writer.writeStartElement("Value");
				stack.push("val");
				writer.writeAttribute("name", ((JsonObject) jsonArray.get(i))
						.getAsJsonObject("text").getAsJsonPrimitive("caption")
						.toString().replace("\"", "").replace(" ", "-"));
				String nId = ((JsonObject) jsonArray.get(i))
						.getAsJsonPrimitive("id").toString().replace("-", "")
						.replace("\"", "");
				// write property attribute to vals
				writer.writeAttribute("property", nId);
				// add the once rules to xml
				if (onceRulesList.contains(nId)) {
					writer.writeAttribute("once", "true");
				}
				// add the complex rules to xml
				if (rulesMap != null) {
					if (rulesMap.get(nId) != null) {
						// System.out.println("rulesMap current :"
						// + rulesMap.get(nId).toString());
						ArrayList<String> currentRule = (ArrayList<String>) rulesMap
								.get(nId);
						// System.out.println(currentRule);
						writeComplexRuleToXML(writer, currentRule);
					}
				}
			}
			inputXMLProccesor(
					((JsonObject) jsonArray.get(i)).getAsJsonArray("children"),
					writer, rulesMap, onceRulesList);
		}
	}

	/*
	 * create a xml writer object,add the generic, pre elements to the xmlDoc
	 * and returns the writer object.
	 */
	private static XMLStreamWriter addPreXmlPart(StringWriter sw,
			String mMapName) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = outputFactory.createXMLStreamWriter(sw);
		writer.writeStartDocument();
		writer.writeStartElement("System");
		writer.writeAttribute("name", "Tcases");
		writer.writeStartElement("Function");
		writer.writeAttribute("name", "TC");
		writer.writeStartElement("Input");
		writer.writeStartElement("VarSet");
		writer.writeAttribute("name", mMapName);
		return writer;
	}

	/*
	 * Finalize the xmlDoc writing
	 */
	private static void addPostXmlPart(XMLStreamWriter writer)
			throws XMLStreamException {
		// finalize the elements opened in inputXMLProccesor
		while (!stack.empty()) {
			stack.pop();
			writer.writeEndElement();
		}
		// finalize the elements opened in addPreXmlPart
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		writer.writeEndElement();
		writer.flush();
	}

	// private static void writeOnceRule(XMLStreamWriter writer,
	// ArrayList<String> proccessedCond) throws XMLStreamException {
	//
	//
	// }
	/*
	 * write a complex rule to xml writer
	 */
	private static void writeComplexRuleToXML(XMLStreamWriter writer,
			ArrayList<String> proccessedCond) throws XMLStreamException {
		// System.out.println("processing rule :" + proccessedCond.toString());
		writer.writeStartElement("When");
		for (Iterator iterator = proccessedCond.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			// System.out.println(string);
			if (string.equals("all")) {
				writer.writeStartElement("AllOf");
			} else if (string.equals("any")) {
				writer.writeStartElement("AnyOf");
			} else if (string.equals("not")) {
				writer.writeStartElement("Not");
			} else if (string.equals("/all")) {
				writer.writeEndElement();
			} else if (string.equals("/any")) {
				writer.writeEndElement();
			} else if (string.equals("/not")) {
				writer.writeEndElement();
			} else {
				writer.writeEmptyElement("AllOf");
				writer.writeAttribute("property", string);
			}
		}
		writer.writeEndElement();
	}
}
