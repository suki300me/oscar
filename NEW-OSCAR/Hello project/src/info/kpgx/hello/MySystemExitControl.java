package info.kpgx.hello;

import java.security.Permission;

/*
 * implements way to control system.exit calls(Tcases issues system.exit calls)
 */
class MySystemExitControl {

	public static class ExitTrappedException extends SecurityException {
	}

	public static void forbidSystemExitCall() {
		final SecurityManager securityManager = new SecurityManager() {
			@Override
			public void checkPermission(Permission permission) {
				if (permission.getName().contains("exitVM")) {
					throw new ExitTrappedException();
				}
			}
		};
		System.setSecurityManager(securityManager);
	}

	public static void enableSystemExitCall() {
		System.setSecurityManager(null);
	}
}
