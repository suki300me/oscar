<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String ctx = request.getContextPath();
	String path = ctx + "/oscar";
%>

<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="./img/favicon.png">
		
		<title>Oscar | Generated Test Cases</title>
		
		<!-- Custom styles for this template -->
		<!-- <link href="./dashboard/dashboard.css" rel="stylesheet"> -->
		
		<script src="<%=path%>/js/jquery-1.10.2.js"></script>
		<script src="<%=path%>/js/jquery-ui.js"></script>
		<script src="<%=path%>/js/Commons.js"></script>
		<script src="<%=path%>/js/Database/Core.js"></script>
		
		<link rel="stylesheet" href="<%=path%>/css/bootstrap-responsive.css">
		<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=path%>/css/oscar-styles.css">
		<link rel="stylesheet" href="<%=path%>/css/jquery-ui.css">
		
		<script>
			$(document).ready(function() {
				hideWindowLoader();
			});
		</script>
	</head>
	<body style="">	
		<jsp:include page="./Commons/PageHeader.jsp" />	
		<script>
			document.getElementById("requirementsHeading").className = "active";
		</script>
		<div id="sb-site" style="min-height: 642px;">
			<div class="container-fluid fill">
				<div class="row fill">
					<div class="col-xs-12 main vcenter">
						<div class="container">
<!-- 						code specific for this page begins here -->
							<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
							<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%-- 							<c:import url="file:///C:/test/bar/testDef.xml" var="xmltext" /> --%>
							<c:import url="file:///C:/test/foo/template.xsl" var="xslt" />
<%-- 							<p>${xmltext}</p> --%>
							<x:transform xml="${testDef}" xslt="${xslt}" />
<!-- 						code specific for this page ends here -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="./Commons/Sidebar.jsp" />	
		<script src="<%=path%>/js/bootstrap.min.js"></script>	
		<jsp:include page="./Commons/CommonDialogs.jsp" />
	</body>
</html>