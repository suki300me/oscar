import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class ComplexCondMain {
	private final static String RULE_CONSTRAINT_WHEN = "when";
	private final static String RULE_CONSTRAINT_WHEN_NOT = "whenNot";
	// add above defined rules to this array
	private final static String[] DEFINED_RULE_CONSTRAINTS = {
			RULE_CONSTRAINT_WHEN, RULE_CONSTRAINT_WHEN_NOT };

	private final static String S_NOT = "not";
	private final static String E_NOT = "/not";
	private final static String S_ALL = "all";
	private final static String E_ALL = "/all";
	private final static String S_ANY = "any";
	private final static String E_ANY = "/any";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// sample rules
		String[] rawRule = { "A", "whenNot", "B", "&", "C" };
		String[] rawRule2 = { "A", "when" };
		String[] rawRule1 = { "C", "when", "D" };
		String[] rawRule3 = { "C", "E", "D" };
		String[] rawRule4 = { "C", "when","!","A", "&", "!","(","B", "|","|", "(","C", "&", "D",")",")" };

		String[][] rawRules = { rawRule, rawRule1, rawRule2, rawRule3,rawRule4 };

		Map<String, ArrayList<String>> proccessedRulesMap = processRuleArray(rawRules);

//		for (Map.Entry entry : proccessedRulesMap.entrySet()) {
//			System.out.println(entry.getKey() + ", " + entry.getValue());
//		}
		
		//simulate the xml writing
		StringWriter sw = new StringWriter();
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		try {
			XMLStreamWriter writer = outputFactory.createXMLStreamWriter(sw);
			writer.writeStartDocument();
			writer.writeStartElement("AllRules");
			for (Map.Entry entry : proccessedRulesMap.entrySet()) {
				System.out.println(entry.getKey() + ", " + entry.getValue());
				writeRuleToXML(writer, (ArrayList<String>) entry.getValue());
			}
			writer.writeEndElement();
			writer.flush();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(sw.toString());
	}

	/*
	 * process the whole rule array
	 */
	public static Map<String, ArrayList<String>> processRuleArray(
			String[][] rules) {
		Map<String, ArrayList<String>> proccessedRules = new HashMap<String, ArrayList<String>>();
		for (int i = 0; i < rules.length; i++) {
			proccessedRules = processRule(proccessedRules, rules[i]);
		}
		return proccessedRules;

	}

	/*
	 * process a individual rule
	 */
	public static Map<String, ArrayList<String>> processRule(
			Map<String, ArrayList<String>> ruleMap, String[] rule) {
		// rule must contain least 3 elements, else it's invalid
		if (rule.length < 3) {
			System.err.println("invalid rule :"
					+ Arrays.asList(rule).toString());
			return ruleMap;
		}
		String ruleHead = rule[0];
		String ruleConstraint = rule[1];
		// 2nd element of rule must be a rule_constraint. else it's invalid
		if (!Arrays.asList(DEFINED_RULE_CONSTRAINTS).contains(ruleConstraint)) {
			System.err.println("invalid rule :"
					+ Arrays.asList(rule).toString());
			return ruleMap;
		}
		// get the rule body
		String[] ruleBody = Arrays.copyOfRange(rule, 2, rule.length);

		StringBuilder builder = new StringBuilder();
		// make a string from rule body
		for (String string : ruleBody) {
			if (builder.length() > 0) {
				builder.append(" ");
			}
			builder.append(string);
		}
		String ruleBodyString = builder.toString();
		// Process the rule body
		ArrayList<String> processedRule = getProccessedRule(ruleBodyString);
		// handle special cases
		if (ruleConstraint.equals(RULE_CONSTRAINT_WHEN_NOT)) {
			processedRule.add(0, S_NOT);
			processedRule.add(E_NOT);
		}
		if (ruleBody.length == 1) {
			processedRule.add(0, S_ALL);
			processedRule.add(E_ALL);
		}
		// System.out.println("Processed rule :"+processedRule.toString());
		ruleMap.put(ruleHead, processedRule);
		return ruleMap;

	}

	public static void writeRuleToXML(XMLStreamWriter writer,
			ArrayList<String> proccessedCond) throws XMLStreamException {

		for (Iterator iterator = proccessedCond.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			if (string.equals("all")) {
				writer.writeStartElement("AllOf");
			} else if (string.equals("any")) {
				writer.writeStartElement("AnyOf");
			} else if (string.equals("not")) {
				writer.writeStartElement("Not");
			} else if (string.equals("/all")) {
				writer.writeEndElement();
			} else if (string.equals("/any")) {
				writer.writeEndElement();
			} else if (string.equals("/not")) {
				writer.writeEndElement();
			} else {
				writer.writeEmptyElement("AllOf");
				writer.writeAttribute("property", string);
			}
		}
	}

	/*
	 * process the rule body
	 */
	public static ArrayList<String> getProccessedRule(String rule) {
		return mapComplexCond(infixToPostfix(rule).split(" "));
	}

	/*
	 * process the array returned by shunting yard
	 */
	public static ArrayList<String> mapComplexCond(String[] complexCond) {

		Stack<ArrayList<String>> mArrayList = new Stack<ArrayList<String>>();
		String[] definedOperators = { "!", "&", "|" };
		System.out.println(Arrays.asList(complexCond).toString());
		for (int i = 0; i < complexCond.length; i++) {
			String currentElement = complexCond[i];
			System.out.println("mapComplexCond :"+currentElement);
			if (Arrays.asList(definedOperators).contains(currentElement)) {
				// operator
				
				if (currentElement.equals("!")) {
					ArrayList<String> currentArrayList = mArrayList.pop();
					currentArrayList.add(0, S_NOT);
					currentArrayList.add(E_NOT);
					mArrayList.push(currentArrayList);
				} else if (currentElement.equals("&")) {
					ArrayList<String> currentArrayListOne = mArrayList.pop();
					ArrayList<String> currentArrayList = new ArrayList<>(
							currentArrayListOne);
					if (mArrayList.size()>0) {
						ArrayList<String> currentArrayListTwo = mArrayList.pop();
						currentArrayList.addAll(currentArrayListTwo);
					}
					currentArrayList.add(0, S_ALL);
					currentArrayList.add(E_ALL);
					mArrayList.push(currentArrayList);
				} else if (currentElement.equals("|")) {
					ArrayList<String> currentArrayListOne = mArrayList.pop();
					ArrayList<String> currentArrayList = new ArrayList<>(
							currentArrayListOne);
					if (mArrayList.size()>0) {
						ArrayList<String> currentArrayListTwo = mArrayList.pop();
						currentArrayList.addAll(currentArrayListTwo);
					}
					currentArrayList.add(0, S_ANY);
					currentArrayList.add(E_ANY);
					mArrayList.push(currentArrayList);
				} else {
					System.out.println("Error undefined operator :"
							+ currentElement);
				}
			} else {
				// operand
				ArrayList<String> newArrayList = new ArrayList<>();
				newArrayList.add(currentElement);
				mArrayList.add(newArrayList);
			}
		}
		if (mArrayList.size() > 1) {
			return null;
		}
		return mArrayList.get(0);
	}

	/*
	 * change the infix expression to postfix expression using Shunting-yard
	 * algorithm
	 */
	static String infixToPostfix(String infix) {
		// precedence increases left to right
		final String ops = "&|!";
		StringBuilder sb = new StringBuilder();
		Stack<Integer> s = new Stack<>();

		for (String token : infix.split("\\s")) {
			char c = token.charAt(0);
			int idx = ops.indexOf(c);
			if (idx != -1 && token.length() == 1) {
				if (s.isEmpty())
					s.push(idx);
				else {
					while (!s.isEmpty()) {
						int prec2 = s.peek() / 2;
						int prec1 = idx / 2;
						if (prec2 > prec1 || (prec2 == prec1 && c != '^'))
							sb.append(ops.charAt(s.pop())).append(' ');
						else
							break;
					}
					s.push(idx);
				}
			} else if (c == '(') {
				s.push(-2);
			} else if (c == ')') {
				while (s.peek() != -2)
					sb.append(ops.charAt(s.pop())).append(' ');
				s.pop();
			} else {
				sb.append(token).append(' ');
			}
		}
		while (!s.isEmpty())
			sb.append(ops.charAt(s.pop())).append(' ');
		return sb.toString();
	}

}
