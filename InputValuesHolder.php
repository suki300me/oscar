<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InputValuesHolder
 *
 * @author samarakoon
 */

class InputValuesHolder {
    public $outerArray=array();
    public $innerArray=array();
    
    public function getOuterArray() {
        return $this->outerArray;
    }

    public function getInnerArray() {
        return $this->innerArray;
    }

    public function setOuterArray($outerArray) {
        $this->outerArray = $outerArray;
    }

    public function setInnerArray($innerArray) {
        $this->innerArray = $innerArray;
    }
}
