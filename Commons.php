<?php

require_once('./Logger.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commons
 *
 * @author samarakoon
 */
class Commons {

    public function array_merge_recursive_sequential($array1, $array2) {
        $outArray = [];
        $iterations = max(sizeof($array1), sizeof($array2));
        for ($x = 0; $x <= $iterations; $x++) {
            $arr1Val = [];
            $arr2Val = [];
            if(isset($array1[$x])){
                $arr1Val = $array1[$x];
            }
            if(isset($array2[$x])){
                $arr2Val = $array2[$x];
            }
            array_push($outArray, array_merge($arr1Val, $arr2Val));
        }
        return $outArray;
    }
    
    public function filterArrayByValue() {
        
    }

}
