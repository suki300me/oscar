<?php
 require_once './DBConnector.php';
 require_once './ProcessTuples.php';
 require_once './ProcessMindMap.php';
 // getElementPathById
 
// $projId="069f4f43f414a01472b3b3a55c18e4dd";
// $storyId="734f5b219cae31fb77d239ffb573f6ef";
$projId="069f4f43f414a01472b3b3a55c18e4dd";
$storyId="734f5b219cae31fb77d239ffb573f6ef";
// $ruleId="brid_5406daa7c03cf";
// $inputMap = json_decode(DBConnector::getMindMap($projId, $storyId));
// $mmProccesor=new ProcessMindMap($projId,$storyId);
// print_r($mmProccesor);
// function getTcasesInputXML(){
//     global $inputMap;
//     $inputXML = new SimpleXMLElement('<System name="Tcases"></System>');
//     $function=$inputXML->addChild('Function');
//     $function->addAttribute('name','TC');
//     $function->addChild('Input');
//     inputXMLProccesor($inputMap->mindmap->root->children,$inputXML->Function->Input);
//     return $inputXML;
// }
// function inputXMLProccesor($node, $xmlDocument){
//     global $mmProccesor;
//     if (is_array($node)) {
//         foreach ($node as $keyChild => $valueChild) {            
//             if (count($valueChild->children)>0) {
//                 if (count($valueChild->children[0]->children)>0) {
//                     // VarSet
//                     $newNode=$xmlDocument->addChild("VarSet");                    
//                 } else {
//                     // Var
//                     $newNode=$xmlDocument->addChild("Var");
//                 }                
//             }
//             else{
//                 // value
//                 $newNode=$xmlDocument->addChild("Value");
//                 $elementPath=$mmProccesor->getElementPathById($valueChild->id,'-');
//                 $newNode->addAttribute('property',str_replace('.', '', $elementPath));
//             }
//             $newNode->addAttribute('name',$valueChild->text->caption);
//             inputXMLProccesor($valueChild->children, $newNode);
//         }
//     } 
// }

// file_put_contents("debugXML.xml", print_r( getTcasesInputXML()->asXML(),true));
function processRulesNew($storyId, $xmlStr, $filepath){
        // $mmProcessor = new ProcessMindMap($projId, $storyId);
        $rules = DBConnector::getRules($storyId);
        file_put_contents("debug.txt", print_r($rules,true));
        $xmlStr=simplexml_load_string($xmlStr);
        // $inputXML = new SimpleXMLElement($xmlStr);        
        $rulesArray= array();      
        $ruleCons=array('rule-constraint-only_when','rule-constraint-only_not_when');        
        for ($i=0; $i < count($rules); $i++) {
            //iterate over rules
            $ruleArray=array();
            $ruleCon;
            $rulesFirstPartArray=array();
            $rulesSecondPartArray=array();
            $secondPart= false;
            for ($j=0; $j < count($rules[$i]['ruleParts']); $j++) {
                // iterate over rule parts
                if (!$secondPart) {
                    //still before/in the constraint part
                    if (in_array($rules[$i]['ruleParts'][$j], $ruleCons)) {
                        //in the contraint part
                        $ruleCon=$rules[$i]['ruleParts'][$j];//init the constraint
                        $secondPart=true;                        
                    } else {
                        array_push($rulesFirstPartArray,$rules[$i]['ruleParts'][$j]);
                    }                    
                } else {
                    //after the constraint part
                    //put the element in rulesSecondPartArray array
                    array_push($rulesSecondPartArray,$rules[$i]['ruleParts'][$j]);
                }  
            }
            //build the rule
            foreach ($rulesFirstPartArray as $fkey => $fvalue) {
                $ruleArray[$fvalue]=array($ruleCon=>array());
                foreach ($rulesSecondPartArray as $skey => $svalue) {
                    array_push($ruleArray[$fvalue][$ruleCon],$svalue);
                }
            }
            // merge all the rules together     
            $rulesArray=array_merge_recursive($ruleArray,$rulesArray);
        }
        //start processing the xml
        foreach ($rulesArray as $key => $value) {
            $fpart=$this->mmProcessor->getElementPathById($key, '-');
            $spart=$value;
            $fpartAsArray=explode('.',$fpart);
            $fXPath='/System[@name="Tcases"]/Function[@name="TC"]/Input';
            for ($varCount=count($fpartAsArray); $varCount >2 ; $varCount--) { 
                $fXPath=$fXPath.'/VarSet[@name="%s"]';            
                }
            $fXPath=$fXPath.'/Var[@name="%s"]/Value[@name="%s"]';
            $fXPathUpdated=vsprintf($fXPath, $fpartAsArray);
            foreach ($spart as $skey => $svalue) {
                $conName=$skey;
                $conElements=array();
                foreach ($svalue as $srkey => $srvalue) {
                    $conElement=$fpart=$this->mmProcessor->getElementPathById($srvalue, '-');
                    array_push($conElements, $conElement);
                }
                $itemList=$xmlStr->xpath($fXPathUpdated);
                $conElementsStr=implode (", ", $conElements);
                if ($conName=='rule-constraint-only_when') {
                    $itemList[0]->addAttribute('when', $conElementsStr);
                } else if($conName=='rule-constraint-only_not_when'){
                    $itemList[0]->addAttribute('whenNot', $conElementsStr);
                }  
            }
        }
        // change the xml according to pririty rule constraint =-1
        // $xmlStr=Self::processRulesWhenTupleConstraintMinus($xmlStr);
        
        return $xmlStr->saveXML($filepath);

    }

	function createGenerators($mindMapProcessor,$function) {
	global $storyId;
	// global $mindMapProcessor;
	$tuples = DBConnector::getTuples($storyId);
	$rootConstraint = 2;
	$generatorsXML = new SimpleXMLElement("<Generators></Generators>");
	$tupleGenerator = $generatorsXML->addChild('TupleGenerator');
	$tupleGenerator->addAttribute('function', $function);

	foreach ($tuples as $tuple) {
		$tupleElements = explode(',', $tuple['nodeId']);
		$tupleConstraint = $tuple['tupleConstraint'];
		if ($tupleConstraint>-1) {
		$combine = $tupleGenerator->addChild("Combine");
		$combine->addAttribute("tuples", $tupleConstraint);
		foreach ($tupleElements as $tupleElement) {
			if ($tupleElement == "8b686570-dc8d-49aa-9279-5320a7185b8d") {
			    $rootConstraint = $tupleConstraint;
			} else {
			    $include = $combine->addChild("Include");
			    $include->addAttribute("var", $mindMapProcessor->getElementPathById($tupleElement, '-'));
			}
			}
		}
	}
	$tupleGenerator->addAttribute('tuples', $rootConstraint);
	$generatorsXML->saveXML("DebugGenerators.xml");
	}

$mindMapProcessor = new ProcessMindMap($projId, $storyId);
$xml2 = $mindMapProcessor->getTcasesInputXML();
processRulesNew($storyId, $xml2, "DebugRules.xml");
createGenerators($mindMapProcessor,"TC", '2');
// file_put_contents("debug.txt", print_r( $xml2->asXML(),true));

?>