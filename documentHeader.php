
<div id="WindowLoadingModal" class="loadingModal"></div> <!-- This shows a spinner when page loaded. Call hideWindowLoader() in document.ready() -->

<?php
if (isset($_GET['projectid'])) {
    $parms = "?projectid=" . $_GET['projectid'];
    if (isset($_GET['storyid']))
        $parms.= "&storyid=" . $_GET['storyid'];
} else {
    $parms = "";
}
?>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span>
                <div class="navbar-brand sb-toggle-left navbar-left">
                    <div class="navicon-line"></div>
                    <div class="navicon-line"></div>
                    <div class="navicon-line"></div>
                </div>

                <a class="navbar-brand" href="index.php">Oscar
                </a>
            </span>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><img src="img/header_right_arrow_transparant.png" height="52" width="30"></li>
                <li id="requirementsHeading"><a href="./requirements.php<?php echo $parms; ?>">Requirements</a></li>
                <li><img src="img/header_right_arrow_transparant.png" height="52" width="30"></li>
                <li id="mindMapHeading"><a href="./mindMap.php<?php echo $parms; ?>">Test Design</a></li>
                <li><img src="img/header_right_arrow_transparant.png" height="52" width="30"></li>
                <li id="ruleEditorHeading"><a href="./ruleEditor.php<?php echo $parms; ?>">Rule Editor</a></li>
                <li><img src="img/header_right_arrow_transparant.png" height="52" width="30"></li>
                <li id="generatedTestCasesHeading"><a href="./generatedTestCases.php<?php echo $parms; ?>">Test Cases</a></li>
            </ul>
            <!--
            <form class="navbar-form navbar-right">
              <input type="text" class="form-control" placeholder="Search...">
            </form>
            -->
        </div>
    </div>
</div>
<div id="alert_placeholder_outer">
    <div id="alert_placeholder" style="position: absolute; z-index: 10000"></div>
</div>