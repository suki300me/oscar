<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

header("Content-Type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=testcases.xls"); // put your file name here
$url = '.\tcases\bin\TC-Test.xml'; // xml file location with file name
if (file_exists($url)) {
    $xml = simplexml_load_file($url);
    $firstIteration = true;
    foreach ($xml->Function->TestCase as $testCase) {
        if ($firstIteration) {
            echo('id' . "\t");
            foreach ($testCase->Input->Var as $var) {
                echo($var->attributes()->name) . "\t";
            }
            echo "\n";
        }
        $firstIteration = false;
        echo($testCase->attributes()->id + 1) . "\t";
        foreach ($testCase->Input->Var as $var) {
            echo($var->attributes()->value) . "\t";
        }
        echo "\n";
//                print_r($testCase);
    }
}