-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2014 at 04:02 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oscardb`
--

-- --------------------------------------------------------

--
-- Table structure for table `generators`
--

CREATE TABLE IF NOT EXISTS `generators` (
  `genRuleID` int(11) NOT NULL,
  `genText` varchar(50) DEFAULT NULL,
  `genElementId` varchar(50) NOT NULL,
  `genPartId` int(11) NOT NULL,
  `genStoryId` int(11) NOT NULL,
  PRIMARY KEY (`genRuleID`,`genElementId`,`genPartId`,`genStoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `generators`
--

INSERT INTO `generators` (`genRuleID`, `genText`, `genElementId`, `genPartId`, `genStoryId`) VALUES
(0, 'sdfsdf', 'rule-element-dragged-5e70bae2-cf35-4f16-ab70-b66a0', 1, 1),
(0, 'One Tuple', 'tuple-constraint-one', 0, 1),
(1, 'sdfsdf.sdfsdfhhhh', 'rule-element-dragged-a4095057-4073-448a-ab7b-ca9d6', 2, 1),
(1, 'sdfsdf.sdfsdfhhhh.New Idea', 'rule-element-dragged-e6a5d76c-b15f-4664-a2b6-6f7d6', 1, 1),
(1, 'Two Tuples', 'tuple-constraint-two', 0, 1),
(2, 'sdfsdf.sdfsdfhhhh.New Idea.New Idea', 'rule-element-dragged-78536ec2-29bf-4d57-9dc6-a2a84', 1, 1),
(2, 'sdfsdf.sdfsdfhhhh', 'rule-element-dragged-a4095057-4073-448a-ab7b-ca9d6', 2, 1),
(2, 'N Tuples', 'tuple-constraint-n', 0, 1),
(3, 'sdfsdf.dsfsd.sdf.1', 'rule-element-dragged-03e30acd-06e2-48bb-9b53-d89c7', 3, 1),
(3, 'sdfsdf.dsfsd', 'rule-element-dragged-7b0c565c-4860-4239-ab35-44260', 1, 1),
(3, 'sdfsdf.dfgdfg.NewIdea', 'rule-element-dragged-9b23006c-79c1-4fee-b0c8-8a640', 1, 1),
(3, 'sdfsdf.dsfsd.sdf', 'rule-element-dragged-fb8dc249-e039-4a83-98b8-583cc', 2, 1),
(3, 'One Tuple', 'tuple-constraint-one', 0, 1),
(3, 'Two Tuples', 'tuple-constraint-two', 0, 1),
(4, 'sdfsdf.dfgdfg.sdfds.3', 'rule-element-dragged-46556ee8-c78b-43f9-ba1b-790a7', 2, 1),
(4, 'sdfsdf', 'rule-element-dragged-5e70bae2-cf35-4f16-ab70-b66a0', 0, 1),
(4, 'sdfsdf.dfgdfg.sdfds.2', 'rule-element-dragged-72437486-f174-4ec6-8f06-38cbd', 3, 1),
(4, 'Two Tuples', 'tuple-constraint-two', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `idproject` varchar(45) NOT NULL,
  `projectName` varchar(45) NOT NULL,
  PRIMARY KEY (`idproject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`idproject`, `projectName`) VALUES
('1', 'Project-01'),
('2', 'proj2');

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `idRule` int(11) NOT NULL,
  `text` varchar(45) DEFAULT NULL,
  `elementId` tinytext,
  `partId` int(11) NOT NULL,
  `idstory` varchar(45) NOT NULL,
  PRIMARY KEY (`idRule`,`idstory`,`partId`),
  KEY `fk_story` (`idstory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`idRule`, `text`, `elementId`, `partId`, `idstory`) VALUES
(0, 'Have', 'rule-constraint-have', 0, '4'),
(0, '4WD', 'rule-element-dragged-05247bc4-b480-4612-a6b5-cd587140850c', 1, '4'),
(0, 'Only When', 'rule-constraint-only_when', 2, '4'),
(0, 'Front wheel drive', 'rule-element-dragged-0128bd34-5662-4a1e-8f63-55a9a4cea1b1', 3, '4'),
(1, 'Hatchback', 'rule-element-dragged-a063e4fb-f3b1-4378-a810-3e200decc497', 0, '4'),
(1, 'Only Not When', 'rule-constraint-only_not_when', 1, '4'),
(1, 'SUV', 'rule-element-dragged-0e359d90-0ecd-45ea-8a8c-dc5e744e82dd', 2, '4'),
(3, 'Have', 'rule-constraint-have', 0, '1'),
(3, 'sdfsdf.dfgdfg.NewIdea', 'rule-element-dragged-9b23006c-79c1-4fee-b0c8-8a640c97f914', 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `story`
--

CREATE TABLE IF NOT EXISTS `story` (
  `idstory` varchar(45) NOT NULL,
  `story` varchar(45) DEFAULT NULL,
  `mindmap` longtext,
  `xmlstring` longtext NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `idproject` varchar(45) NOT NULL,
  PRIMARY KEY (`idstory`),
  KEY `fk_project` (`idproject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `story`
--

INSERT INTO `story` (`idstory`, `story`, `mindmap`, `xmlstring`, `title`, `description`, `idproject`) VALUES
('1', 'story-1', '{"id":"bf9a6126-3f75-4832-9bf2-8d2711dcfa54","title":"sdfsdf","mindmap":{"root":{"id":"5e70bae2-cf35-4f16-ab70-b66a039dabe8","parentId":null,"text":{"caption":"sdfsdf","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"60ac51f6-d902-4951-9ac9-9f84fa376407","parentId":"5e70bae2-cf35-4f16-ab70-b66a039dabe8","text":{"caption":"dfgdfg","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":95,"y":157},"foldChildren":false,"branchColor":"#caef15","children":[{"id":"2ea5c7fd-0127-4ade-b553-7bc46daaf29c","parentId":"60ac51f6-d902-4951-9ac9-9f84fa376407","text":{"caption":"sdfds","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-20},"foldChildren":false,"branchColor":"#caef15","children":[{"id":"72437486-f174-4ec6-8f06-38cbdb429c1e","parentId":"2ea5c7fd-0127-4ade-b553-7bc46daaf29c","text":{"caption":"789","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":174,"y":-29},"foldChildren":false,"branchColor":"#caef15","children":[]},{"id":"46556ee8-c78b-43f9-ba1b-790a7d97f8d1","parentId":"2ea5c7fd-0127-4ade-b553-7bc46daaf29c","text":{"caption":"3","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":155,"y":35},"foldChildren":false,"branchColor":"#caef15","children":[]}]},{"id":"9b23006c-79c1-4fee-b0c8-8a640c97f914","parentId":"60ac51f6-d902-4951-9ac9-9f84fa376407","text":{"caption":"NewIdea","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":131,"y":86},"foldChildren":false,"branchColor":"#caef15","children":[{"id":"24e98b9b-c6f7-4f05-9b8f-b8fb0bbd314b","parentId":"9b23006c-79c1-4fee-b0c8-8a640c97f914","text":{"caption":"12","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":168,"y":-38},"foldChildren":false,"branchColor":"#caef15","children":[]},{"id":"5ea81a40-f8bb-4653-a0c1-5e24533dbbfe","parentId":"9b23006c-79c1-4fee-b0c8-8a640c97f914","text":{"caption":"5","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":167,"y":24},"foldChildren":false,"branchColor":"#caef15","children":[]}]}]},{"id":"7b0c565c-4860-4239-ab35-442609a0a826","parentId":"5e70bae2-cf35-4f16-ab70-b66a039dabe8","text":{"caption":"dsfsd","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":120,"y":-79},"foldChildren":false,"branchColor":"#5a4941","children":[{"id":"fb8dc249-e039-4a83-98b8-583cce94f2fc","parentId":"7b0c565c-4860-4239-ab35-442609a0a826","text":{"caption":"sdf","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":285,"y":-59},"foldChildren":false,"branchColor":"#5a4941","children":[{"id":"03e30acd-06e2-48bb-9b53-d89c77b9a658","parentId":"fb8dc249-e039-4a83-98b8-583cce94f2fc","text":{"caption":"1","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":128,"y":-1},"foldChildren":false,"branchColor":"#5a4941","children":[]},{"id":"50761596-976f-41fa-a370-4bdcaea3fc75","parentId":"fb8dc249-e039-4a83-98b8-583cce94f2fc","text":{"caption":"2","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":124,"y":66},"foldChildren":false,"branchColor":"#5a4941","children":[]}]},{"id":"f54d25cb-f615-45e7-92e1-fb1e2e322e19","parentId":"7b0c565c-4860-4239-ab35-442609a0a826","text":{"caption":"sdfs","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":155,"y":47},"foldChildren":false,"branchColor":"#5a4941","children":[{"id":"fcf293cd-c1ca-4ad6-8348-3a70bc53bfa2","parentId":"f54d25cb-f615-45e7-92e1-fb1e2e322e19","text":{"caption":"9","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":112,"y":-61},"foldChildren":false,"branchColor":"#5a4941","children":[]},{"id":"ab0326d8-30ee-48ad-8764-0015995aefe7","parentId":"f54d25cb-f615-45e7-92e1-fb1e2e322e19","text":{"caption":"6","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":152,"y":59},"foldChildren":false,"branchColor":"#5a4941","children":[]}]},{"id":"d54e832b-3351-496a-a8de-34cb008f53ce","parentId":"7b0c565c-4860-4239-ab35-442609a0a826","text":{"caption":"111","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":130,"y":-125},"foldChildren":false,"branchColor":"#5a4941","children":[{"id":"6a8bafd0-5cf6-452d-bcc4-21b4a8f6728a","parentId":"d54e832b-3351-496a-a8de-34cb008f53ce","text":{"caption":"3","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":136,"y":-29},"foldChildren":false,"branchColor":"#5a4941","children":[]},{"id":"7ef2d032-02d7-4142-ba2a-83b7384d701c","parentId":"d54e832b-3351-496a-a8de-34cb008f53ce","text":{"caption":"4","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":120,"y":30},"foldChildren":false,"branchColor":"#5a4941","children":[]}]}]}]}},"dates":{"created":1403594432011,"modified":1404389760840},"dimensions":{"x":4000,"y":2000},"autosave":false}', '<System name="Tcases"><!-- Version: $Revision: 199 $ $Date: 2012-02-26 14:50:41 -0600 (Sun, 26 Feb 2012) $ --><Function name="TC"><Input><VarSet name="sdfsdf" id="5e70bae2-cf35-4f16-ab70-b66a039dabe8"><VarSet name="dfgdfg" id="60ac51f6-d902-4951-9ac9-9f84fa376407"><Var name="sdfds" id="2ea5c7fd-0127-4ade-b553-7bc46daaf29c"><Value name="789" id="72437486-f174-4ec6-8f06-38cbdb429c1e"/><Value name="3" id="46556ee8-c78b-43f9-ba1b-790a7d97f8d1"/></Var><Var name="NewIdea" id="9b23006c-79c1-4fee-b0c8-8a640c97f914"><Value name="12" id="24e98b9b-c6f7-4f05-9b8f-b8fb0bbd314b"/><Value name="5" id="5ea81a40-f8bb-4653-a0c1-5e24533dbbfe"/></Var></VarSet><VarSet name="dsfsd" id="7b0c565c-4860-4239-ab35-442609a0a826"><Var name="sdf" id="fb8dc249-e039-4a83-98b8-583cce94f2fc"><Value name="1" id="03e30acd-06e2-48bb-9b53-d89c77b9a658"/><Value name="2" id="50761596-976f-41fa-a370-4bdcaea3fc75"/></Var><Var name="sdfs" id="f54d25cb-f615-45e7-92e1-fb1e2e322e19"><Value name="9" id="fcf293cd-c1ca-4ad6-8348-3a70bc53bfa2"/><Value name="6" id="ab0326d8-30ee-48ad-8764-0015995aefe7"/></Var><Var name="111" id="d54e832b-3351-496a-a8de-34cb008f53ce"><Value name="3" id="6a8bafd0-5cf6-452d-bcc4-21b4a8f6728a"/><Value name="4" id="7ef2d032-02d7-4142-ba2a-83b7384d701c"/></Var></VarSet></VarSet></Input></Function></System>', NULL, NULL, '1'),
('2', 'story-2', 'sdfsdfd', '', NULL, NULL, '1'),
('4', NULL, '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":134,"y":-26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1403596475647},"dimensions":{"x":4000,"y":2000},"autosave":false}', '<root><VarSet name="Vehicles" id="8b686570-dc8d-49aa-9279-5320a7185b8d"><VarSet name="Car" id="92952255-088e-4e3a-b64e-ec74e0b822aa"><Var name="Hatchback" id="a063e4fb-f3b1-4378-a810-3e200decc497"><Value name="Rear wheel drive" id="7fa04bf7-92b4-4262-adcc-61c694ec2433"/><Value name="Front wheel drive" id="0128bd34-5662-4a1e-8f63-55a9a4cea1b1"/></Var><Var name="Sedan" id="95863c38-bb4c-4e0b-8af9-3b3ce01c9394"><Value name="Front wheel drive" id="dd2f4fae-5b7e-4bd9-9f34-d47843c0506f"/><Value name="Rear wheel drive" id="84564205-3565-4adc-81a6-bcf3dc1ada42"/></Var></VarSet><VarSet name="SUV" id="0e359d90-0ecd-45ea-8a8c-dc5e744e82dd"><Var name="4WD" id="05247bc4-b480-4612-a6b5-cd587140850c"><Value name="Full Sized" id="4403231d-17c4-4f6e-a66b-d235108e3985"/><Value name="Compact" id="018466b5-fdb7-4e0f-ae63-58c0b27ae0ec"/></Var><Var name="2WD" id="e98df823-dffd-447a-976c-ac9afcc8b73c"><Value name="Full Sized" id="53844787-19c7-47a1-affc-08ae55ce4ca4"/><Value name="Compact" id="c2919d7c-45d5-4aa8-85cd-936bb79b6f8a"/></Var></VarSet></VarSet></root>', NULL, NULL, '1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rules`
--
ALTER TABLE `rules`
  ADD CONSTRAINT `fk_story` FOREIGN KEY (`idstory`) REFERENCES `story` (`idstory`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `story`
--
ALTER TABLE `story`
  ADD CONSTRAINT `fk_project` FOREIGN KEY (`idproject`) REFERENCES `project` (`idproject`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
