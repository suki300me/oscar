CREATE DATABASE  IF NOT EXISTS `oscardb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `oscardb`;
-- MySQL dump 10.13  Distrib 5.1.40, for Win32 (ia32)
--
-- Host: 127.0.0.1    Database: oscardb
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `idproject` varchar(45) NOT NULL,
  `projectName` varchar(45) NOT NULL,
  PRIMARY KEY  (`idproject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES ('1','proj'),('qwe321','Project-01');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rules`
--

DROP TABLE IF EXISTS `rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rules` (
  `idRule` int(11) NOT NULL auto_increment,
  `text` varchar(45) default NULL,
  `elementId` tinytext,
  `partId` int(11) NOT NULL,
  `idstory` varchar(45) NOT NULL,
  PRIMARY KEY  (`idRule`,`idstory`,`partId`),
  KEY `fk_story` (`idstory`),
  CONSTRAINT `fk_story` FOREIGN KEY (`idstory`) REFERENCES `story` (`idstory`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rules`
--

LOCK TABLES `rules` WRITE;
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `story`
--

DROP TABLE IF EXISTS `story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `story` (
  `idstory` varchar(45) NOT NULL,
  `story` varchar(45) default NULL,
  `mindmap` longtext,
  `title` varchar(45) default NULL,
  `description` varchar(45) default NULL,
  `idproject` varchar(45) NOT NULL,
  PRIMARY KEY  (`idstory`),
  KEY `fk_project` (`idproject`),
  CONSTRAINT `fk_project` FOREIGN KEY (`idproject`) REFERENCES `project` (`idproject`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `story`
--

LOCK TABLES `story` WRITE;
/*!40000 ALTER TABLE `story` DISABLE KEYS */;
INSERT INTO `story` VALUES ('2','story-2','sdfsdfd',NULL,NULL,'qwe321'),('story1hash','story-1','{\"id\":\"ab984d93-9330-4e92-a8d5-81890c74c487\",\"title\":\"Vehicles\",\"mindmap\":{\"root\":{\"id\":\"8b686570-dc8d-49aa-9279-5320a7185b8d\",\"parentId\":null,\"text\":{\"caption\":\"Vehicles\",\"font\":{\"style\":\"normal\",\"weight\":\"bold\",\"decoration\":\"none\",\"size\":20,\"color\":\"#000000\"}},\"offset\":{\"x\":0,\"y\":0},\"foldChildren\":false,\"branchColor\":\"#000000\",\"children\":[{\"id\":\"92952255-088e-4e3a-b64e-ec74e0b822aa\",\"parentId\":\"8b686570-dc8d-49aa-9279-5320a7185b8d\",\"text\":{\"caption\":\"Car\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":28,\"y\":-87},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[{\"id\":\"a063e4fb-f3b1-4378-a810-3e200decc497\",\"parentId\":\"92952255-088e-4e3a-b64e-ec74e0b822aa\",\"text\":{\"caption\":\"Hatchback\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":96,\"y\":-53},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[{\"id\":\"7fa04bf7-92b4-4262-adcc-61c694ec2433\",\"parentId\":\"a063e4fb-f3b1-4378-a810-3e200decc497\",\"text\":{\"caption\":\"Rear wheel drive\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":143,\"y\":21},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]},{\"id\":\"0128bd34-5662-4a1e-8f63-55a9a4cea1b1\",\"parentId\":\"a063e4fb-f3b1-4378-a810-3e200decc497\",\"text\":{\"caption\":\"Front wheel drive\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":134,\"y\":-26},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]}]},{\"id\":\"95863c38-bb4c-4e0b-8af9-3b3ce01c9394\",\"parentId\":\"92952255-088e-4e3a-b64e-ec74e0b822aa\",\"text\":{\"caption\":\"Sedan\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":96,\"y\":58},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[{\"id\":\"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f\",\"parentId\":\"95863c38-bb4c-4e0b-8af9-3b3ce01c9394\",\"text\":{\"caption\":\"Front wheel drive\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":143,\"y\":-43},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]},{\"id\":\"84564205-3565-4adc-81a6-bcf3dc1ada42\",\"parentId\":\"95863c38-bb4c-4e0b-8af9-3b3ce01c9394\",\"text\":{\"caption\":\"Rear wheel drive\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":146,\"y\":25},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]}]}]},{\"id\":\"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd\",\"parentId\":\"8b686570-dc8d-49aa-9279-5320a7185b8d\",\"text\":{\"caption\":\"SUV\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":29,\"y\":77},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[{\"id\":\"05247bc4-b480-4612-a6b5-cd587140850c\",\"parentId\":\"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd\",\"text\":{\"caption\":\"4WD\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":125,\"y\":-9},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[{\"id\":\"4403231d-17c4-4f6e-a66b-d235108e3985\",\"parentId\":\"05247bc4-b480-4612-a6b5-cd587140850c\",\"text\":{\"caption\":\"Full Sized\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":135,\"y\":-22},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]},{\"id\":\"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec\",\"parentId\":\"05247bc4-b480-4612-a6b5-cd587140850c\",\"text\":{\"caption\":\"Compact\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":119,\"y\":26},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]}]},{\"id\":\"e98df823-dffd-447a-976c-ac9afcc8b73c\",\"parentId\":\"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd\",\"text\":{\"caption\":\"2WD\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":126,\"y\":70},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[{\"id\":\"53844787-19c7-47a1-affc-08ae55ce4ca4\",\"parentId\":\"e98df823-dffd-447a-976c-ac9afcc8b73c\",\"text\":{\"caption\":\"Full Sized\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":135,\"y\":-22},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]},{\"id\":\"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a\",\"parentId\":\"e98df823-dffd-447a-976c-ac9afcc8b73c\",\"text\":{\"caption\":\"Compact\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":119,\"y\":26},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]}]}]}]}},\"dates\":{\"created\":1402321908149,\"modified\":1402325287664},\"dimensions\":{\"x\":4000,\"y\":2000},\"autosave\":false}',NULL,NULL,'qwe321'),('story2hash','story3','{\"id\":\"ab984d93-9330-4e92-a8d5-81890c74c487\",\"title\":\"VehiclesSAVE\",\"mindmap\":{\"root\":{\"id\":\"8b686570-dc8d-49aa-9279-5320a7185b8d\",\"parentId\":null,\"text\":{\"caption\":\"Vehicles\",\"font\":{\"style\":\"normal\",\"weight\":\"bold\",\"decoration\":\"none\",\"size\":20,\"color\":\"#000000\"}},\"offset\":{\"x\":0,\"y\":0},\"foldChildren\":false,\"branchColor\":\"#000000\",\"children\":[{\"id\":\"92952255-088e-4e3a-b64e-ec74e0b822aa\",\"parentId\":\"8b686570-dc8d-49aa-9279-5320a7185b8d\",\"text\":{\"caption\":\"Car\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":28,\"y\":-87},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[{\"id\":\"a063e4fb-f3b1-4378-a810-3e200decc497\",\"parentId\":\"92952255-088e-4e3a-b64e-ec74e0b822aa\",\"text\":{\"caption\":\"Hatchback\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":96,\"y\":-53},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[{\"id\":\"7fa04bf7-92b4-4262-adcc-61c694ec2433\",\"parentId\":\"a063e4fb-f3b1-4378-a810-3e200decc497\",\"text\":{\"caption\":\"Rear wheel drive\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":143,\"y\":21},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]},{\"id\":\"0128bd34-5662-4a1e-8f63-55a9a4cea1b1\",\"parentId\":\"a063e4fb-f3b1-4378-a810-3e200decc497\",\"text\":{\"caption\":\"Front wheel drive\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":134,\"y\":-26},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]}]},{\"id\":\"95863c38-bb4c-4e0b-8af9-3b3ce01c9394\",\"parentId\":\"92952255-088e-4e3a-b64e-ec74e0b822aa\",\"text\":{\"caption\":\"Sedan\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":96,\"y\":58},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[{\"id\":\"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f\",\"parentId\":\"95863c38-bb4c-4e0b-8af9-3b3ce01c9394\",\"text\":{\"caption\":\"Front wheel drive\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":143,\"y\":-43},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]},{\"id\":\"84564205-3565-4adc-81a6-bcf3dc1ada42\",\"parentId\":\"95863c38-bb4c-4e0b-8af9-3b3ce01c9394\",\"text\":{\"caption\":\"Rear wheel drive\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":146,\"y\":25},\"foldChildren\":false,\"branchColor\":\"#7ec420\",\"children\":[]}]}]},{\"id\":\"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd\",\"parentId\":\"8b686570-dc8d-49aa-9279-5320a7185b8d\",\"text\":{\"caption\":\"SUV\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":29,\"y\":77},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[{\"id\":\"05247bc4-b480-4612-a6b5-cd587140850c\",\"parentId\":\"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd\",\"text\":{\"caption\":\"4WD\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":125,\"y\":-9},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[{\"id\":\"4403231d-17c4-4f6e-a66b-d235108e3985\",\"parentId\":\"05247bc4-b480-4612-a6b5-cd587140850c\",\"text\":{\"caption\":\"Full Sized\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":135,\"y\":-22},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]},{\"id\":\"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec\",\"parentId\":\"05247bc4-b480-4612-a6b5-cd587140850c\",\"text\":{\"caption\":\"Compact\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":119,\"y\":26},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]}]},{\"id\":\"e98df823-dffd-447a-976c-ac9afcc8b73c\",\"parentId\":\"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd\",\"text\":{\"caption\":\"2WD\",\"font\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":126,\"y\":70},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[{\"id\":\"53844787-19c7-47a1-affc-08ae55ce4ca4\",\"parentId\":\"e98df823-dffd-447a-976c-ac9afcc8b73c\",\"text\":{\"caption\":\"Full Sized\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":135,\"y\":-22},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]},{\"id\":\"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a\",\"parentId\":\"e98df823-dffd-447a-976c-ac9afcc8b73c\",\"text\":{\"caption\":\"Compact\",\"font\":{\"weight\":\"normal\",\"style\":\"normal\",\"decoration\":\"none\",\"size\":15,\"color\":\"#000000\"}},\"offset\":{\"x\":119,\"y\":26},\"foldChildren\":false,\"branchColor\":\"#213e9c\",\"children\":[]}]}]}]}},\"dates\":{\"created\":1402321908149,\"modified\":1403021881961},\"dimensions\":{\"x\":4000,\"y\":2000},\"autosave\":false}',NULL,NULL,'qwe321');
/*!40000 ALTER TABLE `story` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-19 15:46:12
