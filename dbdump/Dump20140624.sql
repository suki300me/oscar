-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2014 at 01:07 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oscardb`
--

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `idproject` varchar(45) NOT NULL,
  `projectName` varchar(45) NOT NULL,
  PRIMARY KEY (`idproject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`idproject`, `projectName`) VALUES
('1', 'Project-01'),
('2', 'proj2');

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `idRule` int(11) NOT NULL,
  `text` varchar(45) DEFAULT NULL,
  `elementId` tinytext,
  `partId` int(11) NOT NULL,
  `idstory` varchar(45) NOT NULL,
  PRIMARY KEY (`idRule`,`idstory`,`partId`),
  KEY `fk_story` (`idstory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`idRule`, `text`, `elementId`, `partId`, `idstory`) VALUES
(0, 'Have', 'rule-constraint-have', 0, '4'),
(0, '4WD', 'rule-element-dragged-05247bc4-b480-4612-a6b5-cd587140850c', 1, '4'),
(0, 'Only When', 'rule-constraint-only_when', 2, '4'),
(0, 'Front wheel drive', 'rule-element-dragged-0128bd34-5662-4a1e-8f63-55a9a4cea1b1', 3, '4'),
(1, 'Hatchback', 'rule-element-dragged-a063e4fb-f3b1-4378-a810-3e200decc497', 0, '4'),
(1, 'Only Not When', 'rule-constraint-only_not_when', 1, '4'),
(1, 'SUV', 'rule-element-dragged-0e359d90-0ecd-45ea-8a8c-dc5e744e82dd', 2, '4');

-- --------------------------------------------------------

--
-- Table structure for table `story`
--

CREATE TABLE IF NOT EXISTS `story` (
  `idstory` varchar(45) NOT NULL,
  `story` varchar(45) DEFAULT NULL,
  `mindmap` longtext,
  `xmlstring` longtext NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `idproject` varchar(45) NOT NULL,
  PRIMARY KEY (`idstory`),
  KEY `fk_project` (`idproject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `story`
--

INSERT INTO `story` (`idstory`, `story`, `mindmap`, `xmlstring`, `title`, `description`, `idproject`) VALUES
('1', 'story-1', '{"id":"bf9a6126-3f75-4832-9bf2-8d2711dcfa54","title":"sdfsdf","mindmap":{"root":{"id":"5e70bae2-cf35-4f16-ab70-b66a039dabe8","parentId":null,"text":{"caption":"sdfsdf","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"a4095057-4073-448a-ab7b-ca9d663982ee","parentId":"5e70bae2-cf35-4f16-ab70-b66a039dabe8","text":{"caption":"sdfsdfhhhh","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":91,"y":-154},"foldChildren":false,"branchColor":"#4656b6","children":[{"id":"e6a5d76c-b15f-4664-a2b6-6f7d644889a5","parentId":"a4095057-4073-448a-ab7b-ca9d663982ee","text":{"caption":"New Idea","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":167,"y":110},"foldChildren":false,"branchColor":"#4656b6","children":[{"id":"78536ec2-29bf-4d57-9dc6-a2a84dd9ae87","parentId":"e6a5d76c-b15f-4664-a2b6-6f7d644889a5","text":{"caption":"New Idea","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":139,"y":-129},"foldChildren":false,"branchColor":"#4656b6","children":[{"id":"548a4263-4f4d-4572-b5df-b710233b1214","parentId":"78536ec2-29bf-4d57-9dc6-a2a84dd9ae87","text":{"caption":"New Idea","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":0,"y":126},"foldChildren":false,"branchColor":"#4656b6","children":[]}]}]},{"id":"abfbb487-f855-4d3d-b35e-ceaf48def96b","parentId":"a4095057-4073-448a-ab7b-ca9d663982ee","text":{"caption":"New Idea","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":122,"y":-18},"foldChildren":false,"branchColor":"#4656b6","children":[]}]},{"id":"60ac51f6-d902-4951-9ac9-9f84fa376407","parentId":"5e70bae2-cf35-4f16-ab70-b66a039dabe8","text":{"caption":"dfgdfg","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":144,"y":69},"foldChildren":false,"branchColor":"#caef15","children":[]}]}},"dates":{"created":1403594432011,"modified":1403607251670},"dimensions":{"x":4000,"y":2000},"autosave":false}', '<root><VarSet name="sdfsdf" id="5e70bae2-cf35-4f16-ab70-b66a039dabe8"><VarSet name="sdfsdfhhhh" id="a4095057-4073-448a-ab7b-ca9d663982ee"><VarSet name="New Idea" id="e6a5d76c-b15f-4664-a2b6-6f7d644889a5"><Var name="New Idea" id="78536ec2-29bf-4d57-9dc6-a2a84dd9ae87"><Value name="New Idea" id="548a4263-4f4d-4572-b5df-b710233b1214"/></Var></VarSet></VarSet><Var name="sdfsdfhhhh" id="a4095057-4073-448a-ab7b-ca9d663982ee"><Value name="New Idea" id="abfbb487-f855-4d3d-b35e-ceaf48def96b"/></Var></VarSet><Var name="sdfsdf" id="5e70bae2-cf35-4f16-ab70-b66a039dabe8"><Value name="dfgdfg" id="60ac51f6-d902-4951-9ac9-9f84fa376407"/></Var></root>', NULL, NULL, '1'),
('2', 'story-2', 'sdfsdfd', '', NULL, NULL, '1'),
('4', NULL, '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":134,"y":-26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1403596475647},"dimensions":{"x":4000,"y":2000},"autosave":false}', '<root><VarSet name="Vehicles" id="8b686570-dc8d-49aa-9279-5320a7185b8d"><VarSet name="Car" id="92952255-088e-4e3a-b64e-ec74e0b822aa"><Var name="Hatchback" id="a063e4fb-f3b1-4378-a810-3e200decc497"><Value name="Rear wheel drive" id="7fa04bf7-92b4-4262-adcc-61c694ec2433"/><Value name="Front wheel drive" id="0128bd34-5662-4a1e-8f63-55a9a4cea1b1"/></Var><Var name="Sedan" id="95863c38-bb4c-4e0b-8af9-3b3ce01c9394"><Value name="Front wheel drive" id="dd2f4fae-5b7e-4bd9-9f34-d47843c0506f"/><Value name="Rear wheel drive" id="84564205-3565-4adc-81a6-bcf3dc1ada42"/></Var></VarSet><VarSet name="SUV" id="0e359d90-0ecd-45ea-8a8c-dc5e744e82dd"><Var name="4WD" id="05247bc4-b480-4612-a6b5-cd587140850c"><Value name="Full Sized" id="4403231d-17c4-4f6e-a66b-d235108e3985"/><Value name="Compact" id="018466b5-fdb7-4e0f-ae63-58c0b27ae0ec"/></Var><Var name="2WD" id="e98df823-dffd-447a-976c-ac9afcc8b73c"><Value name="Full Sized" id="53844787-19c7-47a1-affc-08ae55ce4ca4"/><Value name="Compact" id="c2919d7c-45d5-4aa8-85cd-936bb79b6f8a"/></Var></VarSet></VarSet></root>', NULL, NULL, '1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rules`
--
ALTER TABLE `rules`
  ADD CONSTRAINT `fk_story` FOREIGN KEY (`idstory`) REFERENCES `story` (`idstory`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `story`
--
ALTER TABLE `story`
  ADD CONSTRAINT `fk_project` FOREIGN KEY (`idproject`) REFERENCES `project` (`idproject`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
