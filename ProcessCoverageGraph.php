<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CombinationHolder
 *
 * @author asandakelum
 */
require_once('./InputValuesHolder.php');
require_once('./CombinationHolder.php');
require_once('./CombinationGenerator.php');
require_once('./TestCaseProcessor.php');
require_once('./ProcessMindMap.php');
require_once('DBConnector.php');
require_once('XmlToJson.php');

class ProcessCoverageGraph {

    public $testCases;
    public $inputMap;
    public $mindMapProcessor;

    //public $elemArray = array();

    function __construct($projectid, $storyid, $TcasesProjName) {
        $testCaseProcessor = new TestCaseProcessor($TcasesProjName);
        $this->testCases = $testCaseProcessor->getTestCases();
        $this->mindMapProcessor = new ProcessMindMap($projectid, $storyid);
        $this->inputMap = $this->mindMapProcessor->getInputMap();
    }

    public function go($optimized) {
//        $variableArray = array(
//            2 => array("Vehicles.Cars.Config", "Vehicles.Cars.Drive", "Vehicles.SUVs.Config", "Vehicles.SUVs.Drive"),
//            3 => array("Vehicles.Cars.Config", "Vehicles.Cars.Drive", "Vehicles.SUVs.Config", "Vehicles.SUVs.Drive")
//        );
//        
//        
        //print_r($this->getTupleCountsForTestCases($variableArray, $this->getAllPossibleCombinations($variableArray)));
//        $variableArray = array(
//            2 => array("TravelSystem.Customer", "TravelSystem.TraveType", "TravelSystem.Browser", "TravelSystem.Device")
//        );
        
        $allVariables = $this->mindMapProcessor->getChildrenPaths(null, true); // This gives all variable paths under root
        
        $variableArray = array(
            2 => $allVariables
        );
        
        return $this->getTupleCountsForTestCases($variableArray, $this->getAllPossibleCombinations($variableArray), $optimized);
    }

    public function go1() {
        $innerObject = $this->inputMap->mindmap->root;
        echo '<pre>';
        print_r($this->mindMapProcessor->getChildVars($innerObject, "", array()));
        echo '</pre>';
    }

    public function getAllPossibleCombinations($variableArray) {
        //$json = '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":134,"y":-26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1404211411987},"dimensions":{"x":4000,"y":2000},"autosave":false}';
        //$json='{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":134,"y":-26},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"374bad7e-268b-4576-851d-8d12aaf38c49","parentId":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","text":{"caption":"front","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":176,"y":-18},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"aec862c8-05af-4df8-acf2-19fd21d40c28","parentId":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","text":{"caption":"rear","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":231,"y":26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1404736751217},"dimensions":{"x":4000,"y":2000},"autosave":false}';
        //$json = '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"Vehicles","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d","parentId":null,"text":{"caption":"Vehicles","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[{"id":"92952255-088e-4e3a-b64e-ec74e0b822aa","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"Car","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":28,"y":-87},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"a063e4fb-f3b1-4378-a810-3e200decc497","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Hatchback","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":-53},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"7fa04bf7-92b4-4262-adcc-61c694ec2433","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Rear wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":21},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"c8ae15ed-858e-4daf-9fbf-16e4f8c8f3ad","parentId":"7fa04bf7-92b4-4262-adcc-61c694ec2433","text":{"caption":"qqq","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":251,"y":-14},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"6f340391-d929-4cbf-be5e-d5a926d14622","parentId":"7fa04bf7-92b4-4262-adcc-61c694ec2433","text":{"caption":"uuuu","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":289,"y":72},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"70579485-841c-4ebb-a94f-4da37dd49ae6","parentId":"7fa04bf7-92b4-4262-adcc-61c694ec2433","text":{"caption":"eee","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":290,"y":29},"foldChildren":false,"branchColor":"#7ec420","children":[]}]},{"id":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","parentId":"a063e4fb-f3b1-4378-a810-3e200decc497","text":{"caption":"Front wheel drive","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":139,"y":-54},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"374bad7e-268b-4576-851d-8d12aaf38c49","parentId":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","text":{"caption":"front","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":234,"y":-13},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"aec862c8-05af-4df8-acf2-19fd21d40c28","parentId":"0128bd34-5662-4a1e-8f63-55a9a4cea1b1","text":{"caption":"rear","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":231,"y":26},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","parentId":"92952255-088e-4e3a-b64e-ec74e0b822aa","text":{"caption":"Sedan","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":96,"y":58},"foldChildren":false,"branchColor":"#7ec420","children":[{"id":"dd2f4fae-5b7e-4bd9-9f34-d47843c0506f","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Front wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":143,"y":-43},"foldChildren":false,"branchColor":"#7ec420","children":[]},{"id":"84564205-3565-4adc-81a6-bcf3dc1ada42","parentId":"95863c38-bb4c-4e0b-8af9-3b3ce01c9394","text":{"caption":"Rear wheel drive","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":146,"y":25},"foldChildren":false,"branchColor":"#7ec420","children":[]}]}]},{"id":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","parentId":"8b686570-dc8d-49aa-9279-5320a7185b8d","text":{"caption":"SUV","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":29,"y":77},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"05247bc4-b480-4612-a6b5-cd587140850c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"4WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":125,"y":-9},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"4403231d-17c4-4f6e-a66b-d235108e3985","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Full Sized","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"018466b5-fdb7-4e0f-ae63-58c0b27ae0ec","parentId":"05247bc4-b480-4612-a6b5-cd587140850c","text":{"caption":"Compact","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]},{"id":"e98df823-dffd-447a-976c-ac9afcc8b73c","parentId":"0e359d90-0ecd-45ea-8a8c-dc5e744e82dd","text":{"caption":"2WD","font":{"style":"normal","weight":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":126,"y":70},"foldChildren":false,"branchColor":"#213e9c","children":[{"id":"53844787-19c7-47a1-affc-08ae55ce4ca4","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Full Sized","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":135,"y":-22},"foldChildren":false,"branchColor":"#213e9c","children":[]},{"id":"c2919d7c-45d5-4aa8-85cd-936bb79b6f8a","parentId":"e98df823-dffd-447a-976c-ac9afcc8b73c","text":{"caption":"Compact","font":{"weight":"normal","style":"normal","decoration":"none","size":15,"color":"#000000"}},"offset":{"x":119,"y":26},"foldChildren":false,"branchColor":"#213e9c","children":[]}]}]}]}},"dates":{"created":1402321908149,"modified":1404737376248},"dimensions":{"x":4000,"y":2000},"autosave":false}';
        $allPossibleCombinationsArray = array();
        $innerObject = $this->inputMap->mindmap->root;
        foreach ($variableArray as $tupleConstraint => $variableList) {
            $pathHolder = "";
            $elemArray = $this->mindMapProcessor->pharseInputValueMatrix($innerObject, $pathHolder, new InputValuesHolder());
            //print_r($elemArray);
            $allPossibleCombinationsArray[$tupleConstraint] = $this->buildCombinationMatrix($elemArray, $variableList, $tupleConstraint);
        }
        return $allPossibleCombinationsArray;
    }

    public function buildCombinationMatrix($inputMatrix, $variableArray, $tupleConstraint) {
        $allPossibleTupleCombinations = array();
        $combinationFlag = false;
//        $variableArray = array("Vehicles.Car.Hatchback", "Vehicles.Car.Sedan", "Vehicles.SUV.4WD", "Vehicles.SUV.2WD");
//        $tupleConstraint = 2;
        foreach (new CombinationGenerator($variableArray, $tupleConstraint) as $substring) {
            $combinationArray = array();
            //echo implode(",", $substring), '<br/>';
            foreach ($substring as $value) {
                array_push($combinationArray, $inputMatrix->outerArray[$value]);
            }
            $cartesian_product = $this->array_cartesian_product($combinationArray);
            //print_r($cartesian_product);
            foreach ($cartesian_product as $singleComination) {
                //echo implode(",", $singleComination), '<br/>';
                $CombinationHolder = new CombinationHolder($singleComination, $combinationFlag);
                array_push($allPossibleTupleCombinations, $CombinationHolder);
            }
        }
        return $allPossibleTupleCombinations;
    }

    function array_cartesian_product($arrays) {
        $result = array();
        $arrays = array_values($arrays);
        $sizeIn = sizeof($arrays);
        $size = $sizeIn > 0 ? 1 : 0;
        foreach ($arrays as $array)
            $size = $size * sizeof($array);
        for ($i = 0; $i < $size; $i ++) {
            $result[$i] = array();
            for ($j = 0; $j < $sizeIn; $j ++)
                array_push($result[$i], current($arrays[$j]));
            for ($j = ($sizeIn - 1); $j >= 0; $j --) {
                if (next($arrays[$j]))
                    break;
                elseif (isset($arrays[$j]))
                    reset($arrays[$j]);
            }
        }
        return $result;
    }

    public function printVariableDistribution() {
        //print_r($this->testCases);
//        foreach (new CombinationGenerator($variableArray, $r) as $substring) {
//            echo implode(",", $substring), '<br/>';
//        }
        //$this->getCoveredTuplesForTestCase(array(array('Vehicles.Cars.Config', 'Vehicles.Cars.Drive'), array('Vehicles.Cars.Drive', 'Vehicles.SUVs.Drive')), 1);
        print_r($this->getTupleCountsForTestCases(""));
    }

    public function getValueForVariableInTestCase($testCase, $variable) {
//        foreach ($this->getTestCase($testCaseId) as $input) {
//            print_r($input);
//        }
        $flag = false;
        foreach ($testCase->Input->Var as $inputs) {
            //print_r($inputs->attributes()->name);
            if ($inputs->attributes()->name == $variable) {
                $flag = true;
                return $inputs->attributes()->value;
            }
        }
        if (!$flag) {
            echo("<script>console.log('PHP: " . 'A Combination was invalid... Coverage graph may not be accurate. Variable = ' . $variable . "');</script>");
            return 'error';
        }
    }

    public function getCoveredTuplesForTestCase($variableCombinations, $testCase) {
        $allTuples = array();
        foreach ($variableCombinations as $varCombination) {
            $singleTuple = array();
            foreach ($varCombination as $variable) {
                array_push($singleTuple, $this->getValueForVariableInTestCase($testCase, $variable));
            }
            array_push($allTuples, $singleTuple);
        }
        return $allTuples;
    }
    
    public function appendStringToSeqArray($string, $array, $stringBeforeArray){
        $outArray = array();
        foreach ($array as $value) {
            if($stringBeforeArray){
                $currentString = $string.$value;
            }
            else{
                $currentString = $value.$string;
            }
            array_push($outArray, $currentString);
        }
        return $outArray;
    }

    public function getTupleCountsForTestCases($variableArray, $allTuples, $optimized) {
        
        $coveredTupleCountsRunningSum = array();
        $uniqueTuplesCount = array();
        
        $arrayHeaderRow = array_keys($variableArray);
        
        if($optimized){
            $postfix = " (Optimized)";
        }
        else{
            $postfix = " (All Combinations)";
        }
        
        $arrayHeaderRow = $this->appendStringToSeqArray(" Tuples".$postfix, $arrayHeaderRow, false);
        array_push($arrayHeaderRow, "Total".$postfix);
        
        array_push($uniqueTuplesCount, $arrayHeaderRow);
        
        foreach ($this->testCases->Function->TestCase as $testCase) {
            $uniqueTuplesCountPerTuple = array();
            foreach ($variableArray as $tupleConstraint => $variableArrayForTuple) {
                $variableCombinations = new CombinationGenerator($variableArrayForTuple, $tupleConstraint);
//                foreach ($variableCombinations as $v1) {
//                    print_r($v1);
//                }
                if (array_key_exists($tupleConstraint, $coveredTupleCountsRunningSum)) {
                    $tupleCount = $coveredTupleCountsRunningSum[$tupleConstraint];
                } else {
                    $tupleCount = 0;
                    $coveredTupleCountsRunningSum[$tupleConstraint] = $tupleCount;
                }
                $expectedTuples = $this->getCoveredTuplesForTestCase($variableCombinations, $testCase);
//                print_r($expectedTuples);
                //echo '<br/>------------------------------------------------------<br/>';
                foreach ($expectedTuples as $expectedTuple) { //Tuples in TestCase
                    foreach ($allTuples[$tupleConstraint] as $tuple) { //Tuples in AllPossibleCombinations
                        if ($tuple->getCombination() == $expectedTuple) {
                            if (!$tuple->getCombinationFlag()) {
//                                echo $tupleConstraint . ' => new comb ------------------------------------<br/>';
                                $tuple->setCombinationFlag(true);
                                $tupleCount++;
                            } else {
                                echo("<script>console.log('PHP: " . $tupleConstraint . ' => Tuple is already covered in an earlier testcase. Ignored' . "');</script>"); //TODO: debug line. Remove this
                            }
                        }
                    } // End of -> Tuples in AllPossibleCombinations
                } // End of -> Tuples in TestCase
                array_push($uniqueTuplesCountPerTuple, $tupleCount);
                $coveredTupleCountsRunningSum[$tupleConstraint] = $tupleCount;
            }//end of tuple iterator
            array_push($uniqueTuplesCountPerTuple, array_sum($uniqueTuplesCountPerTuple)); // This adds the sum of unique tuples in each test case to the last elemnt of each of the inner arrays
            array_push($uniqueTuplesCount, $uniqueTuplesCountPerTuple);
        }//end of test cases for each
//            foreach ($tuples as $tuple) {
        //array_push($uniqueTuplesCount, $this->getCoveredTuplesForTestCase($variableCombinations, $testCase));
//            }//end of tuples for each

        return $uniqueTuplesCount;
    }

    public function getVarsForTuple($constraint, $storyId) {
        $tupleArray[] = array();
        $rules = DBConnector::getTupleVariables($constraint, $storyId);
        //iterate over rules

        foreach ($rules as $ruleId) {

            echo $ruleId['genText'] . "</br>";
            array_push($tupleArray, $ruleId['genText']);
        }
        return $tupleArray;
    }

}

?>
