


<!--<script src="jquery.min.js"></script>-->
<link href="bootstrap-combined.min.css" rel="stylesheet"> 
<script src="bootstrap.min.js"></script>
<link href="./css/responsiveSheet.css" rel="stylesheet">
<link href="./css/treeview.css" rel="stylesheet">

<div class="btn-group col-xs-12">
    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Create New <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a href="#" data-toggle="modal" data-target="#newitem">Project</a></li>
        <li><a href="#" data-toggle="modal" data-target="#newStory">Story</a></li>
    </ul>
</div>



<?php
require_once ('DBConnector.php');

$projs = DBConnector::getAllProject();

$stories = DBConnector::getStories();

$projStories = array();
?>
<div class="tree well" style="overflow-x: scroll;overflow-y: scroll; height: 90%; bottom: 10px; ">
    <ul>
        <li>
            <span><i class="glyphicon glyphicon-folder-open" ></i><a href='' class="responsiveFont">Projects</a></span>
            <ul>

                <?php
                foreach ($projs as $key => $value) {
                    $proj = $value['idproject'];
//print projects 
                    ?>  
                    <li>
                        <?php
                        if (isset($_GET['projectid']) && !isset($_GET['storyid'])) {
                            //echo a with project class
                            if ($proj == $_GET['projectid']) {
                                //print with class
                                ?>

                                <span style="background-color: #DDD"><i class="glyphicon  glyphicon-eye-open" id="mySpan" ></i><a class="responsiveFont" href='?projectid=<?php echo $proj ?>'><?php echo $value['projectName'] ?></a></span>
                                <ul>
                                <?php } else {
                                    ?>
                                    <span style=""><i class="glyphicon  glyphicon-eye-open" id="mySpan" ></i><a class="responsiveFont"  href='?projectid=<?php echo $proj ?>'><?php echo $value['projectName'] ?></a></span>
                                    <ul>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    ////////////////////////////////////////////////////////////////////
                                    foreach ($stories as $key => $story) {

                                        if ($story['idproject'] === $proj) {


                                            //print stories
                                            ?>

                                            <li>
                                                <span><i class="glyphicon  glyphicon-eye-open"></i><a class="responsiveFont"  href='?projectid=<?php echo $proj ?>&storyid=<?php echo $story['idstory'] ?>'><?php echo $story['story'] ?>123</a> </span>

                                            </li>
                                            <?php
                                        }
                                    }
                                    /////////////////////////////////////////////////////////
                                    ?>
                                </ul>  

                                <?php
                            } if (isset($_GET['projectid']) && isset($_GET['storyid'])) {
                                //echo a with project class
                                //print with class
                                ?>
                                <span style=""><i class="glyphicon glyphicon-folder-open" id="mySpan" ></i><a class="responsiveFont" href='?projectid=<?php echo $proj ?>'><?php echo $value['projectName'] ?></a></span>
                                <ul>

                                    <?php
                                    ////////////////////////////////////////////////////////////////////
                                    foreach ($stories as $key => $story) {

                                        if ($story['idproject'] === $proj) {
                                            if ($story['idstory'] == $_GET['storyid']) {


                                                //print stories
                                                ?>

                                                <li>
                                                    <span style="background-color: #DDD"><i class="glyphicon glyphicon-eye-open"></i><a class="responsiveFont" href='?projectid=<?php echo $proj ?>&storyid=<?php echo $story['idstory'] ?>'><?php echo $story['story'] ?></a> </span>

                                                </li>
                                                <?php
                                            } else {
                                                ?>

                                                <li>
                                                    <span><i class="glyphicon glyphicon-eye-open"></i><a class="responsiveFont" href='?projectid=<?php echo $proj ?>&storyid=<?php echo $story['idstory'] ?>'><?php echo $story['story'] ?></a> </span>

                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    /////////////////////////////////////////////////////////
                                    ?>
                                </ul>
                            <?php } elseif (!isset($_GET['projectid']) && !isset($_GET['storyid'])) {
                                ?>


                                <span style=""><i class="glyphicon glyphicon-folder-open" id="mySpan" ></i><a class="responsiveFont"  href='?projectid=<?php echo $proj ?>'><?php echo $value['projectName'] ?></a></span>

                                <ul>
                                    <?php
                                    ////////////////////////////////////////////////////////////////////
                                    foreach ($stories as $key => $story) {

                                        if ($story['idproject'] === $proj) {

                                            //print stories
                                            ?>

                                            <li>
                                                <span><i class="glyphicon  glyphicon-eye-open"></i><a class="responsiveFont" href='?projectid=<?php echo $proj ?>&storyid=<?php echo $story['idstory'] ?>'><?php echo $story['story'] ?></a> </span>

                                            </li>
                                            <?php
                                        }
                                    }
                                    /////////////////////////////////////////////////////////
                                    ?>
                                </ul>

                                <?php
                            }
                        }
                        ?>

                </li>

            </ul>

        </li>
    </ul>
</div>


<script typp="text/javascript">
    $(function() {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
        $('.tree li.parent_li > span').on('click', function(e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon glyphicon-plus').removeClass('glyphicon glyphicon-minus');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon glyphicon-minus').removeClass('glyphicon glyphicon-plus');
            }
            e.stopPropagation();
        });


    });




</script>

