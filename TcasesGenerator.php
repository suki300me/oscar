<?php

include './ShellExecute.php';

class TcasesGenerator {

    function writeTcasesToFile() {

        $xmlStylesheetBlock = '<?xml-stylesheet type="text/xsl" href="project.xsl"?>';

        try {
            copy("tcases\bin\TC-Input.xml", "tcases\bin\TCAll-Input.xml"); //To Generate all the possible combinations
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                exec('tcases\bin\tcases < tcases\bin\TC-Input.xml -g tcases\bin\TC-Generators.xml -n', $retval);
                exec('tcases\bin\tcases < tcases\bin\TCAll-Input.xml -g tcases\bin\TCAll-Generators.xml -n', $retvalAllComb);

                $this->writeTestCasesFile(implode("\n", $retval) . $xmlStylesheetBlock, "tcases/bin/TC-Test.xml");
                //below line is for debuging only
                // $this->writeTestCasesFile(implode("\n", $retval) . $xmlStylesheetBlock, "tcases/bin/TC-Test2.xml");
                $this->writeTestCasesFile(implode("\n", $retvalAllComb) . $xmlStylesheetBlock, "tcases/bin/TCAll-Test.xml");
            } else {
                exec('sh unix_exec.sh');
                file_put_contents("tcases/bin/TC-Test.xml", $xmlStylesheetBlock, FILE_APPEND);
            }
            return true;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            return false;
        }
    }

    private function writeTestCasesFile($stringToWrite, $path) {
        // file_put_contents("debug.txt", print_r($stringToWrite,true));
        // file_put_contents("tcases/bin/TC-Test.xml", print_r($stringToWrite,true));
        
        $myfile = fopen($path, "w") or die("Unable to open file: ".$path);
        // file_put_contents($path."debug", print_r($stringToWrite,true));
        fwrite($myfile, $stringToWrite);
        fclose($myfile);
    }

}

?>