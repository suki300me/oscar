<?php

include 'DBConnector.php';
require_once './Commons.php';

if (isset($_POST["jsondata"])) {
    $map = DBConnector::updateMindMap($_POST['projectid'], $_POST['storyid'], $_POST["jsondata"], $_POST["xmlString"]);
} else {
    $map = "Didn't receive mind map to write to database";
}
Logger::log(LogType::info, $map);
?>
