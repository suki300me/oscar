<?php

require_once('Database.php');

//    $stringData = $_POST['dataString']; 
//    echo $stringData;

class DBConnector {

    public function __construct() {
        die('Init function is not allowed');
    }
    
    // story table
    public static function getXML($storyId) {
        $pdo = Database::connect();
        $sql = "SELECT xmlstring FROM story WHERE idstory = :storyid";
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute(array(':storyid' => $storyId));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result['xmlstring'];
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

    // story table
    public static function getMindMap($projectid, $storyid) {
        try {
            $pdo = Database::connect();
            $sql = "SELECT mindmap FROM story where idproject='" . $projectid . "' AND idstory='" . $storyid . "'";
            foreach ($pdo->query($sql) as $row) {
                $map = $row['mindmap'];
            }
            Database::disconnect();
            return $map;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    // story table
    public static function updateMindMap($projectid, $storyid, $jsondata, $xmlStr) {
        try {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE story set mindmap = ?, xmlstring = ? WHERE idstory = ? And idproject=?";
            $q = $pdo->prepare($sql);
            $q->execute(array($jsondata, $xmlStr, $storyid, $projectid));
            Database::disconnect();
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    // story table
    public static function updateRequirment($projectid, $storyid, $title, $description) {
        try {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE story set title = ?,description=? WHERE storyid = ? And idproject=?";
            $q = $pdo->prepare($sql);
            $q->execute(array($title, $description, $storyid, $projectid));

            Database::disconnect();
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

    /**
     * Returns all storys as array
     * @param type $idproject
     * @return null
     */
    // story table
    public static function getStories() {
        $pdo = Database::connect();
        $sql = "SELECT idstory, story, idproject FROM story ORDER BY idstory ASC, idproject ASC";
        $stmt = $pdo->query($sql);
        try {
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            Database::disconnect();
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

    /**
     * Returns all storys as array
     * @param type $idproject
     * @return null
     */
    // story table
    public static function getStoriesForProject($projectId) {
        $pdo = Database::connect();
        $sql = "SELECT idstory, story FROM story where idproject = :projectId";
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute(array(':projectId' => $projectId));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            Database::disconnect();
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

    /**
     * Returns story details as an array
     * @param type $storyId
     */
    // story table
    public static function getStory($storyId) {
        $pdo = Database::connect();
        $sql = "SELECT story, description, refId FROM story where idstory = :storyId";
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute(array(':storyId' => $storyId));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            Database::disconnect();
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    // story table
    public static function createStory($storyName, $storyDescription, $projectId, $storyRefID) {
        try {
            $key = "oscarStoryId";
            $time = time();
            $storyId = md5($key . $time);
            $defaultMindMap = '{"id":"ab984d93-9330-4e92-a8d5-81890c74c487","title":"MindMap","mindmap":{"root":{"id":"8b686570-dc8d-49aa-9279-5320a7185b8d",'
                    . '"parentId":null,"text":{"caption":"Main Idea","font":{"style":"normal","weight":"bold","decoration":"none","size":20,"color":"#000000"}},'
                    . '"offset":{"x":0,"y":0},"foldChildren":false,"branchColor":"#000000","children":[]}},"dates":{"created":1402321908149,"modified":1403599913905},'
                    . '"dimensions":{"x":4000,"y":2000},"autosave":false}';

            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO story (idstory, story, mindmap, refId, description, idproject) values(?, ?, ?, ?, ?, ?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($storyId, $storyName, $defaultMindMap, $storyRefID, $storyDescription, $projectId));

            Database::disconnect();
            return [$projectId, $storyId];
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    // updated to acompany nodeId
    // TODO: remove the 'elementId' and 'text' insertions to DB    
    // rules table
    public static function insertRule($ruleId, $storyId, $ruleParts) {
        //include 'Database.php';        
        $success = false;
        if (is_array($ruleParts)) {
            $pdo = Database::connect();
            //build query
            $sql = "INSERT INTO rules (idRule, text, elementId, partId, idstory, nodeId) ";
            $sql .="VALUES (:idRule, :text, :elemId, :partId, :storyId, :nodeId)";
            //pdo statement
            $stmt = $pdo->prepare($sql);
            //bind parameters
            $stmt->bindParam(':idRule', $idRule, PDO::PARAM_INT);
            $stmt->bindParam(':text', $txt, PDO::PARAM_STR);
            $stmt->bindParam(':elemId', $elemId, PDO::PARAM_STR);
            $stmt->bindParam(':partId', $partId, PDO::PARAM_INT);
            $stmt->bindParam(':storyId', $stryId, PDO::PARAM_STR);
            $stmt->bindParam(':nodeId', $nodeId, PDO::PARAM_STR);
            foreach ($ruleParts as $part) {
                $idRule = $ruleId;
                $txt = $part->text;
                $elemId = $part->elemId;
                $partId = $part->partId;
                $stryId = $storyId;
                $nodeId = $part->nodeId;;
                try {
                    $success = $stmt->execute();
                } catch (Exception $e) {
                    Database::disconnect();
                    Logger::log(LogType::error, $e->getMessage());
                    return false;
                }
            }
            Database::disconnect();
            return $success;
        }
    }

    // updated to acompany nodeId
    // rules table
    public static function deleteRule($ruleId, $storyId) {
        file_put_contents("debug.txt", print_r($ruleId,true));
        $pdo = Database::connect();
        $sql = "DELETE FROM rules WHERE idRule = :ruleid AND idstory = :storyid";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute(
                    array(
                        ':ruleid' => $ruleId,
                        ':storyid' => $storyId
                    )
            );
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

    // updated to acompany nodeId
    // rules table 
    public static function updateRule($ruleId, $storyId, $ruleParts){        
        try {
            $deleteSuccess=self::deleteRule($ruleId, $storyId);
            if ($deleteSuccess) {
                //insert only if delete suceed.
                try {
                    $updateSuccess=self::insertRule($ruleId, $storyId, $ruleParts);
                } catch (Exception $e) {
                    Logger::log(LogType::error, $e->getMessage());
                    return false;
                }                
            }
        } catch (Exception $e) {
            Logger::log(LogType::error, $e->getMessage());
                    return false;
        }
        return true;
    }
    // updated to acompany nodeId
    //one ring to rule them all
    // rules table
    public static  function getRules($storyId) {
        $pdo = Database::connect();
        $sql = "SELECT idRule,partId,nodeId FROM rules WHERE idstory = :storyid ORDER BY idRule ASC, partId ASC";
        $stmt = $pdo->prepare($sql);
        $allRules = array();
        $result;
        try {
            $stmt->execute(array(':storyid' => $storyId));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $currentRule = array();
            if (count($result) > 0) {
                //first rule begins
                $currentRuleId= $result[0]['idRule'];
                foreach ($result as $key => $value) {
                    if ($currentRuleId == $value['idRule']){                        
                        array_push($currentRule, $value['nodeId']);//parts of same rule
                    }
                    else{
                        //new rule begins
                        array_push($allRules, array('ruleId' => $currentRuleId, 'ruleParts' => $currentRule));//push the pror rule to allrules
                        $currentRuleId = $value['idRule'];
                        $currentRule = array();
                        array_push($currentRule, $value['nodeId']);//push the 1st part of new rule                    
                    }
                }
                array_push($allRules, array('ruleId' => $currentRuleId, 'ruleParts' => $currentRule));//push the last rule to allRules
            }              
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
        return $allRules;
    }

    /**
     * Returns rule in story by rule id and story id
     * @param type $storyId
     * @param type $ruleId
     * @return null
     */

    // check if the changes in rules table affected where used
    // rules table
    //what's the use of this function??? this is used in processRule.php and ProccessTcasesInput.php (same processRule function in bith files)
    public static function getRulesByStoryId($storyId, $ruleId) {
        $pdo = Database::connect();
        $sql = "SELECT * FROM rules WHERE idstory = :storyid AND idRule = :ruleid ORDER BY partId ASC";
        $stmt = $pdo->prepare($sql);
        try {
            //execute with given parameters
            $stmt->execute(array(
                ':storyid' => $storyId,
                ':ruleid' => $ruleId));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

    /**
     * Returns all rule id's in a story as array
     * @param type $storyId
     * @return null
     */
    // updated to acompany nodeId(no update needed)
    // rules table
    public static function getRulesIdArray($storyId) {
        $pdo = Database::connect();
        $sql = "SELECT idRule FROM rules WHERE idstory = :storyid";
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute(array(':storyid' => $storyId));
            $result = $stmt->fetchAll(PDO::FETCH_COLUMN | PDO::FETCH_UNIQUE, 0);
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    
    /**
     * Returns projects in DB as array
     * @param type $idproject
     * @return null
     */
    // project table
    public static function getAllProject() {
        $pdo = Database::connect();
        $sql = "SELECT * FROM project";
        $stmt = $pdo->query($sql);
        try {

            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }

// project table
    public static function createProject($projectName) {
        try {
            $key = "oscarProjId";
            $time = time();
            $projectId = md5($key . $time);

            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO project (idproject, projectName) values(?, ?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($projectId, $projectName));

            Database::disconnect();
            return $projectId;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    
// generators table
    public static function addTuple($nodeId, $tupleConstraint, $storyId) {
        try {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO generators (nodeId, tupleConstraint, storyId) values(:nodeId, :tupleConstraint, :storyId)"
                    . " ON DUPLICATE KEY UPDATE tupleConstraint=VALUES(tupleConstraint)";
            $q = $pdo->prepare($sql);
            $q->execute(array(':nodeId' => $nodeId, ':tupleConstraint' => $tupleConstraint, ':storyId' => $storyId));
            Database::disconnect();
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
        }
    }

    /**
     * Returns all tuples for the story
     * @param type $storyId
     */
    // generators table
    public static function getTuples($storyId) {
        $pdo = Database::connect();
        $sql = "SELECT GROUP_CONCAT(nodeId) as nodeId, tupleConstraint FROM generators where storyId = :storyId GROUP BY tupleConstraint";
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute(array(':storyId' => $storyId));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            Database::disconnect();
            return $result;
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
            return null;
        }
    }
    // generators table
    public static function removeTuples($storyId) {
        try {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM generators WHERE storyId = :storyId";
            $q = $pdo->prepare($sql);
            $q->execute(array(':storyId' => $storyId));
            Database::disconnect();
        } catch (Exception $e) {
            Database::disconnect();
            Logger::log(LogType::error, $e->getMessage());
        }
    }

    //NOTE: unused and old functions are documented below

    // not used, created a new function
    // rules table
    // public static function getRulesOld($storyId) {
    //     file_put_contents("debug.txt", print_r("DBConnector::getRules called from processRule\n",true), FILE_APPEND);
    //     $pdo = Database::connect();
    //     $sql = "SELECT * FROM rules WHERE idstory = :storyid ORDER BY idRule ASC, partId ASC";
    //     $stmt = $pdo->prepare($sql);
    //     $result;
    //     try {
    //         $stmt->execute(array(':storyid' => $storyId));
    //         $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //         $rulePartArr = array();
    //         $ruleArr = array();
    //         if (count($result) > 0) {                
    //             $currRuleId = $result[0]['idRule'];
    //             for ($i = 0; $i < count($result); $i++) {
    //                 if ($currRuleId == $result[$i]['idRule']) {
    //                     array_push($rulePartArr, $result[$i]);
    //                 } else {                        
    //                     array_push($ruleArr, array('ruleid' => $currRuleId, 'ruleparts' => $rulePartArr));
    //                     $currRuleId = $result[$i]['idRule'];
    //                     $rulePartArr = array();
    //                     array_push($rulePartArr, $result[$i]);
    //                 }
    //             }                
    //             array_push($ruleArr, array('ruleid' => $currRuleId, 'ruleparts' => $rulePartArr));                
    //             return $ruleArr;
    //         } else {
    //              file_put_contents("debug.txt", print_r($result,true), FILE_APPEND);
    //             return $result;
    //         }
    //     } catch (Exception $e) {
    //         Database::disconnect();
    //         Logger::log(LogType::error, $e->getMessage());
    //         return null;
    //     }
    // }

    /**
     * Return number of rules in particular story
     * @param type $storyId
     * @return null
     */
     // updated to acompany nodeId(no update needed)
    // rules table
    //why not count the rules returned by getRules???this isn't used
    // public static function getRulesCountOfStory($storyId) {
    //     $pdo = Database::connect();
    //     $sql = "SELECT COUNT(DISTINCT idRule) as rulecount FROM rules WHERE idstory = :storyid";
    //     $stmt = $pdo->prepare($sql);
    //     try {
    //         $stmt->execute(array(':storyid' => $storyId));
    //         $result = $stmt->fetch(PDO::FETCH_ASSOC);
    //         return $result['rulecount'];
    //     } catch (Exception $e) {
    //         Database::disconnect();
    //         Logger::log(LogType::error, $e->getMessage());
    //         return null;
    //     }
    // }

    // need to be modified for nodeId 
    // NOTE: check if this is used anywhere
    // rules table
    //what's the use of this function???? getRules also returns the rules as an array
    //this function is not used
    // public static function getRulesAsArray($storyId) {
    //     // file_put_contents("debug.txt", print_r("DBConnector::getRulesAsArray called from processRule\n",true), FILE_APPEND);
    //     $rulesArr = array();
    //     $pdo = Database::connect();
    //     // this needs to be edited
    //     $sql = "SELECT elementId, text FROM rules WHERE idstory = :storyid AND idRule = :ruleid ORDER BY partId ASC";
    //     $stmt = $pdo->prepare($sql);
    //     $rules = DBConnector::getRulesIdArray($storyId);
    //     //bind parameters
    //     $stmt->bindParam(':ruleid', $idRule, PDO::PARAM_INT);
    //     $stmt->bindParam(':storyid', $stryId, PDO::PARAM_STR);
    //     foreach ($rules as $rule) {
    //         try {
    //             $idRule = $rule;
    //             $stryId = $storyId;
    //             $stmt->execute();
    //             $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //             //var_dump(array_column($result, 'text', 'elementId'));
    //             //var_dump(array_column($rulesArr, "text"));
    //             //var_dump(array_column($rulesArr, 'text', 'elementId'));
    //             //array_push($rulesArr, implode(" ", $result));
    //             $keyValArr = array_column($result, 'text', 'elementId');
    //             array_push($rulesArr, $keyValArr);
    //         } catch (Exception $e) {
    //             Database::disconnect();
    //             Logger::log(LogType::error, $e->getMessage());
    //         }
    //     }
    //     Database::disconnect();
    //     return $rulesArr;
    // }

    // this is old, doesn't handle nodeId part. just kept for historic value. 
    // public static function updateRuleOld($ruleId, $storyId, $ruleParts) {
    //     //get rule       
    //     $oldRule = DBConnector::getRulesByStoryId($storyId, $ruleId);
    //     $oldRulePartCount = count($oldRule);
    //     $newRulePartCount = count($ruleParts);
    //     //include 'Database.php';
    //     $success = false;
    //     $pdo = Database::connect();
    //     //build update query
    //     $upsql = "UPDATE rules SET text = :text, elementId = :elemId WHERE ";
    //     $upsql .= "idRule = :idRule AND idstory = :storyId AND partId = :partId";
    //     //pdo statement
    //     $upstmt = $pdo->prepare($upsql);
    //     //bind parameters
    //     $upstmt->bindParam(':text', $txt, PDO::PARAM_STR);
    //     $upstmt->bindParam(':elemId', $elemId, PDO::PARAM_STR);
    //     $upstmt->bindParam(':idRule', $idRule, PDO::PARAM_INT);
    //     $upstmt->bindParam(':storyId', $stryId, PDO::PARAM_STR);
    //     $upstmt->bindParam(':partId', $partId, PDO::PARAM_INT);
    //     //build insert query
    //     $insql = "INSERT INTO rules (idRule, text, elementId, partId, idstory) ";
    //     $insql .="VALUES (:idRule, :text, :elemId, :partId, :storyId)";
    //     //insert pdo statement
    //     $instmt = $pdo->prepare($insql);
    //     //build delete query
    //     $delsql = "DELETE FROM rules WHERE idRule = :ruleid AND idstory = :storyid AND partId = :partid";
    //     $delstmt = $pdo->prepare($delsql);

    //     if (is_array($ruleParts)) {
    //         $pdo->beginTransaction();
    //         if ($oldRulePartCount == $newRulePartCount) {
    //             //equal no of parts update all                
    //             foreach ($ruleParts as $part) {
    //                 $idRule = $ruleId;
    //                 $txt = $part->text;
    //                 $elemId = $part->elemId;
    //                 $partId = $part->partId;
    //                 $stryId = $storyId;
    //                 try {
    //                     $success = $upstmt->execute();
    //                 } catch (Exception $e) {
    //                     Logger::log(LogType::error, $e->getMessage());
    //                     return false;
    //                 }
    //             }
    //         } else if ($oldRulePartCount < $newRulePartCount) {
    //             //insert new rule parts and update existing parts
    //             for ($i = 0; $i < count($ruleParts); $i++) {
    //                 if ($i < count($oldRule)) {
    //                     //update matched part
    //                     $idRule = $ruleId;
    //                     $txt = $ruleParts[$i]->text;
    //                     $elemId = $ruleParts[$i]->elemId;
    //                     $partId = $ruleParts[$i]->partId;
    //                     $stryId = $storyId;
    //                     try {
    //                         $success = $upstmt->execute();
    //                     } catch (Exception $e) {
    //                         Logger::log(LogType::error, $e->getMessage());
    //                         return false;
    //                     }
    //                 } else {
    //                     //insert new part
    //                     try {
    //                         $instmt->execute(array(
    //                             ':idRule' => $ruleId,
    //                             ':text' => $ruleParts[$i]->text,
    //                             ':elemId' => $ruleParts[$i]->elemId,
    //                             ':partId' => $ruleParts[$i]->partId,
    //                             ':storyId' => $storyId
    //                         ));
    //                     } catch (Exception $e) {
    //                         $pdo->rollback();
    //                         Logger::log(LogType::error, $e->getMessage());
    //                         return false;
    //                     }
    //                 }
    //             }
    //         } else if ($oldRulePartCount > $newRulePartCount) {
    //             for ($i = 0; $i < count($oldRule); $i++) {
    //                 if ($i < count($ruleParts)) {
    //                     //update matched part
    //                     $idRule = $ruleId;
    //                     $txt = $ruleParts[$i]->text;
    //                     $elemId = $ruleParts[$i]->elemId;
    //                     $partId = $ruleParts[$i]->partId;
    //                     $stryId = $storyId;
    //                     try {
    //                         $success = $upstmt->execute();
    //                     } catch (Exception $e) {
    //                         Logger::log(LogType::error, $e->getMessage());
    //                         return false;
    //                     }
    //                 } else {
    //                     //remove old part
    //                     try {
    //                         $delstmt->execute(array(
    //                             ':ruleid' => $ruleId,
    //                             ':storyid' => $storyId,
    //                             ':partid' => $oldRule[$i]['partId']
    //                         ));
    //                     } catch (Exception $e) {
    //                         $pdo->rollback();
    //                         Logger::log(LogType::error, $e->getMessage());
    //                         return false;
    //                     }
    //                 }
    //             }
    //         }
    //         $pdo->commit();
    //     }
    // }

}

?>
