<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestCaseProcessor
 *
 * @author samarakoon
 */
include './XmlParser.php';

class TestCaseProcessor {

    protected $url;

    function __construct($TcasesProjName) {
        $this->url = "./tcases/bin/" . $TcasesProjName . "-Test.xml";
    }

    public function getTestCases() {
        $xmlParser = new XmlParser();
        return $xmlParser->parseXmlToObjectFromFile($this->url); // TODO: Get this from database 
    }

    public function createTestCaseTable() {
        $html = new DOMDocument('1.0', 'iso-8859-1');
        $html->formatOutput = true;

        if (file_exists($this->url)) {
            $xml = simplexml_load_file($this->url);
            $firstIteration = true;

            $theader = $html->createElement('theader');
            $tbody = $html->createElement('tbody');
                $tbody->setAttribute('style', "height:'20px'; overflow-y: 'scroll'; overflow-x: 'scroll';");
            $html->appendChild($theader);
            $html->appendChild($tbody);

            foreach ($xml->Function->TestCase as $testCase) {

                if ($firstIteration) {
                    
                    $tableHeaderRow = $html->createElement('tr');
                    $tableHeaderCell = $html->createElement('th');
                    $tableHeaderCellText = $html->createTextNode('id');
                    $tableHeaderCell->appendChild($tableHeaderCellText);
//                    print_r($tableHeaderCellText);
                    $tableHeaderRow->appendChild($tableHeaderCell);
//                    echo('id' . "\t");
                    foreach ($testCase->Input->Var as $var) {
//                        echo($var->attributes()->name) . "\t";
                        $tableHeaderCell = $html->createElement('th');
                        $tableHeaderCellText = $html->createTextNode($var->attributes()->name);
//                        print_r($tableHeaderCellText);
//                        print_r($tableHeaderCellText->textContent);
                        $tableHeaderCellText->nodeValue=preg_replace('/^[^.]*.\s*/', '', $tableHeaderCellText->nodeValue);
                        $tableHeaderCell->appendChild($tableHeaderCellText);
                        $tableHeaderRow->appendChild($tableHeaderCell);
                    }
//                    echo "\n";
                    $theader->appendChild($tableHeaderRow);
                }

                $tableRow = $html->createElement('tr');
                $tableDataCell = $html->createElement('td');
                $tableDataCellText = $html->createTextNode($testCase->attributes()->id + 1);
                $tableDataCell->appendChild($tableDataCellText);
                $tableRow->appendChild($tableDataCell);

                $firstIteration = false;
//                echo($testCase->attributes()->id + 1) . "\t";

                foreach ($testCase->Input->Var as $var) {
                    $tableDataCell = $html->createElement('td');
                    $tableDataCellText = $html->createTextNode($var->attributes()->value);
                    $tableDataCell->appendChild($tableDataCellText);
                    $tableRow->appendChild($tableDataCell);
//                    echo($var->attributes()->value) . "\t";
                }
//                echo "\n";
                $tbody->appendChild($tableRow);
//                print_r($testCase);
            }
        } else {
            return "Couldn't generate test cases. Please contact your system administrator";
        }
        return html_entity_decode($html->saveHTML());
    }

}
